using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class LightSource : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    private Light light;
    public int nodeFlags;
    public Rayform.Scene.Node.Flags objFlags;
    public Rayform.LightSource.LightFlags lightFlags;
    public Color color { get { return light.color; } set { light.color = value; } }
    public float ambient;
    public float brightness { get { return light.intensity; } set { light.intensity = value; } }
    public float distance { get { return light.range; } set { light.range = value; } }
    //public float shadowSoften { get { return light.shadowStrength; } set { light.shadowStrength = value; } }
    public float shadowSoften;
    public float spotCut;
    public float spotSoft;
    public float animStrength;
    public float animSpeed;
    public float animEmission;

    //public float spotCut { get { return light.spotAngle; } set { light.spotAngle = value; } }
    //public float range = 10;
    //public Color color;
    //public LightShadows shadows = LightShadows.Soft;

    void Init()
    {
        if (light == null)
        {
            light = gameObject.AddComponent<Light>();
            light.shadows = LightShadows.Soft;
        }
        light.hideFlags = HideFlags.HideInInspector;
    }

    private void Start()
    {
        Init();
        UpdateLight();
    }

    private void UpdateLight()
    {
        //light.intensity = intensity;
        //light.range = range;
        //light.color = color;        
    }

    private void OnValidate()
    {
        Init();
        UpdateLight();
    }

    public void OnDestroy()
    {
        DestroyImmediate(light);
    }

    public static void Import(GameObject obj, Rayform.LightSource lightSource)
    {

    }

    [CustomEditor(typeof(LightSource))]
    public class CustomInspector : Editor
    {
        LightSource target;

        public override void OnInspectorGUI()
        {
            GUIStyle style = new GUIStyle { richText = true };
            //target.lightFlags = (Rayform.LightSource.LightFlags)EditorGUILayout.EnumPopup("Light Type", target.lightFlags);
            target.objFlags = (Rayform.Scene.Node.Flags)EditorGUILayout.EnumFlagsField("Obj Flags", target.objFlags);
            target.lightFlags = (Rayform.LightSource.LightFlags)EditorGUILayout.EnumFlagsField("Light Flags", target.lightFlags);
            target.light.color = EditorGUILayout.ColorField("Color", target.light.color);
            target.ambient = EditorGUILayout.FloatField("Ambient", target.ambient);
            target.brightness = EditorGUILayout.FloatField("Brightness", target.brightness);
            target.distance = EditorGUILayout.FloatField("Distance", target.distance);
            target.shadowSoften = EditorGUILayout.FloatField("Shadow Soften", target.shadowSoften);
            if (target.objFlags.HasFlag(Rayform.Scene.Node.Flags.SpotLight))
            {
                target.spotCut = EditorGUILayout.FloatField("Spot Cut", target.spotCut);
                target.spotSoft = EditorGUILayout.FloatField("Spot Soft", target.spotSoft);
            }
            if (target.lightFlags.HasFlag(Rayform.LightSource.LightFlags.Animation))
            {
                target.animStrength = EditorGUILayout.FloatField("Anim Strength", target.animStrength);
                target.animSpeed = EditorGUILayout.FloatField("Anim Speed", target.animSpeed);
                target.animEmission = EditorGUILayout.FloatField("Anim Emission", target.animEmission);
            }
            //target.spotAngle = EditorGUILayout.FloatField("Spot Cut", target.light.spotAngle);
            //target.innerSpotAngle = EditorGUILayout.FloatField("Spot Soft", target.light.innerSpotAngle);
            //target.light.type = (LightType)EditorGUILayout.EnumPopup("Type", target.light.type);
        }

        void Awake()
        {
            target = (LightSource)serializedObject.targetObject;
            //obj.id = new Rayform.Id();
            //obj.data = new Rayform.Character();
            //Debug.Log(obj.data.name);
        }
    }
}