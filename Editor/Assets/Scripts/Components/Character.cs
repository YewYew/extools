using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static Rayform.Exploration;

[ExecuteInEditMode]
[SelectionBase]
[GlobalSelectionBase]
public class Character : MonoBehaviour
{
    public Rayform.Id id;
    public Rayform.CharacterPosition.Flags flags;
    public Rayform.Character data;
    public GameObject body;
    public bool exportPose;

    public void ImportBody()
    {
        if (body != null)
        {
            DestroyImmediate(body);
        }
        // Big ew, let's change this as soon madoc releases the new race system please
        string model = "humanbase.rfc";
        if (data.race == Rayform.Character.Race.Human)
        {
            model = "humanbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Skeleton)
        {
            model = "skelbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Ancient)
        {
            model = "ancientbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Ogre)
        {
            model = "ogrebase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Sir)
        {
            model = "hrtogrbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Fael)
        {
            model = "hrtafaelbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Ghoul)
        {
            model = "hrtaghlhbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.AlphaGhoul)
        {
            model = "hrtaghlabase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Gobbler)
        {
            model = "gobblerbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Controller)
        {
            model = "tntclsklbase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Wraith)
        {
            model = "wraithhabase.rfc";
        }
        else if (data.race == Rayform.Character.Race.Golem)
        {
            model = "golembase.rfc";
        }
        else if (data.race == Rayform.Character.Race.GolemBoss)
        {
            model = "glmbsabase.rfc";
        }

        ImportedAsset bodyAsset = ExanimaResources.LoadByName(model, typeof(GameObject), Rayform.PackageManager.PackageCategory.Resources);
        body = bodyAsset.Instantiate();
        body.transform.parent = transform;
        body.transform.Reset();
    }

    public static GameObject Import(Rayform.CharacterPosition characterPosition)
    {
        GameObject characterObj = Import(characterPosition.characterId);
        if (characterObj == null) return null;
        Character characterComp = characterObj.GetComponent<Character>();
        characterComp.flags = characterPosition.flags;
        characterObj.transform.position = characterPosition.locOrigin.ConvertVector3();
        characterObj.transform.eulerAngles = new Vector3(0, -characterPosition.locOrientation * Mathf.Rad2Deg, 0);

        // If the chunk is present, import the current pose
        Rayform.CharacterPosition.BonePositions bones = characterPosition.chunks.OfType<Rayform.CharacterPosition.BonePositions>().FirstOrDefault();
        if (bones != null)
        {
            characterComp.exportPose = true;
            Transform[] boneObjs = ExanimaEditor.GetChildTransformsRecursively(characterComp.body.transform);
            //last 2 bones are always left and right hand items? need to make sure
            for (int i = 0; i < bones.transforms.Count-2; i++)
            {
                //Debug.Log(boneObjs[i].transform.name);
                if (i == boneObjs.Length) break;
                boneObjs[i].transform.FromMatrix(bones.transforms[i].ConvertTo4x4(), false);
            }
        }
        return characterObj;
    }

    public static GameObject Import(Rayform.Id id)
    {
        Rayform.Save save = null;
        Rayform.Content locale = null;
        if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
        {
            save = (Rayform.Save)ExanimaEditor.file.data;
            locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == ExanimaEditor.selLocale).data;
        }
        else
        {
            locale = (Rayform.Content)ExanimaEditor.file.data;
        }

        Rayform.Character data = (Rayform.Character)Rayform.Rdb.FindRdbEntry(locale, save, id, typeof(Rayform.RdbChars));
        if (data == null) return null;
        GameObject characterObj = new GameObject();
        characterObj.name = data.name;
        characterObj.transform.parent = Tag.Find("Characters").transform;
        Character characterComp = characterObj.AddComponent<Character>();
        characterComp.data = data;
        characterComp.id = id;
        characterComp.ImportBody();
        return characterObj;
    }

    public Rayform.CharacterPosition Export()
    {
        Rayform.CharacterPosition characterPosition = new Rayform.CharacterPosition();
        characterPosition.characterId = id;
        characterPosition.flags = flags;
        characterPosition.locOrigin = transform.ToMatrix(false).GetPosition().ConvertVector3();
        characterPosition.locOrientation = -transform.eulerAngles.y * Mathf.Deg2Rad;

        if (exportPose) 
        {
            Rayform.CharacterPosition.BonePositions bonePositions = new Rayform.CharacterPosition.BonePositions();
            Transform[] boneObjs = ExanimaEditor.GetChildTransformsRecursively(body.transform);
            for (int i = 0; i < boneObjs.Length; i++)
            {
                //Debug.Log(boneObjs[i].transform.name);
                bonePositions.transforms.Add(boneObjs[i].transform.ToMatrix(false).ConvertTo3x4());
            }
            bonePositions.transforms.Add(new Rayform.Matrix3x4());
            bonePositions.transforms.Add(new Rayform.Matrix3x4());

            characterPosition.chunks.Add(bonePositions);
        }

        return characterPosition;
    }

    private void Awake()
    {
        if (data == null && id.baseId != 0)
        {
            Rayform.Save save = null;
            Rayform.Content locale = null;
            if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
            {
                save = (Rayform.Save)ExanimaEditor.file.data;
                locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == ExanimaEditor.selLocale).data;
            }
            else
            {
                locale = (Rayform.Content)ExanimaEditor.file.data;
            }

            data = (Rayform.Character)Rayform.Rdb.FindRdbEntry(locale, save, id, typeof(Rayform.RdbChars));
            name = data.name;
        }
    }

    [CustomEditor(typeof(Character))]
    public class CustomInspector : Editor
    {
        Character target;

        public override void OnInspectorGUI()
        {
            GUIStyle style = new GUIStyle { richText = true };

            target.exportPose = EditorGUILayout.Toggle("Export Pose", target.exportPose);

            GUILayout.BeginVertical(EditorStyles.helpBox);
            target.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", target.id.baseId);
            target.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Flags", target.id.flags);
            GUILayout.EndVertical();

            target.flags = (Rayform.CharacterPosition.Flags)EditorGUILayout.EnumPopup("Flags", target.flags);
            target.name = target.data.name;
            Character.CharacterFields(target.data);
        }

        void Awake()
        {
            target = (Character)serializedObject.targetObject;
            //obj.id = new Rayform.Id();
            //obj.data = new Rayform.Character();
            //Debug.Log(obj.data.name);
        }
    }

    public static void CharacterFields(Rayform.Character character)
    {
        GUIStyle style = new GUIStyle { richText = true };

        GUILayout.BeginVertical(EditorStyles.helpBox);
        character.name = EditorGUILayout.TextField("Name", character.name);
        character.surname = EditorGUILayout.TextField("Surname", character.surname);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("<b>Appearance</b>", style);
        //EditorGUILayout.LabelField("0 is slimmest and 1 is fattest", style);
        character.weight = EditorGUILayout.FloatField("Weight", character.weight);
        //EditorGUILayout.LabelField("0 is most muscular and 1 is least muscular", style);
        character.physique = EditorGUILayout.FloatField("Physique", character.physique);
        //EditorGUILayout.LabelField("-1 is tallest and 1 is shortest", style);
        character.height = EditorGUILayout.FloatField("Height", character.height);
        character.age = EditorGUILayout.FloatField("Age", character.age);
        character.skinX = (ushort)EditorGUILayout.IntSlider("Skin Tan", character.skinX, 0, ushort.MaxValue);
        character.skinY = (ushort)EditorGUILayout.IntSlider("Skin Hue", character.skinY, 0, ushort.MaxValue);
        character.hairStyle = EditorGUILayout.IntField("Hairstyle", character.hairStyle);
        character.hairHue = (byte)EditorGUILayout.IntSlider("Hair Hue", character.hairHue, 0, byte.MaxValue);
        character.hairBrightness = (byte)EditorGUILayout.IntSlider("Hair Brightness", character.hairBrightness, 0, byte.MaxValue);
        character.hairGreyness = (byte)EditorGUILayout.IntSlider("Hair Greyness", character.hairGreyness, 0, byte.MaxValue);
        character.voice = EditorGUILayout.TextField("Voice", character.voice);
        character.voicePitch = EditorGUILayout.FloatField("Voice Pitch", character.voicePitch);
        character.characterFlags = (Rayform.Character.CharacterFlags)EditorGUILayout.EnumFlagsField("Character Flags", character.characterFlags);
        character.race = (Rayform.Character.Race)EditorGUILayout.EnumPopup("Race", character.race);
        character.decomposition = EditorGUILayout.FloatField("Decomposition", character.decomposition);
        character.RNGSeed = EditorGUILayout.IntField("Seed", character.RNGSeed);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(EditorStyles.helpBox);
        character.health = (ushort)EditorGUILayout.IntSlider("Health", character.health, 0, ushort.MaxValue);
        character.stamina = (ushort)Mathf.Clamp(EditorGUILayout.IntSlider("Stamina", character.stamina, 0, ushort.MaxValue), 0, character.health);
        character.focusHealth = (ushort)EditorGUILayout.IntSlider("Focus Health", character.focusHealth, 0, ushort.MaxValue);
        character.focusStamina = (ushort)Mathf.Clamp(EditorGUILayout.IntSlider("Focus Stamina", character.focusStamina, 0, ushort.MaxValue), 0, character.focusHealth);
        GUILayout.EndVertical();

        bool showIds = true;

        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("<b>Primary Weapons</b>", style);
        GUILayout.Label("Right Hand");
        character.handEquips[0].id.baseId = (ushort)EditorGUILayout.IntField("Base Id", character.handEquips[0].id.baseId);
        character.handEquips[0].id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", character.handEquips[0].id.flags);
        GUILayout.Label("Left Hand");
        character.handEquips[1].id.baseId = (ushort)EditorGUILayout.IntField("Base Id", character.handEquips[1].id.baseId);
        character.handEquips[1].id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", character.handEquips[1].id.flags);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("<b>Secondary Weapons</b>", style);
        GUILayout.Label("Right Hand");
        character.handEquips2[0].id.baseId = (ushort)EditorGUILayout.IntField("Base Id", character.handEquips2[0].id.baseId);
        character.handEquips2[0].id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", character.handEquips2[0].id.flags);
        GUILayout.Label("Left Hand");
        character.handEquips2[1].id.baseId = (ushort)EditorGUILayout.IntField("Base Id", character.handEquips2[1].id.baseId);
        character.handEquips2[1].id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", character.handEquips2[1].id.flags);
        GUILayout.EndVertical();

        showIds = EditorGUILayout.Foldout(showIds, "Apparel");
        if (showIds)
        {
            Func<Rayform.HeldItem, Rayform.HeldItem> func = item =>
            {
                item.flags = (Rayform.ObjClass.Flags)EditorGUILayout.EnumFlagsField("Item Flags", item.flags);
                item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.id.baseId);
                item.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", item.id.flags);
                return item;
            };
            Toolbar.ListGUI(character.equippedItems, func);
        }

        GUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("<b>Personality</b>", style);
        character.personality.trustfulness = (byte)EditorGUILayout.IntSlider("Trustfulness", character.personality.trustfulness, 0, byte.MaxValue);
        character.personality.bravery = (byte)EditorGUILayout.IntSlider("Bravery", character.personality.bravery, 0, byte.MaxValue);
        character.personality.benAch = (byte)EditorGUILayout.IntSlider("BenAch", character.personality.benAch, 0, byte.MaxValue);
        character.personality.neuroticism = (byte)EditorGUILayout.IntSlider("Neuroticism", character.personality.neuroticism, 0, byte.MaxValue);
        GUILayout.EndVertical();

        showIds = EditorGUILayout.Foldout(showIds, "Inventory");
        if (showIds)
        {
            Func<Rayform.InventoryItem, Rayform.InventoryItem> func = item =>
            {
                item.x = (ushort)EditorGUILayout.IntField("X", item.x);
                item.y = (ushort)EditorGUILayout.IntField("Y", item.y);
                item.item.flags = (Rayform.ObjClass.Flags)EditorGUILayout.EnumFlagsField("Item Flags", item.item.flags);
                item.item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.item.id.baseId);
                item.item.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", item.item.id.flags);
                return item;
            };
            Toolbar.ListGUI(character.inventory, func);
        }

        showIds = EditorGUILayout.Foldout(showIds, "Roles");
        if (showIds)
        {
            Func<Rayform.Character.Role, Rayform.Character.Role> func = item =>
            {
                item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.id.baseId);
                return item;
            };
            Toolbar.ListGUI(character.roles, func);
        }

        if (GUILayout.Button("Reset Relations"))
        {
            character.relations = new Rayform.Character.Relations();
            character.worldRelations.Clear();
        }
        //for (int i = 0; i < character.equips.Count; i++)
        //{
        //    var dialogue = _dialogues.GetArrayElementAtIndex(i);
        //    EditorGUILayout.PropertyField(dialogue, new GUIContent("Dialogue " + i));
        //}

        //target.gameObject.name = target.data.name;
    }
}
