using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[GlobalSelectionBase]

public class LibNode : MonoBehaviour
{
    //private ImportedAsset asset;
    //public bool exportSubnodes;
    public Rayform.LibNodes.Node.Flags flags;
    [SerializeField]
    [HideInInspector]
    private string model;
    [SerializeField]
    public bool exportSubnodes;

    public static GameObject Import(Rayform.LibNodes.Node node)
    {
        GameObject libNodeObj = Import(node.name);
        if (libNodeObj == null) return null;

        libNodeObj.transform.FromMatrix(node.transform.ConvertTo4x4(), false);
        libNodeObj.GetComponent<LibNode>().flags = node.flags;
        if (node.subnodes.nodes.Count > 0)
        {
            libNodeObj.GetComponent<LibNode>().exportSubnodes = true;
        }
        ExanimaEditor.ImportSubnodes(node.subnodes, libNodeObj);
        return libNodeObj;
    }

    public static GameObject Import (string model)
    {
        ImportedAsset asset = ExanimaResources.LoadByName(model, typeof(GameObject), Rayform.PackageManager.PackageCategory.LibNodes);
        if (asset == null) return null;

        GameObject libNodeObj = asset.Instantiate();
        libNodeObj.transform.parent = Tag.Find("LibNodes").transform;
        libNodeObj.transform.GetChild(0).Reset();
        libNodeObj.name = model;
        LibNode nodeComp = libNodeObj.AddComponent<LibNode>();     

        return libNodeObj;
    }

    public Rayform.LibNodes.Node Export()
    {
        Rayform.LibNodes.Node node = new Rayform.LibNodes.Node();
        node.name = model;
        node.flags = flags;
        node.transform = Utils.ConvertTo3x4(transform.ToMatrix(false));

        if(exportSubnodes)
        {
            node.subnodes = ExanimaEditor.ExportSubnodes(gameObject);
        }
        else
        {
            node.subnodes = new Rayform.Subnodes();
        }

        return node;
    }

    void Awake()
    {
        if(string.IsNullOrEmpty(model))
        {
            model = name;
        }
        //Debug.Log("whar");
        else
        {
            //name = asset.fileName;
            name = model;
        }
    }
}
