using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class EditorExtensions : EditorWindow
{
    public static bool isBoxSelecting;
    public static Vector2 boxStartPos;
    public static Vector2 boxEndPos;
    public static Rect selectionRect;
    public static Vector2 boxWidth;
    public static Vector2 boxHeight;
    public static Vector2 boxOffset;

    static EditorExtensions()
    {
        SceneView.duringSceneGui += OnSceneGUI;

    }

    private static void OnSceneGUI(SceneView sceneView)
    {

    }
}