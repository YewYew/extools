using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Config", menuName = "Config", order = 1)]
public class Config : ScriptableObject
{
    public string installPath;

    public static Config Find()
    {
        string configPath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets($"t:{typeof(Config)}")[0]);
        //Debug.Log(configPath);
        //string configPath = "Assets/Config/Config.asset";
        return (Config)AssetDatabase.LoadAssetAtPath(configPath, typeof(Config));
    }
}