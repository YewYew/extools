using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class Initialize
{
    static Initialize()
    {
        Debug.Log("Initializing...");
        Config config = Config.Find();
        Rayform.Config.installPath = config.installPath;
    }
}
