﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Resources;
using System.Security.Policy;
using System.Globalization;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[InitializeOnLoad]
public static class FixCultureEditor
{
    static FixCultureEditor()
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
    }
}

 
public static class FixCultureRuntime
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void FixCulture()
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
    }
}

public class Toolbar : MonoBehaviour
{
    public class LoadFile : EditorWindow
    {
        [MenuItem("Exanima/Load", false, 0)]
        private static void Load()
        {
            string path = EditorUtility.OpenFilePanel("Select file", "", "");
            if (path.Length != 0)
            {
                ExanimaResources.Init();
                ExanimaEditor.Load(path);
            }
        }
    }

    [MenuItem("Exanima/Save", false, 1)]
    private static void Save()
    {
        ExanimaEditor.Save();
    }

    [MenuItem("Exanima/Unload/Confirm", false, 2)]
    private static void Unload()
    {
        ExanimaEditor.Unload();
        ExanimaResources.Unload();
    }

    [Serializable]
    public class LibNodeData
    {
        public string modelName;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
    }

    public class AddLibnodeGUI : EditorWindow
    {
        string name = "";
        Vector2 scrollPosition;
        Rayform.Package libNodes;
        int selPackage;
        string textSearch = "";

        [MenuItem("Exanima/LibNodes")]
        static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Add LibNode";
            window.Show();
        }

        void OnGUI()
        {
            Color selectedColor = new Color(0.25f, 0.5f, 0.75f, 1);
            Color borderColor = new Color(0.66f, 0.66f, 0.66f, 1);

            selPackage = EditorGUILayout.Popup("LibNode Package", selPackage, ((Rayform.Project)Rayform.PackageManager.projectFile.data).libNodes.ToArray());
            if (GUI.changed)
            {
                libNodes = (Rayform.Package)Rayform.PackageManager.LoadByPath(((Rayform.Project)Rayform.PackageManager.projectFile.data).libNodes[selPackage]);
            }
            textSearch = EditorGUILayout.TextField("Search", textSearch);

            //name = EditorGUILayout.TextField("LibNode", name);

            if (GUILayout.Button("Import"))
            {
                GameObject libNodeObj = LibNode.Import(name);
                placeAtCamera(libNodeObj);
                Selection.activeObject = libNodeObj;
            }

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);

            foreach (Rayform.Package.Entry entry in libNodes.entries)
            {
                if (!entry.name.ToString().Contains(textSearch, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                bool selected = entry.name == name;
                if (GUILayout.Button("", GUI.skin.label))
                {
                    GUI.FocusControl(null);
                    name = entry.name;
                }
                Rect lastRect = GUILayoutUtility.GetLastRect();

                if (selected)
                {
                    EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y, lastRect.width - 1, lastRect.height), selectedColor);
                }

                EditorGUI.DrawRect(new Rect(lastRect.x, lastRect.y + 1, 1, lastRect.height - 1), borderColor);
                EditorGUI.DrawRect(new Rect(lastRect.x + lastRect.width, lastRect.y + 1, 1, lastRect.height - 1), borderColor);

                EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y, lastRect.width - 1, 1), borderColor);
                EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y + lastRect.height, lastRect.width - 1, 1), borderColor);

                GUI.Label(new Rect(lastRect.x + 4, lastRect.y, lastRect.width, lastRect.height), entry.name);
            }
            GUILayout.EndScrollView();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save Selected Group", GUILayout.Width(EditorGUIUtility.labelWidth)))
            {
                SaveLibNodesDataToFile();
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Import Groups File (.libg)", GUILayout.Width(EditorGUIUtility.labelWidth)))
            {
                LoadLibNodesDataFromFile();
            }

            GUILayout.EndHorizontal();

            this.Repaint();
        }

        private void SaveLibNodesDataToFile()
        {
            string defaultFileName = ".libg";

            string windowTitle = "Save .libg File";

            string filePath = EditorUtility.SaveFilePanel(windowTitle, "", defaultFileName, "libg");

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            LibNodeData[] sceneData = LibNodeSceneHandler.CollectLibNodeData();

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                foreach (LibNodeData data in sceneData)
                {
                    writer.WriteLine($"MODEL: {data.modelName}");
                    writer.WriteLine($"POS: {data.position.x}, {data.position.y}, {data.position.z}");
                    writer.WriteLine($"ROT: {data.rotation.x}, {data.rotation.y}, {data.rotation.z}");
                    writer.WriteLine($"SCALE: {data.scale.x}, {data.scale.y}, {data.scale.z}");
                    writer.WriteLine();
                }
            }
        }
        private void LoadLibNodesDataFromFile()
        {
            string filePath = EditorUtility.OpenFilePanel("Import .libg File", "", "libg");

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            string[] fileLines = File.ReadAllLines(filePath);

            GameObject libNodes = GameObject.Find("LibNodes");

            if (libNodes == null)
            {
                libNodes = new GameObject("LibNodes");
            }

            string currentModelName = null;
            Vector3 currentPosition = Vector3.zero;
            Vector3 currentRotation = Vector3.zero;
            Vector3 currentScale = Vector3.one;
            GameObject currentLibNodeObj = null;

            List<GameObject> selectedObjects = new List<GameObject>();

            foreach (string line in fileLines)
            {
                if (line.StartsWith("MODEL: "))
                {
                    currentModelName = line.Replace("MODEL: ", "").Trim();

                    currentLibNodeObj = LibNode.Import(currentModelName);

                    currentLibNodeObj.transform.SetParent(libNodes.transform);
                }
                else if (line.StartsWith("POS: "))
                {
                    currentPosition = ParseVector3FromLine(line, "POS:");
                }
                else if (line.StartsWith("ROT: "))
                {
                    currentRotation = ParseVector3FromLine(line, "ROT:");
                }
                else if (line.StartsWith("SCALE: "))
                {
                    currentScale = ParseVector3FromLine(line, "SCALE:");

                    if (currentLibNodeObj != null)
                    {
                        currentLibNodeObj.transform.position = currentPosition;
                        currentLibNodeObj.transform.eulerAngles = currentRotation;
                        currentLibNodeObj.transform.localScale = currentScale;

                        selectedObjects.Add(currentLibNodeObj);
                    }
                }
            }

            Selection.objects = selectedObjects.ToArray();
        }

        private Vector3 ParseVector3FromLine(string line, string prefix)
        {
            line = line.Replace(prefix, "").Trim();
            string[] values = line.Split(',');
            if (values.Length == 3)
            {
                float x, y, z;
                if (float.TryParse(values[0], out x) && float.TryParse(values[1], out y) && float.TryParse(values[2], out z))
                {
                    return new Vector3(x, y, z);
                }
            }

            // Return a default value
            return Vector3.zero;
        }

        private void Awake()
        {
            libNodes = (Rayform.Package)Rayform.PackageManager.LoadByPath(((Rayform.Project)Rayform.PackageManager.projectFile.data).libNodes[selPackage]);
        }
    }

    public class ConfigGUI : EditorWindow
    {
        [MenuItem("Exanima/Install Path")]
        private static void Load()
        {
            Config config = Config.Find();
            string path = config.installPath;
            path = EditorUtility.OpenFolderPanel("Select Path", path, "");
            if (path.Length != 0)
            {
                config.installPath = path;
                Rayform.Config.installPath = path;
            }
        }
    }

    public class TilemapGUI : EditorWindow
    {
        public HashSet<Vector2Int> selection = new HashSet<Vector2Int> { Vector2Int.zero };
        //public HashSet<Vector2Int> copy = new HashSet<Vector2Int> { Vector2Int.zero };
        //int x = 0;
        //int y = 0;
        bool ctrlPressed;
        bool shiftPressed;
        bool mousePressed;
        int gridWidth;
        int gridHeight;
        Rayform.Tilemap.Cell cell = new Rayform.Tilemap.Cell(true);
        Vector2 scrollPosition;
        bool disableObjects;
        bool editTiles;

		//Yew like kissing boys don't yew?
		bool applyComponent = true;
		bool applyVariant	= true;
		bool applySet		= true;
		bool enableCompass	= true;
		bool clipboardView = false;
		//bool roomOverwrite  = false; //If this worked, it would extend adjacent/selected rooms.
		//Nyew undo and redo. I could nest these stacks into tuples buuuut nah.
		private Stack<List<Rayform.Tilemap.Cell>> undoCellStack = new Stack<List<Rayform.Tilemap.Cell>>();
		private Stack<List<Vector2Int>> undoPositionsStack = new Stack<List<Vector2Int>>();
		private Stack<List<Rayform.Tilemap.Cell>> redoCellStack = new Stack<List<Rayform.Tilemap.Cell>>();
		private Stack<List<Vector2Int>> redoPositionsStack = new Stack<List<Vector2Int>>();
		//Cell has cell data, pos has pos data. Lol. >:3.
		private List<Rayform.Tilemap.Cell> copyCell = new List<Rayform.Tilemap.Cell>();
		private HashSet<Vector2Int> copyPos = new HashSet<Vector2Int> { Vector2Int.zero };
		//Nya! >-<
		
        [MenuItem("Exanima/Tilemap")]
        public static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Tilemap";
            window.Show();
        }

        void OnEnable()
        {
            Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
            gridWidth = tilemap.gridWidth;
            gridHeight = tilemap.gridHeight;
            //EditorGUIUtility.systemCopyBuffer = "";
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        void OnGUI()
        {
            int x = selection.Last().x;
            int y = selection.Last().y;

            GUIStyle style = new GUIStyle { richText = true };

            Rayform.Tilemap tilemap = ExanimaEditor.tilemap;

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);

            GUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("<b>Tilemap</b>", style);

                EditorGUILayout.LabelField("Wall Sets");
                Func<Rayform.Str128, Rayform.Str128> func = item =>
                {
                    return EditorGUILayout.TextField(item);
                };
                Toolbar.ListGUI(tilemap.wallSets, func);

                EditorGUILayout.LabelField("Floor Sets");
                Toolbar.ListGUI(tilemap.floorSets, func);

                gridWidth = EditorGUILayout.IntField("Grid Width", gridWidth);
                gridHeight = EditorGUILayout.IntField("Grid Height", gridHeight);
                
                if (GUILayout.Button("Apply"))
                {
                    //int paddingWidth = (tilemap.gridWidth - gridWidth)/2;
                    //int paddingHeight = (tilemap.gridHeight - gridHeight)/2;
                    tilemap.cells = Utils.Resize2D(tilemap.cells, gridWidth, gridHeight);
                    tilemap.gridWidth = gridWidth;
                    tilemap.gridHeight = gridHeight;
                    ExanimaEditor.ImportTilemap(tilemap);
                }
            GUILayout.EndVertical();

			EditorGUILayout.BeginHorizontal();
            editTiles = EditorGUILayout.Toggle("Edit Tiles", editTiles);
            if (GUI.changed)
            {
                if (editTiles)
                {
                    SceneView.duringSceneGui += OnSceneGUI;
                    Selection.activeGameObject = null;
                }
                else
                {
                    SceneView.duringSceneGui -= OnSceneGUI;
                    //selection.Clear();
                    //copy.Clear();
                }
            }

            disableObjects = EditorGUILayout.Toggle("Disable Object Selection", disableObjects);
            if (GUI.changed)
            {
                if (disableObjects)
                {
                    Selection.activeGameObject = null;
                }
            }
			EditorGUILayout.EndHorizontal();

            GUILayout.BeginVertical(EditorStyles.helpBox);
			
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("<b>Tile</b>", style);
            enableCompass = EditorGUILayout.Toggle("Compass", enableCompass);
			EditorGUILayout.EndHorizontal();
			
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("X: ", x.ToString());
            EditorGUILayout.LabelField("Y: ", y.ToString());
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Component");
            EditorGUILayout.LabelField("  W        N         E         S       SW     NW     NE      SE", new GUIStyle { fontSize = 8 }, GUILayout.Height(10));

            EditorGUILayout.BeginHorizontal();
            cell.component[0] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[0], GUILayout.Width(20));
            cell.component[1] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[1], GUILayout.Width(20));
            cell.component[2] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[2], GUILayout.Width(20));
            cell.component[3] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[3], GUILayout.Width(20));
            cell.component[4] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[4], GUILayout.Width(20));
            cell.component[5] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[5], GUILayout.Width(20));
            cell.component[6] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[6], GUILayout.Width(20));
            cell.component[7] = (Rayform.Int4)EditorGUILayout.IntField(cell.component[7], GUILayout.Width(20));
            applyComponent = EditorGUILayout.Toggle("Apply?", applyComponent);
			EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Variant");
            EditorGUILayout.LabelField("  W        N         E         S       SW     NW     NE      SE", new GUIStyle { fontSize = 8 }, GUILayout.Height(10));

            EditorGUILayout.BeginHorizontal();
            cell.variant[0] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[0], GUILayout.Width(20));
            cell.variant[1] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[1], GUILayout.Width(20));
            cell.variant[2] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[2], GUILayout.Width(20));
            cell.variant[3] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[3], GUILayout.Width(20));
            cell.variant[4] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[4], GUILayout.Width(20));
            cell.variant[5] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[5], GUILayout.Width(20));
            cell.variant[6] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[6], GUILayout.Width(20));
            cell.variant[7] = (Rayform.Int4)EditorGUILayout.IntField(cell.variant[7], GUILayout.Width(20));
            applyVariant = EditorGUILayout.Toggle("Apply?", applyVariant);
			EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Set");
            EditorGUILayout.LabelField("Floor  Wall      ?          ?         ?         ?          ?         ?", new GUIStyle { fontSize = 8 }, GUILayout.Height(10));

            EditorGUILayout.BeginHorizontal();
            cell.set[0] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[0], GUILayout.Width(20));
            cell.set[1] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[1], GUILayout.Width(20));
            cell.set[2] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[2], GUILayout.Width(20));
            cell.set[3] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[3], GUILayout.Width(20));
            cell.set[4] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[4], GUILayout.Width(20));
            cell.set[5] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[5], GUILayout.Width(20));
            cell.set[6] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[6], GUILayout.Width(20));
            cell.set[7] = (Rayform.Int4)EditorGUILayout.IntField(cell.set[7], GUILayout.Width(20));
            applySet = EditorGUILayout.Toggle("Apply?", applySet);
			EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Apply"))
            {
                Apply();
            }
            GUILayout.EndVertical();

			//oooooo yew like boys ur a boykisser.
            GUILayout.BeginVertical(EditorStyles.helpBox);
			
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("<b>TileDraw</b>", style);
            clipboardView = EditorGUILayout.Toggle("Clipboard Ghosting", clipboardView);
			EditorGUILayout.EndHorizontal();
            //drawMode = EditorGUILayout.Toggle("Draw Mode", drawMode);
			//EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Make Selection Room"))
            {
                calculateRoom();
            }
			//roomOverwrite = EditorGUILayout.Toggle("Overwrite existing room?", roomOverwrite);
			//EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Pillar(s) (Minimum 2x2 area)"))
            {
                addPillar();
            }
            if (GUILayout.Button("Remove Pillar(s)"))
            {
                removePillar();
            }
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Add Door(s) (Minimum 1x2 area)"))
            {
                addDoor();
            }
			if (GUILayout.Button("Remove Door(s)"))
            {
                removeDoor();
            }
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Rotate Selected:", style, GUILayout.Width(98));
			if (GUILayout.Button("↺ Counter"))
            {
                rotateTile(90); 
            }
			if (GUILayout.Button("↻ Clockwise"))
            {
                rotateTile(-90);
            }
			if (GUILayout.Button("Mirror N/S"))
            {
                mirrorTile("N");
            }
			if (GUILayout.Button("Mirror E/W"))
            {
                mirrorTile("E");
            }
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Clear Selection"))
            {
                clearArea();
            }
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Undo"))
            {
                UndoTileEdit();
            }
			if (GUILayout.Button("Redo"))
            {
                RedoTileEdit();
            }
			EditorGUILayout.EndHorizontal();
            GUILayout.EndVertical();
			//Nyaa~

            GUILayout.EndScrollView();
            //EditorGUILayout.LabelField("Enviroment");

            //Debug.Log("gui");
            this.Repaint();
        }
		//Nyew implementation is fast!!!
		//PushUndoState() is save.
		private void PushUndoState()
		{
			Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
			List<Rayform.Tilemap.Cell> currentCellState = new List<Rayform.Tilemap.Cell>();
			List<Vector2Int> changedPositions = new List<Vector2Int>();

			foreach (Vector2Int pos in selection)
			{
				currentCellState.Add(copyCellFromPos(pos, tilemap));
				changedPositions.Add(pos);
			}

			// Clear redo stacks when a new undo state is pushed.
			redoCellStack.Clear();
			redoPositionsStack.Clear();

			// Add the current state to the undo stacks.
			undoCellStack.Push(currentCellState);
			undoPositionsStack.Push(changedPositions);
		}

		private void UndoTileEdit()
		{
			Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
			if (undoCellStack.Count > 0)
			{
				//Pop the undo out of the stack.
				List<Rayform.Tilemap.Cell> undoCells = undoCellStack.Pop();
				List<Vector2Int> undoPositions = undoPositionsStack.Pop();
				
				//Create our temp vars.
				List<Rayform.Tilemap.Cell> currentCells = new List<Rayform.Tilemap.Cell>();
				List<Vector2Int> changedPositions = new List<Vector2Int>();
				
				//Fill up the lists with the current state based on the undo state changes.
				foreach (Vector2Int pos in undoPositions)
				{
					currentCells.Add(copyCellFromPos(pos, tilemap));
					changedPositions.Add(pos);
				}
				
				//Push the current state lists into the redo stack.
				redoCellStack.Push(currentCells);
				redoPositionsStack.Push(changedPositions);
				
				//Apply the undo
				ApplyUndoRedo(undoCells, undoPositions);
			}
			else
			{
				Debug.Log("Tilemap: Nothing to Undo!");
			}
		}
		private void RedoTileEdit()
		{
			Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
			if (redoCellStack.Count > 0)
			{
				List<Rayform.Tilemap.Cell> redoCells = redoCellStack.Pop();
				List<Vector2Int> redoPositions = redoPositionsStack.Pop();

				//Create our temp vars.
				List<Rayform.Tilemap.Cell> currentCells = new List<Rayform.Tilemap.Cell>();
				List<Vector2Int> changedPositions = new List<Vector2Int>();
				
				//Fill up the lists with the current state based on the redo state changes.
				foreach (Vector2Int pos in redoPositions)
				{
					currentCells.Add(copyCellFromPos(pos, tilemap));
					changedPositions.Add(pos);
				}
				
				//Push the current state lists into the undo stack.
				undoCellStack.Push(currentCells);
				undoPositionsStack.Push(changedPositions);

				ApplyUndoRedo(redoCells, redoPositions);
			}
			else
			{
				Debug.Log("Tilemap: Nothing to Redo!");
			}
		}
		private void ApplyUndoRedo(List<Rayform.Tilemap.Cell> changedCells, List<Vector2Int> changedPositions)
		{
			Rayform.Tilemap tilemap = ExanimaEditor.tilemap;

			for (int i = 0; i < changedCells.Count; i++)
			{
				Rayform.Tilemap.Cell cell = changedCells[i];
				Vector2Int pos = changedPositions[i];

				tilemap.cells[pos.x, pos.y] = cell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		//I should have probably done this earlier and wove it into other functions that yews it... Whatevah.
		private Rayform.Tilemap.Cell copyCellFromPos(Vector2Int pos, Rayform.Tilemap tilemap)
		{
			return new Rayform.Tilemap.Cell
				{
					component = tilemap.cells[pos.x, pos.y].component.ToArray(),
					variant = tilemap.cells[pos.x, pos.y].variant.ToArray(),
					set = tilemap.cells[pos.x, pos.y].set.ToArray()
				};
		}
		//Yew are silly :3 Nya~!
		private void calculateRoom()
        {
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = tilemap.cells[pos.x, pos.y];
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);

				//Wall
				calcdcell.component[1]= (selection.Contains(north)) ? (Rayform.Int4)0 : (Rayform.Int4)7;
				calcdcell.component[0]= (selection.Contains(west))  ? (Rayform.Int4)0 : (Rayform.Int4)7;
				calcdcell.component[2]= (selection.Contains(east))  ? (Rayform.Int4)0 : (Rayform.Int4)7;
				calcdcell.component[3]= (selection.Contains(south)) ? (Rayform.Int4)0 : (Rayform.Int4)7;

				//Straight
				calcdcell.component[4]= (selection.Contains(south) && !selection.Contains(west)) ? (Rayform.Int4)9 : (Rayform.Int4)10;
				calcdcell.component[5]= (selection.Contains(north) && !selection.Contains(west)) ? (Rayform.Int4)10 : (Rayform.Int4)9;
				calcdcell.component[6]= (selection.Contains(north) && !selection.Contains(east)) ? (Rayform.Int4)9 : (Rayform.Int4)10;
				calcdcell.component[7]= (selection.Contains(south) && !selection.Contains(east)) ? (Rayform.Int4)10 : (Rayform.Int4)9;
				
				//Outside Corner
				calcdcell.component[4]= (selection.Contains(south) && selection.Contains(west)) ? (Rayform.Int4)12 : calcdcell.component[4];
				calcdcell.component[5]= (selection.Contains(north) && selection.Contains(west)) ? (Rayform.Int4)12 : calcdcell.component[5];
				calcdcell.component[6]= (selection.Contains(north) && selection.Contains(east)) ? (Rayform.Int4)12 : calcdcell.component[6];
				calcdcell.component[7]= (selection.Contains(south) && selection.Contains(east)) ? (Rayform.Int4)12 : calcdcell.component[7];
				
				//Inside Corner
				calcdcell.component[4]= (!selection.Contains(south) && !selection.Contains(west)) ? (Rayform.Int4)11 : calcdcell.component[4];
				calcdcell.component[5]= (!selection.Contains(north) && !selection.Contains(west)) ? (Rayform.Int4)11 : calcdcell.component[5];
				calcdcell.component[6]= (!selection.Contains(north) && !selection.Contains(east)) ? (Rayform.Int4)11 : calcdcell.component[6];
				calcdcell.component[7]= (!selection.Contains(south) && !selection.Contains(east)) ? (Rayform.Int4)11 : calcdcell.component[7];
				
				//Anti-Pillar
				calcdcell.component[4]= (selection.Contains(south) && selection.Contains(west) && selection.Contains(southwest)) ? (Rayform.Int4)13 : calcdcell.component[4];
				calcdcell.component[5]= (selection.Contains(north) && selection.Contains(west) && selection.Contains(northwest)) ? (Rayform.Int4)13 : calcdcell.component[5];
				calcdcell.component[6]= (selection.Contains(north) && selection.Contains(east) && selection.Contains(northeast)) ? (Rayform.Int4)13 : calcdcell.component[6];
				calcdcell.component[7]= (selection.Contains(south) && selection.Contains(east) && selection.Contains(southeast)) ? (Rayform.Int4)13 : calcdcell.component[7];
				
				//baseSet[0]= (Rayform.Int4) 0; 
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
            }
        }

		private void addPillar()
        {
			if(selection.Count < 4) { Debug.LogWarning("Invalid Selection"); return;}
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = tilemap.cells[pos.x, pos.y];
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);
				
				//If 4x4 grid, make pillar.
				calcdcell.component[4]= (selection.Contains(south) && selection.Contains(west) && selection.Contains(southwest)) ? (Rayform.Int4)12 : calcdcell.component[4];
				calcdcell.component[5]= (selection.Contains(north) && selection.Contains(west) && selection.Contains(northwest)) ? (Rayform.Int4)12 : calcdcell.component[5];
				calcdcell.component[6]= (selection.Contains(north) && selection.Contains(east) && selection.Contains(northeast)) ? (Rayform.Int4)12 : calcdcell.component[6];
				calcdcell.component[7]= (selection.Contains(south) && selection.Contains(east) && selection.Contains(southeast)) ? (Rayform.Int4)12 : calcdcell.component[7];
				//Prolly should add checks to see if you aren't selecting an open space, cause rn it overwrites proper corners 'n that jazz.
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		private void removePillar()
        {
			if(selection.Count < 4) { Debug.LogWarning("Invalid Selection"); return;}
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = tilemap.cells[pos.x, pos.y];
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);
				
				//Anti-Pillar
				calcdcell.component[4]= (calcdcell.component[4] == (Rayform.Int4)12 && selection.Contains(south) && selection.Contains(west) && selection.Contains(southwest)) ? (Rayform.Int4)13 : calcdcell.component[4];
				calcdcell.component[5]= (calcdcell.component[5] == (Rayform.Int4)12 && selection.Contains(north) && selection.Contains(west) && selection.Contains(northwest)) ? (Rayform.Int4)13 : calcdcell.component[5];
				calcdcell.component[6]= (calcdcell.component[6] == (Rayform.Int4)12 && selection.Contains(north) && selection.Contains(east) && selection.Contains(northeast)) ? (Rayform.Int4)13 : calcdcell.component[6];
				calcdcell.component[7]= (calcdcell.component[7] == (Rayform.Int4)12 && selection.Contains(south) && selection.Contains(east) && selection.Contains(southeast)) ? (Rayform.Int4)13 : calcdcell.component[7];
				//Should have more checks tbh.
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		private void addDoor()
        {
			if(selection.Count < 2) { Debug.LogWarning("Invalid Selection"); return;}
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = tilemap.cells[pos.x, pos.y];
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);
				
				//If adjacent wall, make door.
				calcdcell.component[1]= (selection.Contains(north) && calcdcell.component[1]== (Rayform.Int4)7) ? (Rayform.Int4)11 : calcdcell.component[1];
				calcdcell.component[0]= (selection.Contains(west)  && calcdcell.component[0]== (Rayform.Int4)7) ? (Rayform.Int4)11 : calcdcell.component[0];
				calcdcell.component[2]= (selection.Contains(east)  && calcdcell.component[2]== (Rayform.Int4)7) ? (Rayform.Int4)11 : calcdcell.component[2];
				calcdcell.component[3]= (selection.Contains(south) && calcdcell.component[3]== (Rayform.Int4)7) ? (Rayform.Int4)11 : calcdcell.component[3];
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		private void removeDoor()
        {
			if(selection.Count < 2) { Debug.LogWarning("Invalid Selection"); return;}
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = tilemap.cells[pos.x, pos.y];
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);
				
				//Remove Door.
				calcdcell.component[1]= (selection.Contains(north) && calcdcell.component[1]== (Rayform.Int4)11) ? (Rayform.Int4)7 : calcdcell.component[1];
				calcdcell.component[0]= (selection.Contains(west)  && calcdcell.component[0]== (Rayform.Int4)11) ? (Rayform.Int4)7 : calcdcell.component[0];
				calcdcell.component[2]= (selection.Contains(east)  && calcdcell.component[2]== (Rayform.Int4)11) ? (Rayform.Int4)7 : calcdcell.component[2];
				calcdcell.component[3]= (selection.Contains(south) && calcdcell.component[3]== (Rayform.Int4)11) ? (Rayform.Int4)7 : calcdcell.component[3];
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		private void mirrorTile(string direction) 
		{
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				Rayform.Tilemap.Cell oldcell = tilemap.cells[pos.x, pos.y];
				Rayform.Tilemap.Cell newcell = new Rayform.Tilemap.Cell(true);
				newcell.set = oldcell.set.ToArray();
				if(direction == "N" || direction == "S") {
					newcell.component[1] = oldcell.component[3]; //N = S
					newcell.component[3] = oldcell.component[1]; //S = N
					//NE = SE
					newcell.component[6] = (oldcell.component[7] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[7];
					newcell.component[6] = (oldcell.component[7] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[6];
					//SE = NE
					newcell.component[7] = (oldcell.component[6] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[6];
					newcell.component[7] = (oldcell.component[6] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[7];
					//NW = SW
					newcell.component[5] = (oldcell.component[4] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[4];
					newcell.component[5] = (oldcell.component[4] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[5];
					//SW = NW
					newcell.component[4] = (oldcell.component[5] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[5];
					newcell.component[4] = (oldcell.component[5] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[4];
					newcell.component[0] = oldcell.component[0]; //W = W 
					newcell.component[2] = oldcell.component[2]; //E = E 
					newcell.variant[0] = oldcell.variant[0]; //W = W 
					newcell.variant[2] = oldcell.variant[2]; //E = E 
					newcell.variant[1] = oldcell.variant[3]; //N = S
					newcell.variant[3] = oldcell.variant[1]; //S = N
					newcell.variant[6] = oldcell.variant[7]; //NE = SE
					newcell.variant[7] = oldcell.variant[6]; //SE = NE
					newcell.variant[5] = oldcell.variant[4]; //NW = SW
					newcell.variant[4] = oldcell.variant[5]; //SW = NW
				}
				if(direction == "E" || direction == "W") {
					newcell.component[0] = oldcell.component[2]; //W = E
					newcell.component[2] = oldcell.component[0]; //E = W
					//SW = SE
					newcell.component[4] = (oldcell.component[7] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[7];
					newcell.component[4] = (oldcell.component[7] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[4];
					//SE = SW
					newcell.component[7] = (oldcell.component[4] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[4];
					newcell.component[7] = (oldcell.component[4] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[7];
					//NW = NE
					newcell.component[5] = (oldcell.component[6] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[6];
					newcell.component[5] = (oldcell.component[6] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[5];
					//NE = NW
					newcell.component[6] = (oldcell.component[5] == (Rayform.Int4)9) ? (Rayform.Int4)10 : oldcell.component[5];
					newcell.component[6] = (oldcell.component[5] == (Rayform.Int4)10) ? (Rayform.Int4)9 : newcell.component[6];
					newcell.component[1] = oldcell.component[1]; //N = N
					newcell.component[3] = oldcell.component[3]; //S = S 
					newcell.variant[1] = oldcell.variant[1]; //N = N
					newcell.variant[3] = oldcell.variant[3]; //S = S 
					newcell.variant[0] = oldcell.variant[2]; //W = E
					newcell.variant[2] = oldcell.variant[0]; //E = W
					newcell.variant[4] = oldcell.variant[7]; //SW = SE
					newcell.variant[7] = oldcell.variant[4]; //SE = SW
					newcell.variant[5] = oldcell.variant[6]; //NW = NE
					newcell.variant[6] = oldcell.variant[5]; //NE = NW
				}
				tilemap.cells[pos.x, pos.y] = newcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		private void rotateTile(int rotation)
        {
			PushUndoState();
			bool reversed = rotation < 0 ? true : false;
			rotation = Mathf.Abs(rotation);
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				Rayform.Tilemap.Cell oldcell = tilemap.cells[pos.x, pos.y];
				Rayform.Tilemap.Cell newcell = new Rayform.Tilemap.Cell(true);
				newcell.set = oldcell.set.ToArray();
				if (rotation % 90 == 0)
				{
					int rotations = rotation / 90;
					for (int ii = 0; ii < rotations; ii++)
					{
						if(!reversed) {
							//There is a cleaner way to do this with loops, but it didn't want to work.
							//Component Walls
							newcell.component[0] = oldcell.component[1];
							newcell.component[1] = oldcell.component[2];
							newcell.component[2] = oldcell.component[3];
							newcell.component[3] = oldcell.component[0];
							//Component Corners
							newcell.component[4] = oldcell.component[5];
							newcell.component[5] = oldcell.component[6];
							newcell.component[6] = oldcell.component[7];
							newcell.component[7] = oldcell.component[4];
							//Variant Walls
							newcell.variant[0] = oldcell.variant[1];
							newcell.variant[1] = oldcell.variant[2];
							newcell.variant[2] = oldcell.variant[3];
							newcell.variant[3] = oldcell.variant[0];
							//Variant Corners
							newcell.variant[4] = oldcell.variant[5];
							newcell.variant[5] = oldcell.variant[6];
							newcell.variant[6] = oldcell.variant[7];
							newcell.variant[7] = oldcell.variant[4];
						}
						else 
						{
							//Component Walls
							newcell.component[3] = oldcell.component[2];
							newcell.component[2] = oldcell.component[1];
							newcell.component[1] = oldcell.component[0];
							newcell.component[0] = oldcell.component[3];
							//Component Corners
							newcell.component[7] = oldcell.component[6];
							newcell.component[6] = oldcell.component[5];
							newcell.component[5] = oldcell.component[4];
							newcell.component[4] = oldcell.component[7];
							//Variant Walls
							newcell.variant[3] = oldcell.variant[2];
							newcell.variant[2] = oldcell.variant[1];
							newcell.variant[1] = oldcell.variant[0];
							newcell.variant[0] = oldcell.variant[3];
							//Variant Corners
							newcell.variant[7] = oldcell.variant[6];
							newcell.variant[6] = oldcell.variant[5];
							newcell.variant[5] = oldcell.variant[4];
							newcell.variant[4] = oldcell.variant[7];
						}
						oldcell.component = newcell.component;
						oldcell.variant   = newcell.variant;
					}
					tilemap.cells[pos.x, pos.y] = newcell;
					ExanimaEditor.ImportCell(pos.x, pos.y);
				}
			}
		}
		private void clearArea()
        {
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
				Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				
				Rayform.Tilemap.Cell calcdcell = new Rayform.Tilemap.Cell(true);
				
				Vector2Int north = new Vector2Int(pos.x, pos.y - 1);
				Vector2Int west  = new Vector2Int(pos.x - 1, pos.y);
				Vector2Int east  = new Vector2Int(pos.x + 1, pos.y);
				Vector2Int south = new Vector2Int(pos.x, pos.y + 1);
				
				Vector2Int northwest = new Vector2Int(pos.x - 1, pos.y - 1);
				Vector2Int northeast = new Vector2Int(pos.x + 1, pos.y - 1);
				Vector2Int southwest = new Vector2Int(pos.x - 1, pos.y + 1);
				Vector2Int southeast = new Vector2Int(pos.x + 1, pos.y + 1);
				
				//Blank space layout based on those in Exanima01.rsg, but I'll trust the default cell.
				//calcdcell.component[1]= (Rayform.Int4)15;
				//calcdcell.component[0]= (Rayform.Int4)15;
				//calcdcell.component[2]= (Rayform.Int4)15;
				//calcdcell.component[3]= (Rayform.Int4)15;
				
				tilemap.cells[pos.x, pos.y] = calcdcell;
				ExanimaEditor.ImportCell(pos.x, pos.y);
			}
		}
		//Nyaaaaa~            

        private void Apply()
        {
			PushUndoState();
            foreach(Vector2Int pos in selection)
            {
                Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
				//Reverse changes if we don't have the appropriate apply selectioned -Yew
				//This is a silly fix.
				tilemap.cells[pos.x, pos.y].component = cell.component.ToArray();
				tilemap.cells[pos.x, pos.y].variant   = cell.variant.ToArray();  
				tilemap.cells[pos.x, pos.y].set       = cell.set.ToArray();	  
				if(applyComponent) { tilemap.cells[pos.x, pos.y].component = cell.component.ToArray(); }
				if(applyVariant  ) { tilemap.cells[pos.x, pos.y].variant   = cell.variant.ToArray();   }
				if(applySet      ) { tilemap.cells[pos.x, pos.y].set       = cell.set.ToArray();	   }
				//Wey
                ExanimaEditor.ImportCell(pos.x, pos.y);
            }
            SceneVisibilityManager.instance.DisablePicking(Tag.Find("Tilemap"), true);
            //foreach(Vector2Int tileVec in selection)
            //{
            //    int x = tileVec.x;
            //    int y = tileVec.y;
            //    Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
            //    tilemap.TileComponents[x, y] = tileComponent;
            //    tilemap.TileVariants[x, y] = tileVariant;
            //    tilemap.TileSets[x, y] = tileSet;
            //
            //    ExanimaEditor.ImportTile(x, y);
            //}
        }
        void OnSceneGUI(SceneView sceneView)
        {
            if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.LeftControl)
            {
                ctrlPressed = true;
            }

            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.LeftControl)
            {
                ctrlPressed = false;
            }

            if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.LeftShift)
            {
                shiftPressed = true;
            }

            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.LeftShift)
            {
                shiftPressed = false;
            }

            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                mousePressed = true;
            }

            if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
            {
                mousePressed = false;
            }
            //Debug.Log(mousePressed);
            //int x = selection.Last().x;
            //int y = selection.Last().y;

            Rayform.Tilemap tilemap = ExanimaEditor.tilemap;
            if (disableObjects)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            }

            // Copy and paste NYEW IMPLEMENTATION!!!!!! <3 <3 <3 >3<
            if (ctrlPressed)
            {
                if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.C)
                {
					Event.current.Use();
					copyCell = new List<Rayform.Tilemap.Cell>();
					copyPos = new HashSet<Vector2Int> { Vector2Int.zero };
					foreach (Vector2Int pos in selection) {
						Vector2Int relativePos = pos - selection.First();
						Rayform.Tilemap.Cell copiedCell = new Rayform.Tilemap.Cell
						{
							component = tilemap.cells[pos.x, pos.y].component.ToArray(),
							variant = tilemap.cells[pos.x, pos.y].variant.ToArray(),
							set = tilemap.cells[pos.x, pos.y].set.ToArray()
						};
						copyCell.Add(copiedCell);
						copyPos.Add(relativePos);
					}
                }
                if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.V)
                {
                    Event.current.Use();
					if (copyCell != null && copyPos != null)
					{
						PushUndoState();
						Vector2Int pastePosition = selection.First();
						foreach (var (copiedCell, relativePos) in copyCell.Zip(copyPos, (cell, pos) => (cell, pos)))
						{
							Vector2Int pastePos = pastePosition + relativePos;
							if (pastePos.x >= 0 && pastePos.x < tilemap.gridWidth && pastePos.y >= 0 && pastePos.y < tilemap.gridHeight)
							{
								tilemap.cells[pastePos.x, pastePos.y].component = copiedCell.component.ToArray();
								tilemap.cells[pastePos.x, pastePos.y].variant = copiedCell.variant.ToArray();
								tilemap.cells[pastePos.x, pastePos.y].set = copiedCell.set.ToArray();
							}
							ExanimaEditor.ImportCell(pastePos.x, pastePos.y);
						}
					}
                    SceneVisibilityManager.instance.DisablePicking(Tag.Find("Tilemap"), true);
                }
            }
			// Nya! Nya! Nyaaaa!!!
            // Select
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                //Debug.Log("Left-Mouse Down");
                //Vector3 mousePosition = Camera.current.ScreenToWorldPoint(Event.current.mousePosition);
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                //Vector3 mousePosition = Event.current.mousePosition;
                //mousePosition.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePosition.y;
                //mousePosition = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(mousePosition);
                //mousePosition.y = -mousePosition.y;
                //Debug.Log(mousePosition);

                Plane plane = new Plane(Vector3.up, Vector3.zero);

                float distance = 0.0f;
                if (plane.Raycast(ray, out distance))
                {
                    // Get the intersection point
                    Vector3 intersection = ray.GetPoint(distance);
                    int x = Math.Clamp((int)-((intersection.x / (tilemap.gridSize / 100) + tilemap.gridWidth / 2) - tilemap.gridWidth), 0, tilemap.gridWidth-1);
                    int y = Math.Clamp((int)(intersection.z / (tilemap.gridSize / 100) + tilemap.gridHeight / 2), 0, tilemap.gridHeight-1);
                    Vector2Int target = new Vector2Int(x, y);
                    Vector2Int origin = selection.Last();

                    // Multiselect or replace selection
                    if (!ctrlPressed)
                    {
                        selection.Clear();
                    }
                    // Single selection
                    if (!shiftPressed)
                    {
                        if (selection.Contains(target) && selection.Count > 1)
                        {
                            selection.Remove(target);
                        }
                        else
                        {
                            selection.Add(target);
                        }
                    }
                    // Square selection
                    else
                    {
                        selection.UnionWith(GetSquareSelection(origin, target));
                    }

                    cell = tilemap.cells[selection.Last().x, selection.Last().y];

                    //Debug.Log(x);

                    //Debug.Log("Intersection point: " + intersection);
                }
            }

            // Delete
            if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Delete)
            {
				clearArea();
                SceneVisibilityManager.instance.DisablePicking(Tag.Find("Tilemap"), true);
            }
			
            foreach (Vector2Int tileVector in selection)
            {
                int gridsize = tilemap.gridSize / 100;
                int worldX = -(tileVector.x - tilemap.gridWidth / 2 + 1) * gridsize;
                int worldY = (tileVector.y - tilemap.gridHeight / 2) * gridsize;
                Vector3 origin = new Vector3(worldX, 0, worldY);
                Handles.color = new Color(1, 0.5f, 0);
				
                Handles.DrawLine(origin, origin + Vector3.right * gridsize, 1);
                Handles.DrawLine(origin + Vector3.forward * gridsize, origin + Vector3.forward * gridsize + Vector3.right * gridsize, 1);
                Handles.DrawLine(origin, origin + Vector3.forward * gridsize, 1);
                Handles.DrawLine(origin + Vector3.right * gridsize, origin + Vector3.forward * gridsize + Vector3.right * gridsize, 1);
			
				//~Yew
				//Put directions on tiles. Gonna have to deal with the labels sticking to the screen.
				if (enableCompass) {
					Vector3 middlePointHorizontal = origin + Vector3.right * gridsize * 0.5f;
					Vector3 middlePointVertical = origin + Vector3.forward * gridsize * 0.5f;
					float inset = 0.1f * gridsize;
					Handles.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
					GUIStyle compassStyle = new GUIStyle();
					compassStyle.normal.textColor = new Color(0, 0.5f, 1);
					Handles.Label(middlePointHorizontal + Vector3.forward * inset, "N", compassStyle);
					Handles.Label(middlePointHorizontal + Vector3.forward * gridsize - Vector3.forward * inset, "S", compassStyle);
					Handles.Label(middlePointVertical + Vector3.right * inset, "E", compassStyle);
					Handles.Label(middlePointVertical + Vector3.right * gridsize - Vector3.right * inset, "W", compassStyle);
					Handles.matrix = Matrix4x4.identity;
				}
			}
			//Clipboard Ghosting
			if(clipboardView && copyPos.Count > 0)
			{
				foreach (Vector2Int pos in copyPos)
				{
					int gridsize = (tilemap.gridSize / 100);
					Vector2Int relativePos = selection.First();
					int worldX = -(relativePos.x + pos.x - tilemap.gridWidth / 2 + 1) * gridsize;
					int worldY = (relativePos.y + pos.y - tilemap.gridHeight / 2) * gridsize;
					Vector3 originalOrigin = new Vector3(worldX, 0, worldY);
					Handles.color = new Color(0, 0.8f, 0.8f, 0.48f);

					float shrinkFactor = 0.04f;

					Vector3 rightVector = Vector3.right * gridsize * (1 - shrinkFactor);
					Vector3 forwardVector = Vector3.forward * gridsize * (1 - shrinkFactor);

					Vector3 offset = (Vector3.right * gridsize + Vector3.forward * gridsize) * shrinkFactor * 0.5f;

					Vector3 centeredOrigin = originalOrigin + offset;

					Handles.DrawLine(centeredOrigin, centeredOrigin + rightVector, 1);
					Handles.DrawLine(centeredOrigin + forwardVector, centeredOrigin + forwardVector + rightVector, 1);
					Handles.DrawLine(centeredOrigin, centeredOrigin + forwardVector, 1);
					Handles.DrawLine(centeredOrigin + rightVector, centeredOrigin + forwardVector + rightVector, 1);
					
					if(pos == copyPos.First())
					//First Selection orientation.
					shrinkFactor = 0.85f;
					rightVector = Vector3.right * gridsize * (1 - shrinkFactor);
					forwardVector = Vector3.forward * gridsize * (1 - shrinkFactor);
					offset = (Vector3.right * gridsize + Vector3.forward * gridsize) * shrinkFactor * 0.5f;
					centeredOrigin = originalOrigin + offset;
					
					Handles.color = new Color(0, 0, 0, 0.50f);
					
					Handles.DrawLine(centeredOrigin, centeredOrigin + forwardVector, 1);
					Handles.DrawLine(centeredOrigin + forwardVector, centeredOrigin + forwardVector + rightVector, 1);
					Handles.DrawLine(centeredOrigin, centeredOrigin + rightVector, 1);
					Handles.DrawLine(centeredOrigin + rightVector, centeredOrigin + forwardVector + rightVector, 1);
				}
			}
			//...Nya.
            HandleUtility.Repaint();
            //Handles.BeginGUI();
            // GUI Code here
            //Handles.EndGUI();

            //selection[selection.Count-1] = new Vector2Int(x, y);
        }
    }

    static List<Vector2Int> GetSquareSelection(Vector2Int origin, Vector2Int target)
    {
        List<Vector2Int> selection = new List<Vector2Int>();

        int minX = Math.Min(origin.x, target.x);
        int maxX = Math.Max(origin.x, target.x);
        int minY = Math.Min(origin.y, target.y);
        int maxY = Math.Max(origin.y, target.y);

        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                selection.Add(new Vector2Int(x, y));
            }
        }

        return selection;
    }

    public class ObjectsGUI : EditorWindow
    {
        RdbGUI gui = new RdbGUI(typeof(Rayform.RdbObjs));

        [MenuItem("Exanima/Objects")]
        static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Objects";
            window.Show();
        }

        void OnGUI()
        {
            gui.Draw(position);
            this.Repaint();
        }
    }

    public class CharactersGUI : EditorWindow
    {
        RdbGUI gui = new RdbGUI(typeof(Rayform.RdbChars));

        [MenuItem("Exanima/Characters")]
        static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Characters";
            window.Show();
        }

        void OnGUI()
        {
            gui.Draw(position);
            this.Repaint();
        }
    }

    public class ObjStringsGUI : EditorWindow
    {
        RdbGUI gui = new RdbGUI(typeof(Rayform.RdbObjStrings));

        [MenuItem("Exanima/Obj Strings")]
        static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Obj Strings";
            window.Show();
        }

        void OnGUI()
        {
            gui.Draw(position);
            this.Repaint();
        }
    }

    public class RdbGUI
    {
        Rayform.Rdb rdb;
        Rayform.Id.Flags rdbFlags;
        Rayform.Save save = null;
        Rayform.Content locale = null;
        Rayform.Rdb.Entry selEntry = null;
        Vector2 scrollPosition;
        Vector2 scrollPosition2;
        Type type;
        string textSearch = "";

        public RdbGUI(Type type)
        {
            if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
            {
                save = (Rayform.Save)ExanimaEditor.file.data;
                locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == ExanimaEditor.selLocale).data;
            }
            else
            {
                locale = (Rayform.Content)ExanimaEditor.file.data;
            }

            this.type = type;
            rdb = Rayform.Rdb.FindRdb(locale, save, rdbFlags, type);
        }

        public void Draw(Rect position)
        {
            GUIStyle style = new GUIStyle { richText = true };
            Color selectedColor = new Color(0.25f, 0.5f, 0.75f, 1);
            Color borderColor = new Color(0.66f, 0.66f, 0.66f, 1);

            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(position.width / 2));
            rdbFlags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", rdbFlags);
            if (GUI.changed)
            {
                rdb = Rayform.Rdb.FindRdb(locale, save, rdbFlags, type);
                scrollPosition = Vector2.zero;
                selEntry = null;
            }
            textSearch = EditorGUILayout.TextField("Search", textSearch);

            GUILayout.Label("", GUI.skin.label);

            Rect lastRect = GUILayoutUtility.GetLastRect();
            lastRect.x += 4;
            lastRect.width -= 21;

            GUI.Label(new Rect(lastRect.position.x + 4, lastRect.position.y+1, lastRect.width, lastRect.height), "<b>ID</b>", style);
            EditorGUI.DrawRect(new Rect(lastRect.position.x, lastRect.position.y + 1, 1, lastRect.height - 1), borderColor);
            EditorGUI.DrawRect(new Rect(lastRect.position.x + lastRect.width / 2, lastRect.position.y, 1, lastRect.height), borderColor);
            EditorGUI.DrawRect(new Rect(lastRect.position.x + lastRect.width, lastRect.position.y + 1, 1, lastRect.height - 1), borderColor);

            EditorGUI.DrawRect(new Rect(lastRect.position.x + 1, lastRect.position.y, lastRect.width - 1, 1), borderColor);
            EditorGUI.DrawRect(new Rect(lastRect.position.x + 1, lastRect.position.y + lastRect.height, lastRect.width - 1, 1), borderColor);

            GUI.Label(new Rect(lastRect.position.x + lastRect.width / 2 + 4, lastRect.position.y+1, lastRect.width, lastRect.height), "<b>Name</b>", style);

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            if (rdb != null)
            {
                lastRect.x -= 7;
                // Create the table
                for (int i = 0; i < rdb.entries.Count; i++)
                {
                    Rayform.Rdb.Entry entry = rdb.entries[i];
                    string name = "";
                    int id = rdb.entries[i].baseId;
                    if (type == typeof(Rayform.RdbObjs))
                    {
                        Rayform.Thing thing = ((Rayform.ObjClass)entry.data).classes.OfType<Rayform.Thing>().FirstOrDefault();
                        if(thing == null)
                        {
                            Debug.Log("Object of ID " + entry.baseId + " has no Thing class!");
                            ((Rayform.ObjClass)entry.data).classes.Add(new Rayform.Thing());
                        }
                        else
                        {
                            name = thing.thingName;
                        }
                    }
                    else if (type == typeof(Rayform.RdbChars))
                    {
                        name = ((Rayform.Character)entry.data).name;
                    }
                    else if (type == typeof(Rayform.RdbObjStrings))
                    {
                        name = (string)entry.data;
                        id = i + 1;
                    }
                    if(name == null)
                    {
                        name = "";
                    }

                    if (!name.Contains(textSearch,StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    bool selected = selEntry == rdb.entries[i];
                    if (GUILayout.Button("", GUI.skin.label))
                    {
                        GUI.FocusControl(null);
                        scrollPosition2 = Vector2.zero;
                        selEntry = entry;
                    }
                    lastRect.y = GUILayoutUtility.GetLastRect().y;

                    if (selected)
                    {
                        EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y, lastRect.width - 1, lastRect.height), selectedColor);
                    }


                    EditorGUI.DrawRect(new Rect(lastRect.x, lastRect.y + 1, 1, lastRect.height - 1), borderColor);
                    EditorGUI.DrawRect(new Rect(lastRect.x + lastRect.width / 2, lastRect.y, 1, lastRect.height), borderColor);
                    EditorGUI.DrawRect(new Rect(lastRect.x + lastRect.width, lastRect.y + 1, 1, lastRect.height - 1), borderColor);

                    EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y, lastRect.width - 1, 1), borderColor);
                    EditorGUI.DrawRect(new Rect(lastRect.x + 1, lastRect.y + lastRect.height, lastRect.width - 1, 1), borderColor);

                    if (name.Length > 32)
                    {
                        name = name.Substring(0, 32) + "...";
                    }

                    GUI.Label(new Rect(lastRect.x + lastRect.x, lastRect.y, lastRect.width, lastRect.height), id.ToString());
                    GUI.Label(new Rect(lastRect.x + lastRect.width / 2+4, lastRect.y, lastRect.width, lastRect.height), name);
                }
            }
            GUILayout.EndScrollView();
            if (GUILayout.Button("New"))
            {
                GUI.FocusControl(null);
                selEntry = new Rayform.Rdb.Entry();

                if (type == typeof(Rayform.RdbObjs))
                {
                    Rayform.ObjClass objClass = new Rayform.ObjClass();
                    objClass.classes.Add(new Rayform.Thing());
                    selEntry.data = objClass;
                    selEntry.baseId = ExanimaEditor.FirstAvailableId(rdb);

                }
                else if (type == typeof(Rayform.RdbChars))
                {
                    scrollPosition2 = Vector2.zero;
                    selEntry.data = new Rayform.Character();
                    selEntry.baseId = ExanimaEditor.FirstAvailableId(rdb);
                }
                else if (type == typeof(Rayform.RdbObjStrings))
                {
                    selEntry.data = "";
                }
                rdb.entries.Add(selEntry);
                rdb.entries = rdb.entries.OrderBy(x => x.baseId).ToList();
            }
            GUILayout.EndVertical();


            if (selEntry != null)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(position.width / 2));
                selEntry.baseId = EditorGUILayout.IntField("Base Id", selEntry.baseId);
                selEntry.entryType = (Rayform.Rdb.Entry.EntryType)EditorGUILayout.EnumPopup("Entry Type", selEntry.entryType);

                // Copy
                Rayform.Id.Flags selRdb = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Copy to Rdb", (Rayform.Id.Flags)1);
                if(((ushort)selRdb) != 1)
                {
                    Rayform.Rdb.Entry newEntry = new Rayform.Rdb.Entry();
                    //rdb.entries.Remove(selEntry);
                    Rayform.Rdb newRdb = Rayform.Rdb.FindRdb(locale, save, selRdb, type);
                    if(newRdb != null)
                    {
                        // Unsafe and horrible practice, but who cares
                        object newData = Utils.DeepCopy(selEntry.data);
                        // This will save you alot of headaches
                        if (newData.GetType() == typeof(Rayform.ObjClass) && rdbFlags == Rayform.Id.Flags.Global)
                        {
                            Rayform.Thing thing = ((Rayform.ObjClass)newData).classes.OfType<Rayform.Thing>().FirstOrDefault();
                            thing.parentId.baseId = (ushort)selEntry.baseId;
                            Rayform.Object obj = ((Rayform.ObjClass)newData).classes.OfType<Rayform.Object>().FirstOrDefault();
                            if (obj != null)
                            {
                                obj.nameId = new Rayform.Id { baseId = 0, flags = Rayform.Id.Flags.Local };
                                obj.descId = new Rayform.Id { baseId = 0, flags = Rayform.Id.Flags.Local };
                            }
                        }
                        newEntry = new Rayform.Rdb.Entry();
                        if(type != typeof(Rayform.RdbObjStrings))
                        {
                            newEntry.baseId = ExanimaEditor.FirstAvailableId(newRdb);
                        }
                        newEntry.entryType = selEntry.entryType;
                        newEntry.data = newData;
                        newRdb.entries.Add(newEntry);
                        newRdb.entries = newRdb.entries.OrderBy(x => x.baseId).ToList();
                    }
                }
                if ((type == typeof(Rayform.RdbObjs) || type == typeof(Rayform.RdbChars)) && GUILayout.Button("Place"))
                {
                    GameObject obj = null;
                    if (type == typeof(Rayform.RdbObjs))
                    {
                        obj = Object.Import(new Rayform.Id { baseId = (ushort)selEntry.baseId, flags = rdbFlags });
                    }
                    else if (type == typeof(Rayform.RdbChars))
                    {
                        obj = Character.Import(new Rayform.Id { baseId = (ushort)selEntry.baseId, flags = rdbFlags }); 
                    }
                    if (obj != null)
                    {
                        placeAtCamera(obj);

                        Selection.activeObject = obj;
                    }
                    
                    GUI.FocusControl(null);
                }
                if (GUILayout.Button("Delete"))
                {
                    GUI.FocusControl(null);
                    rdb.entries.Remove(selEntry);
                    selEntry = null;
                }
                else
                {
                    scrollPosition2 = GUILayout.BeginScrollView(scrollPosition2);
                    if (type == typeof(Rayform.RdbObjs))
                    {
                        Object.ObjectFields((Rayform.ObjClass)selEntry.data);
                    }
                    else if(type == typeof(Rayform.RdbChars))
                    {
                        Character.CharacterFields((Rayform.Character)selEntry.data);
                    }
                    else if (type == typeof(Rayform.RdbObjStrings))
                    {
                        GUILayout.Label("Text");
                        selEntry.data = EditorGUILayout.TextArea((string)selEntry.data);
                    }
                    GUILayout.EndScrollView();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();
        }
    }

    //public class IdFields
    //{
    //
    //}

    public static void ListGUI<T>(List<T> list, Func<T, T> func)
    {
        for (int i = 0; i < list.Count; i++)
        {
            GUILayout.BeginHorizontal(EditorStyles.helpBox);
                GUILayout.BeginVertical();
                    //character.roles[i].id.baseId = (ushort)EditorGUILayout.IntField("Base Id", character.roles[i].id.baseId);
                    list[i] = func(list[i]);
                    GUILayout.EndVertical();
                //GUILayout.FlexibleSpace();
                GUILayout.BeginVertical(GUILayout.Width(20));
                    if (GUILayout.Button("X", GUILayout.Width(20)) && list.Count > 0)
                    {
                        list.RemoveAt(i);
                    }
                    if (GUILayout.Button("↑", GUILayout.Width(20)) && i > 0)
                    {
                        var item = list[i];
                        list.RemoveAt(i);
                        list.Insert(i - 1, item);
                    }
                    if (GUILayout.Button("↓", GUILayout.Width(20)) && i < list.Count-1)
                    {
                        var item = list[i];
                        list.RemoveAt(i);
                        list.Insert(i + 1, item);
                    }
                GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("+", GUILayout.Width(20)))
        {
            //list.roles.Add(new Rayform.Character.Role());
            list.Add((T)Activator.CreateInstance(typeof(T)));
        }
        if (GUILayout.Button("-", GUILayout.Width(20)) && list.Count > 0)
        {
            list.RemoveRange(list.Count - 1, 1);
        }
        EditorGUILayout.EndHorizontal();
    }
	/*
	  This is for yewsir friendlyness.
	  By assigning Unity Layers to the objects loaded,
	  we can then hide/show them at will in the Unity editor.
	  Editing is easier without 1 billion objects on screen.
	  Also, this runs incredibly fast, much faster then doing it on object load.
	*/
	public class LayerLoader : EditorWindow
	{
		private static List<GameObject> rayforms;

		[MenuItem("Exanima/Layerize/Confirm")]
		private static void Load()
		{
			rayforms = new List<GameObject>();
			GameObject objLibNode    = GameObject.Find("LibNodes"  );
			GameObject objCharacters = GameObject.Find("Characters");
			GameObject objObjects    = GameObject.Find("Objects"   );
			GameObject objScene      = GameObject.Find("Scene"     );
			GameObject objTilemap    = GameObject.Find("Tilemap"   );
			rayforms.Add(objLibNode    != null ? objLibNode    : LogErrorAndReturnNull("LibNodes Not Found!"  ));
			rayforms.Add(objCharacters != null ? objCharacters : LogErrorAndReturnNull("Characters Not Found!"));
			rayforms.Add(objObjects    != null ? objObjects    : LogErrorAndReturnNull("Objects Not Found!"   ));
			rayforms.Add(objScene      != null ? objScene      : LogErrorAndReturnNull("Scene Not Found!"     ));
			rayforms.Add(objTilemap    != null ? objTilemap    : LogErrorAndReturnNull("Tilemap Not Found!"   ));
			foreach (GameObject obj in rayforms)
			{
				if (obj != null)
				{
					LayerPlague(obj, obj.name);
				}
			}
		}
		private static void LayerPlague(GameObject obj, string layerName)
		{
			obj.layer = LayerMask.NameToLayer(layerName);
			foreach (Transform child in obj.transform)
			{
				LayerPlague(child.gameObject, layerName);
			}
		}
		private static GameObject LogErrorAndReturnNull(string message)
		{
			Debug.LogError(message);
			return null;
		}
	}
	//Nyaaaa~~~ :3
    public class Test : EditorWindow
    {
        public List<Rayform.HeldItem> items = new List<Rayform.HeldItem>();
        public byte selColor;

        [MenuItem("Exanima/Test")]
        static void ShowWindow()
        {
            EditorWindow window = GetWindow(MethodBase.GetCurrentMethod().DeclaringType);
            window.titleContent.text = "Characters";
            window.Show();
        }

        void OnGUI()
        {

            //Func<Rayform.HeldItem, Rayform.HeldItem> func = item =>
            //{
            //    item.flags = (Rayform.ObjClass.Flags)EditorGUILayout.EnumPopup("Item Flag", item.flags);
            //    item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.id.baseId);
            //    item.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", item.id.flags);
            //    return item;
            //};
            //
            //ListGUI(items, func);

            //Color bgColor = GUI.backgroundColor;
            //GUILayout.BeginHorizontal();
            //
            //GUIStyle btnStyle = new GUIStyle(GUI.skin.box);
            //btnStyle.fixedWidth = 10;
            //btnStyle.fixedHeight = 10;
            //btnStyle.margin = new RectOffset(1,1,1,1);
            //
            //for (int i = 0; i < Utils.palette.Count; i++)
            //{
            //    if ((i % 8) == 0)
            //    {
            //        GUILayout.BeginVertical();
            //    }
            //
            //    GUI.backgroundColor = Utils.palette[i];
            //    //Debug.Log(Utils.palette[i]);
            //    if (GUILayout.Button("", btnStyle))
            //    {
            //        //material.color = i;
            //    }
            //
            //    if (((i + 1) % 8) == 0 || i == Utils.palette.Count-1)
            //    {
            //        GUILayout.EndVertical();
            //    }
            //}
            //GUILayout.FlexibleSpace();
            //GUILayout.EndHorizontal();
            //GUI.backgroundColor = bgColor;


            //EditorGUI.DrawRect(new Rect(paletteOrigin.x, paletteOrigin.y, 1, 16), Utils.palette[0]);
            if (GUILayout.Button("PENIS"))
            {
                
            }

            selColor = ColorPalette(selColor);

            this.Repaint();
        }
    }
	
    public static byte ColorPalette(byte selColor)
    {
        int squareSize = 16;
        int rows = 32;
        int cols = 8;
        int margin = 1;

        GUILayout.Space(0);
        Vector2 paletteOrigin = GUILayoutUtility.GetLastRect().position;
        paletteOrigin.y += 3;

        bool select = false;
        if (GUILayout.Button("", GUI.skin.box, GUILayout.Width(rows * (squareSize + margin)), GUILayout.Height(cols * (squareSize + margin))))
        {
            select = true;
        }
        
        //Debug.Log("palette origin: " + paletteOrigin + " mouse pos: " + Event.current.mousePosition + " " + (Event.current.mousePosition.y - paletteOrigin.y));

        if (select)
        {
            //material.color = (byte)
            int mouseX = Math.Clamp((int)((Event.current.mousePosition.x - paletteOrigin.x) / (squareSize + margin)), 0, rows - 1);
            int mouseY = Math.Clamp((int)((Event.current.mousePosition.y - paletteOrigin.y) / (squareSize + margin)), 0, cols - 1);
            selColor = (byte)(mouseX * cols + mouseY);
            //Debug.Log(paletteOrigin.y);
            //Debug.Log(mouseX + " " + mouseY);
        }
        //paletteOrigin.y += 16;

        EditorGUI.DrawRect(new Rect(paletteOrigin.x, paletteOrigin.y, rows * (squareSize + margin) + margin, cols * (squareSize + margin) + margin), Color.black);

        int x = 0;
        int y = 0;
        for (int i = 0; i < Utils.palette.Count; i++)
        {
            Rect squareRect = new Rect(
                x * (squareSize + margin),
                y * (squareSize + margin),
                squareSize,
                squareSize);

            if (i == selColor)
            {
                EditorGUI.DrawRect(new Rect(paletteOrigin.x + (x * (squareSize + margin)), paletteOrigin.y + (y * (squareSize + margin)), squareSize + margin * 2, squareSize + margin * 2), new Color(1, 0.75f, 0, 1));
            }

            EditorGUI.DrawRect(new Rect(paletteOrigin.x + (x * (squareSize + margin)) + margin, paletteOrigin.y + (y * (squareSize + margin)) + margin, squareSize, squareSize), Utils.palette[i]);
            y++;
            if (y == cols)
            {
                x++;
                y = 0;
            }
        }

        //int selX = selColor / cols;
        //int selY = selColor % cols;
        //EditorGUI.DrawRect(new Rect(paletteOrigin.x + (selX * (squareSize + margin)), paletteOrigin.y + (selY * (squareSize + margin)), squareSize + margin * 2, margin), new Color(1, 0.75f, 0, 1));
        //EditorGUI.DrawRect(new Rect(paletteOrigin.x + (selX * (squareSize + margin)), paletteOrigin.y + ((selY + 1) * (squareSize + margin)), squareSize + margin * 2, margin), new Color(1, 0.75f, 0, 1));
        //
        //EditorGUI.DrawRect(new Rect(paletteOrigin.x + (selX * (squareSize + margin)), paletteOrigin.y + (selY * (squareSize + margin)), margin, squareSize + margin * 2), new Color(1, 0.75f, 0, 1));
        //EditorGUI.DrawRect(new Rect(paletteOrigin.x + ((selX + 1) * (squareSize + margin)), paletteOrigin.y + (selY * (squareSize + margin)), margin, squareSize + margin * 2), new Color(1, 0.75f, 0, 1));

        return selColor;
    }

    public static void placeAtCamera(GameObject obj)
    {
        obj.transform.Reset();
        Ray ray = SceneView.lastActiveSceneView.camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        // create a plane at 0,0,0 whose normal points to +Y:
        Plane hPlane = new Plane(Vector3.up, Vector3.zero);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;
        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            // get the hit point:
            obj.transform.position = ray.GetPoint(distance);
        }
    }
	
}