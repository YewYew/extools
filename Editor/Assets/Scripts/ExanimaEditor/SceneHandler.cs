using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;

partial class ExanimaEditor
{
    public Rayform.Save save;
    public bool test;
    // Start is called before the first frame update
    public static GameObject ImportScene(Rayform.Scene scene, string name)
    {
        if (scene == null) return null;

        List<GameObject> bones = new List<GameObject>();
        bones.Add(new GameObject
        {
            name = name,
            //hideFlags = HideFlags.HideAndDontSave,
        });

        //Rayform.Mesh[] meshes = scene.chunks.OfType<Rayform.Mesh>().ToArray();
        for (int i = 0; i < scene.nodes.Count; i++)
        {
            Rayform.Scene.Node RFNode = scene.nodes[i];

            GameObject nodeObj = new GameObject(RFNode.nodeName);
            int parent = RFNode.parent;
            nodeObj.transform.parent = bones[parent].transform;
            Matrix4x4 matrix = Utils.ConvertTo4x4(RFNode.objTransform);
            nodeObj.transform.FromMatrix(matrix, true);
            //if (parent >= bones.Count)
            //{
            //    parent = bones.Count-1;
            //}
            bones.Add(nodeObj);

            if (RFNode.GetType() == typeof(Rayform.Mesh))
            {
                Rayform.Mesh RFmesh = (Rayform.Mesh)scene.nodes[i];

                Rayform.Mesh.Prop prop = RFmesh.chunks.OfType<Rayform.Mesh.Prop>().FirstOrDefault();
                if (prop == null)
                {
                    continue;
                }

                UnityEngine.Mesh mesh = new UnityEngine.Mesh();
                mesh.name = RFmesh.nodeName;

                Vector3[] vertices = new Vector3[prop.cmpVrts];
                for (int j = 0; j < vertices.Length; j++)
                {
                    vertices[j] = RFmesh.vrtDats[prop.vrtMaps[j]].ConvertVector3();
                }

                mesh.SetVertices(vertices);
                mesh.subMeshCount = prop.vrtGroups.Count;

                for (int j = 0; j < prop.poly.submeshes.Count; j++)
                {
                    int start = prop.poly.submeshes[j].start;
                    int end = prop.poly.submeshes[j].end;

                    int[] triangles = new int[(end - start + 1) * 3];

                    for (int k = 0; k < triangles.Length / 3; k++)
                    {
                        triangles[k * 3] = prop.poly.triangles[(start + k) * 3 + 2];
                        triangles[k * 3 + 1] = prop.poly.triangles[(start + k) * 3 + 1];
                        triangles[k * 3 + 2] = prop.poly.triangles[(start + k) * 3];
                    }
                    mesh.SetTriangles(triangles, j);
                }

                List<UnityEngine.Material> selectedMaterials = new List<UnityEngine.Material>();
                {
                    foreach (Rayform.Mesh.VrtGroup vrtGroup in prop.vrtGroups)
                    {
                        Material material = ExanimaResources.materials.FirstOrDefault(x => String.Equals(x.name, vrtGroup.name_material, StringComparison.OrdinalIgnoreCase));
                        if (material == null)
                        {
                            //Debug.Log("pensisi " + vrtGroup.name_material);
                            var result = Rayform.PackageManager.LoadByName(vrtGroup.name_material, Rayform.PackageManager.PackageCategory.Materials);
                            if (result != null && result.data.GetType() == typeof(Rayform.Material))
                            {
                                material = ExanimaResources.ImportMaterial((Rayform.Material)result.data);
                            }
                        }
                        if (material == null)
                        {
                            material = new UnityEngine.Material(Shader.Find("Standard (Specular setup)"));
                            material.name = vrtGroup.name_material;
                            ExanimaResources.materials.Add(material);
                        }
                        selectedMaterials.Add(material);
                    }
                }

                Vector2[] uvs = new Vector2[vertices.Length];
                for (int j = 0; j < prop.uvMaps.Length; j++)
                {
                    uvs[j] = Utils.ConvertVector2(RFmesh.uvDats[prop.uvMaps[j]]);
                }
                //Debug.Log(RFmesh.uvDats.Length + " " + prop.cmpVrts);
                mesh.SetVertices(vertices);
                //mesh.SetTriangles(triangles, 0);
                mesh.SetUVs(0, uvs);
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();

                MeshFilter mf = nodeObj.AddComponent<MeshFilter>();
                MeshRenderer ms = nodeObj.AddComponent<MeshRenderer>();

                ms.materials = selectedMaterials.ToArray();
                mf.mesh = mesh;
                //testNode.transform.position = new Vector3(node.libobj_transform.x / 100, node.libobj_transform.y / 100, node.libobj_transform.z / 100);
            }
            else if (RFNode.GetType() == typeof(Rayform.LightSource))
            {
                Rayform.LightSource lightSource = (Rayform.LightSource)RFNode;
                LightSource light = nodeObj.AddComponent<LightSource>();
                light.nodeFlags = lightSource.nodeFlags;
                light.objFlags = lightSource.objFlags;
                light.lightFlags= lightSource.lightFlags;
                light.color = lightSource.color.ConvertColor();
                light.ambient = lightSource.ambient;
                light.brightness = lightSource.brightness;
                light.distance = lightSource.distance / 100;
                light.shadowSoften = lightSource.shadowSoften;
                //light.shadows = LightShadows.Soft;
                //light.light.type = LightType.Spot;
                light.spotCut = lightSource.spotCut;
                light.spotSoft = lightSource.spotSoft;
                //nodeObj.transform.Rotate(90, 0, 0);
                light.animStrength = lightSource.spotCut;
                light.animSpeed = lightSource.animSpeed;
                light.animEmission = lightSource.animEmission;
            }
            else if (RFNode.GetType() == typeof(Rayform.VoxelLight))
            {
                Rayform.VoxelLight voxelLight = (Rayform.VoxelLight)RFNode;
                VoxelLight vlight = nodeObj.AddComponent<VoxelLight>();
                vlight.nodeFlags = voxelLight.nodeFlags;
                vlight.objFlags = voxelLight.objFlags;
                vlight.unknown1 = voxelLight.unknown1;
                vlight.color = voxelLight.color.ConvertColor();
                vlight.multiplier = voxelLight.multiplier;
                vlight.attenDist = voxelLight.attenDist;
                vlight.unknown3 = voxelLight.unknown3;
            }
            else if (RFNode.GetType() == typeof(Rayform.Node))
            {
                Rayform.Node RFBone = (Rayform.Node)RFNode;
                if(RFNode.nodeName == "_Fire")
                {
                    LightSource light = nodeObj.AddComponent<LightSource>();
                    light.color = new Color(1, 0.75f, 0.5f);
                    light.brightness = 1;
                    light.distance = 10;
                    //light.range = 10;
                    //light.intensity = 1;
                }
            }
        }
        //GameObject.Destroy(bones[0]);
        return bones[0];
    }

    public static Rayform.Scene ExportScene(GameObject parent)
    {
        Rayform.Scene scene = new Rayform.Scene();
        foreach(Transform nodeObj in parent.transform.Children())
        {
            Rayform.Scene.Node RFNode = null;
            if(nodeObj.TryGetComponent(out LightSource light))
            {
                Rayform.LightSource lightSource = new Rayform.LightSource();

                lightSource.nodeFlags = light.nodeFlags;
                lightSource.objFlags = light.objFlags;
                lightSource.lightFlags = light.lightFlags;
                lightSource.color = light.color.ConvertColor();
                lightSource.ambient = light.ambient;
                lightSource.brightness = light.brightness;
                lightSource.distance = light.distance * 100;
                lightSource.shadowSoften = light.shadowSoften;
                lightSource.spotCut = light.spotCut;
                lightSource.spotSoft = light.spotSoft;
                lightSource.animStrength = light.spotCut;
                lightSource.animSpeed = light.animSpeed;
                lightSource.animEmission = light.animEmission;

                RFNode = lightSource;
            }
            if(nodeObj.TryGetComponent(out VoxelLight vlight))
            {
                Rayform.VoxelLight voxelLight = new Rayform.VoxelLight();

                voxelLight.nodeFlags = vlight.nodeFlags;
                voxelLight.objFlags = vlight.objFlags;
                voxelLight.unknown1 = vlight.unknown1;
                voxelLight.color = vlight.color.ConvertColor();
                voxelLight.multiplier = vlight.multiplier;
                voxelLight.attenDist = vlight.attenDist;
                voxelLight.unknown3 = vlight.unknown3;

                RFNode = voxelLight;
            }

            if (RFNode != null)
            {
                RFNode.objTransform = nodeObj.transform.ToMatrix(false).ConvertTo3x4();
                RFNode.nodeName = nodeObj.name;
                scene.nodes.Add(RFNode);
            }
        }
        return scene;
    }
        //private void OnValidate()
        //{
        //    Debug.Log(transform.localToWorldMatrix);
        //}
    }