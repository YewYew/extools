using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public List<GameObject> gameObjects = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnValidate()
    {
        Matrix4x4 matrix = transform.localToWorldMatrix;
        string matrixstr =
        "m00 " + matrix.m00 + '\n' +
        "m10 " + matrix.m10 + '\n' +
        "m20 " + matrix.m20 + '\n' +
        "m30 " + matrix.m30 + '\n' +
        "m01 " + matrix.m01 + '\n' +
        "m11 " + matrix.m11 + '\n' +
        "m21 " + matrix.m21 + '\n' +
        "m31 " + matrix.m31 + '\n' +
        "m02 " + matrix.m02 + '\n' +
        "m12 " + matrix.m12 + '\n' +
        "m22 " + matrix.m22 + '\n' +
        "m32 " + matrix.m32 + '\n' +
        "m03 " + matrix.m03 + '\n' +
        "m13 " + matrix.m13 + '\n' +
        "m23 " + matrix.m23 + '\n' +
        "m33 " + matrix.m33;

        Debug.Log(matrixstr);
        //Debug.Log(Vector3.up);
        //Color color = Color.red;
        //color.a = 0.5f;
        //Debug.Log(color);
    }
}
