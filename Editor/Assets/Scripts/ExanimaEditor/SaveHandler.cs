using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.TextCore.Text;

public partial class ExanimaEditor
{
    public static void ImportSave(Rayform.Save save)
    {
        GameObject saveGroup = new GameObject(file.fileName);

        Rayform.Playerdata playerData = (Rayform.Playerdata)save.sections.FirstOrDefault(x => x.name == "#playerdata").data;

        selLocale = playerData.locale;

        Rayform.Content locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == selLocale).data;
        GameObject localeGroup = ImportContent(locale, selLocale);
        localeGroup.transform.parent = saveGroup.transform;

        Rayform.CharacterPosition playerPosition = playerData.chunks.OfType<Rayform.CharacterPosition>().FirstOrDefault();
        GameObject playerObj = Character.Import(playerPosition);
        playerObj.transform.SetSiblingIndex(0);
        playerObj.tag = "Player";
        //for (int i = 0; i < save.sections.Count; i++)
        //{
        //    string name = save.sections[i].name;
        //    Debug.Log(name);
        //
        //    if (name == "#worldchrdb")
        //    {
        //
        //    }
        //    else if (name == "#worldobjdb")
        //    {
        //
        //    }
        //    else if (name == "#playerdata")
        //    {
        //        Rayform.Playerdata playerData = (Rayform.Playerdata)save.sections[i].data;
        //        Rayform.CharacterPosition playerPosition = playerData.chunks.OfType<Rayform.CharacterPosition>().FirstOrDefault();
        //        Rayform.Character data = (Rayform.Character)Rayform.PackageManager.FindRdbEntry(null, save, playerPosition.characterId, typeof(Rayform.Character)).data;
        //        GameObject characterObj = new GameObject(data.name);
        //        Character characterComp = characterObj.AddComponent<Character>();
        //        characterComp.id = playerPosition.characterId;
        //        characterComp.data = data;
        //        characterObj.transform.position = playerPosition.locOrigin.ConvertVector3();
        //        characterObj.transform.eulerAngles = new Vector3(0, playerPosition.locOrientation * Mathf.Rad2Deg, 0);
        //
        //        ImportedAsset humanAsset = ExanimaResources.LoadFromRpk("humanbase.rfc", typeof(GameObject));
        //        GameObject body = humanAsset.Instantiate();
        //        body.transform.parent = characterObj.transform;
        //        body.transform.Reset();
        //    }
        //    else
        //    {
        //        Rayform.Content locale = (Rayform.Content)save.sections[i].data;
        //        GameObject localeGroup = ImportLocale(locale, name);
        //        localeGroup.transform.parent = saveGroup.transform;
        //    }
        //}
    }

    public static void ExportSave()
    {
        Rayform.Save save = (Rayform.Save)file.data;

        for (int i = 0; i < save.sections.Count; i++)
        {
            string name = save.sections[i].name;
            //Debug.Log(name);

            if (name == "#worldchrdb")
            {

            }
            else if (name == "#worldobjdb")
            {

            }
            else if (name == "#playerdata")
            {
                Character playerObj = GameObject.FindObjectsOfType<Character>().FirstOrDefault(x => x.tag == "Player");
                if (playerObj)
                {
                    Rayform.Playerdata playerData = (Rayform.Playerdata)save.sections[i].data;
                    int index = playerData.chunks.FindIndex(x => x.GetType() == typeof(Rayform.CharacterPosition));
                    playerData.chunks[index] = playerObj.Export();
                }
            }
            else if (name == selLocale)
            {
                Rayform.Content locale = (Rayform.Content)save.sections[i].data;
                ExportLocale(locale, selLocale);
            }
        }
    }
}
