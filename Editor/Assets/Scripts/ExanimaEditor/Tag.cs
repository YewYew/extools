using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class Tag : MonoBehaviour
{
    public static HashSet<Tag>tags = new HashSet<Tag>();
    public string type;
    
    public static void Add (GameObject obj, string type)
    {
        Tag tag = obj.AddComponent<Tag>();
        tag.type = type;
        tags.Add(tag);
    }

    public static GameObject Find(string type)
    {
        Tag tag = tags.FirstOrDefault(x => x.type == type);
        if (tag != null) return tag.gameObject;
        return null;
    }

    private void OnEnable()
    {
        tags.Add(this);
    }

    private void OnDestroy()
    {
        tags.Remove(this);
    }
}
