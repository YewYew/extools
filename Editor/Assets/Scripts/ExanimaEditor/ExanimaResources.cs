using Rayform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEditor;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.Assertions;

partial class ExanimaResources
{
    //public static List<OpenPackage> objs;
    public static List<ImportedAsset> assets = new List<ImportedAsset>();
    public static List<UnityEngine.Material> materials = new List<UnityEngine.Material>();
    public static GameObject resourcesContainer;
    public static Rayform.RdbObjs objdb;
    public static void Init()
    {
        if (resourcesContainer != null)
        {
            return;
        }

        resourcesContainer = new GameObject("Resources");
        resourcesContainer.SetActive(false);
        //add some standard materials created at runtime

        // Transparent, no shadows
        UnityEngine.Material nullMaterial = new UnityEngine.Material(Shader.Find("Standard"));
        nullMaterial.name = "_null";
        nullMaterial.color = UnityEngine.Color.clear;
        nullMaterial.SetFloat("_Mode", 2);
        nullMaterial.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        nullMaterial.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        nullMaterial.SetFloat("_ZWrite", 0);
        nullMaterial.DisableKeyword("_ALPHATEST_ON");
        nullMaterial.EnableKeyword("_ALPHABLEND_ON");
        nullMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        nullMaterial.renderQueue = 3000;
        materials.Add(nullMaterial);

        // Transparent, no shadows
        UnityEngine.Material nullMaterial2 = new UnityEngine.Material(Shader.Find("Standard"));
        nullMaterial2.name = "_null1";
        nullMaterial2.color = UnityEngine.Color.clear;
        nullMaterial2.SetFloat("_Mode", 2);
        nullMaterial2.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        nullMaterial2.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        nullMaterial2.SetFloat("_ZWrite", 0);
        nullMaterial2.DisableKeyword("_ALPHATEST_ON");
        nullMaterial2.EnableKeyword("_ALPHABLEND_ON");
        nullMaterial2.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        nullMaterial2.renderQueue = 3000;
        materials.Add(nullMaterial2);

        UnityEngine.Material dclss01 = new UnityEngine.Material(Shader.Find("Standard"));
        dclss01.name = "dclss01";
        dclss01.color = UnityEngine.Color.clear;
        dclss01.SetFloat("_Mode", 2);
        dclss01.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        dclss01.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        dclss01.SetFloat("_ZWrite", 0);
        dclss01.DisableKeyword("_ALPHATEST_ON");
        dclss01.EnableKeyword("_ALPHABLEND_ON");
        dclss01.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        dclss01.renderQueue = 3000;
        materials.Add(dclss01);

        UnityEngine.Material dclsm01 = new UnityEngine.Material(Shader.Find("Standard"));
        dclsm01.name = "dclsm01";
        dclsm01.color = UnityEngine.Color.clear;
        dclsm01.SetFloat("_Mode", 2);
        dclsm01.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        dclsm01.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        dclsm01.SetFloat("_ZWrite", 0);
        dclsm01.DisableKeyword("_ALPHATEST_ON");
        dclsm01.EnableKeyword("_ALPHABLEND_ON");
        dclsm01.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        dclsm01.renderQueue = 3000;
        materials.Add(dclsm01);

        // Black
        UnityEngine.Material darkMaterial = new UnityEngine.Material(Shader.Find("Unlit/Color"));
        darkMaterial.name = "_dark";
        darkMaterial.color = UnityEngine.Color.clear;
        materials.Add(darkMaterial);

        // Transparent shadow caster. Why are there 3 of them???? Madooooooooooc
        UnityEngine.Material blackMaterial = new UnityEngine.Material(Shader.Find("Particles/Standard Unlit"));
        blackMaterial.name = "black";
        blackMaterial.SetFloat("_Mode", 2);
        blackMaterial.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        blackMaterial.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        blackMaterial.SetFloat("_ZWrite", 0);
        blackMaterial.DisableKeyword("_ALPHATEST_ON");
        blackMaterial.EnableKeyword("_ALPHABLEND_ON");
        blackMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        blackMaterial.renderQueue = 3000;
        blackMaterial.color = UnityEngine.Color.clear;
        materials.Add(blackMaterial);

        UnityEngine.Material blackMaterial2 = new UnityEngine.Material(Shader.Find("Particles/Standard Unlit"));
        blackMaterial2.name = "_black";
        blackMaterial2.SetFloat("_Mode", 2);
        blackMaterial2.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        blackMaterial2.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        blackMaterial2.SetFloat("_ZWrite", 0);
        blackMaterial2.DisableKeyword("_ALPHATEST_ON");
        blackMaterial2.EnableKeyword("_ALPHABLEND_ON");
        blackMaterial2.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        blackMaterial2.renderQueue = 3000;
        blackMaterial2.color = UnityEngine.Color.clear;
        materials.Add(blackMaterial2);

        UnityEngine.Material blackMaterial3 = new UnityEngine.Material(Shader.Find("Particles/Standard Unlit"));
        blackMaterial3.name = "black1";
        blackMaterial3.SetFloat("_Mode", 2);
        blackMaterial3.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        blackMaterial3.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        blackMaterial3.SetFloat("_ZWrite", 0);
        blackMaterial3.DisableKeyword("_ALPHATEST_ON");
        blackMaterial3.EnableKeyword("_ALPHABLEND_ON");
        blackMaterial3.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        blackMaterial3.renderQueue = 3000;
        blackMaterial3.color = UnityEngine.Color.clear;
        materials.Add(blackMaterial3);

        UnityEngine.Material blackMaterial4 = new UnityEngine.Material(Shader.Find("Particles/Standard Unlit"));
        blackMaterial4.name = "_black1";
        blackMaterial4.SetFloat("_Mode", 2);
        blackMaterial4.SetFloat("_SrcBlend", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
        blackMaterial4.SetFloat("_DstBlend", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        blackMaterial4.SetFloat("_ZWrite", 0);
        blackMaterial4.DisableKeyword("_ALPHATEST_ON");
        blackMaterial4.EnableKeyword("_ALPHABLEND_ON");
        blackMaterial4.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        blackMaterial4.renderQueue = 3000;
        blackMaterial4.color = UnityEngine.Color.clear;
        materials.Add(blackMaterial4);

        //ImportMaterialLibrary("common.rml");
        //ImportMaterialLibrary("underworld0.rml");
        //ImportMaterialLibrary("underworldb.rml");
    }

    //public static void ImportMaterialLibrary(string name)
    //{
    //    Package commonMats = (Package)PackageManager.LoadByPath("Resource.rpk\\" + name);
    //    foreach (Package.Entry entry in commonMats.entries)
    //    {
    //        Debug.Log("Resource.rpk\\" + name + "\\" + entry.name);
    //        ImportMaterial((Rayform.Material)PackageManager.LoadByPath("Resource.rpk\\" + name + "\\" + entry.name));
    //    }
    //}

    public static void Unload()
    {
        materials.Clear();
        assets.Clear();
        GameObject.DestroyImmediate(resourcesContainer);
        PackageManager.Unload();
    }

    public static ImportedAsset LoadByName(string name, Type type, Rayform.PackageManager.PackageCategory category)
    {
        ImportedAsset asset = assets.FirstOrDefault(x => x.fileName == name && x.category == category);
        if (asset == null)
        {
            PackageManager.Result result = PackageManager.LoadByName(name, category);
            if (result != null)
            {
                asset = ImportAsset((Struct)result.data, result.path, type);
                asset.category = category;
            }
        }
        return asset;
    }

    public static ImportedAsset LoadByPath(string path, Type type)
    {
        //check if asset has been already imported
        ImportedAsset asset = assets.FirstOrDefault(x => x.path == path);
        if (asset == null)
        {
            Struct data = (Struct)PackageManager.LoadByPath(path);
            asset = ImportAsset(data, path, type);
        }
        return asset;
    }

    public static ImportedAsset ImportAsset(Rayform.Struct data, string path, Type type)
    {
        ImportedAsset asset = new ImportedAsset();
        asset.path = path;
        asset.data = data;
        assets.Add(asset);

        //Debug.Log(path);
        if (asset.data == null)
        {
            return asset;
        }


        if (type == typeof(GameObject))
        {
            Content content = (Content)asset.data;

            //read the materials
            Rayform.Materials RFmaterials = content.chunks.OfType<Rayform.Materials>().FirstOrDefault();

            if (RFmaterials != null)
            {
                foreach (Rayform.Material RFmaterial in RFmaterials.materials)
                {
                    ImportMaterial(RFmaterial);
                }
            }

            asset._object = ExanimaEditor.ImportScene(content.chunks.OfType<Rayform.Scene>().First(), asset.fileName);
            ((GameObject)asset._object).transform.parent = resourcesContainer.transform;
        }
        else if (type == typeof(Texture2D))
        {
            if (asset != null && asset.data.GetType() == typeof(Image))
            {
                Image image = (Image)asset.data;
                asset._object = ImportTexture(image, asset.fileName);
            }
            else
            {
                asset._object = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            }

        }
        return asset;
    }
}
public class ImportedAsset
{
    public string path;
    public string fileName { get { return Path.GetFileName(path); } }
    public Struct data;
    public UnityEngine.Object _object;
    public Rayform.PackageManager.PackageCategory category;

    public GameObject Instantiate()
    {
        GameObject obj = GameObject.Instantiate((GameObject)_object);
        obj.name = _object.name;
        return obj;
    }
}