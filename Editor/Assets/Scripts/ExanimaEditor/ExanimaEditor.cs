using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

public partial class ExanimaEditor
{
    public static Rayform.OpenFile file;
    public static string selLocale;
    public static bool loaded;
    public static void Load(string path)
    {
        if (file != null)
        {
            Unload();
        }

        file = Rayform.OpenFile.Load(path);

        if (Path.GetExtension(file.fileName) == ".rsg")
        {
            file.data = new Rayform.Save();
            file.data.ReadStruct(file.fs);
            Rayform.Save save = (Rayform.Save)file.data;
            ImportSave(save);
        }
        else if (Path.GetExtension(file.fileName) == ".rfc")
        {
            selLocale = file.fileName;
            file.data = new Rayform.Content();
            file.data.ReadStruct(file.fs);
            Rayform.Content locale = (Rayform.Content)file.data;
            ImportContent(locale, file.fileName);
        }
        loaded = true;
    }

    public static void Unload()
    {
        if (file != null)
        {
            file.Close();
        }
        GameObject.DestroyImmediate(GameObject.Find(file.fileName));
        selLocale = "";
        loaded = false;
    }

    public static void Save()
    {
        string backupPath = file.path + " Unity Backups";

        Directory.CreateDirectory(backupPath);

        string[] files = Directory.GetFiles(backupPath);

        int number = 0;

        if (files.Length > 0)
        {
            number = files
                  .Select(x => int.Parse(Path.GetFileNameWithoutExtension(x).TakeLast(3).ToArray()))
                  .OrderBy(x => x).Last() + 1;
        }

        string numberStr = String.Format("{0:000}", number);
        string fileName = Path.GetFileNameWithoutExtension(file.fileName) + ' ' + numberStr + Path.GetExtension(file.fileName);

        var backupFile = File.Open(backupPath + '\\' + fileName, FileMode.OpenOrCreate);
        file.fs.Position = 0;
        file.fs.CopyTo(backupFile);
        backupFile.Flush();
        backupFile.Dispose();

        if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
        {
            ExportSave();
        }
        else
        {
            ExportLocale((Rayform.Content)file.data, file.fileName);
        }
        file.Write();
        Debug.Log("Saved!");
    }

    public static int FirstAvailableId(Rayform.Rdb rdb)
    {
        int baseId = 1;
        if (rdb.entries.Count > 0)
        {
            List<int> usedIds = rdb.entries.Select(x => x.baseId).Distinct().OrderBy(x => x).ToList();
            baseId = Enumerable.Range(baseId, usedIds.Last()+1).Except(usedIds).First();
        }
        
        return baseId;
    }
}
