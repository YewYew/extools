﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


    partial class ExanimaResources
    {
    public static Texture2D ImportTexture(Rayform.Image image, string name)
    {
        //if (image.codec == Rayform.Image.Codec.)
        //    throw new Exception("Invalid TextureFormat. Only DXT1 and DXT5 formats are supported by t$$anonymous$$s method.");

        //byte ddsSizeCheck = ddsBytes[4];
        //if (ddsSizeCheck != 124)
        //    throw new Exception("Invalid DDS DXTn texture. Unable to read");  //t$$anonymous$$s header byte should be 124 for DDS image files
        //
        //int height = ddsBytes[13] * 256 + ddsBytes[12];
        //int width = ddsBytes[17] * 256 + ddsBytes[16];
        TextureFormat format = TextureFormat.DXT1;

        if (new string[]{ "BC1_1", "BC1_2", "BC1_4"}.Contains(image.format.ToString()))
            format = TextureFormat.DXT1;
        else if (new string[] { "BC5U_1", "BC5U_3", "BC5U_4", "BC5U_5", "BC4U_1", "BC4U_2", "BC4U_3", "BC4U_4", "BC4U_5", "BC4U_6", "BC4U_7" }.Contains(image.format.ToString()))
            format = TextureFormat.BC5;
        else if (new string[] { "BC5U_2" }.Contains(image.format.ToString()))
            format = TextureFormat.DXT5;
        else if (new string[] { "BC1_3" }.Contains(image.format.ToString()))
            format = TextureFormat.RGB24;

        //int DDS_HEADER_SIZE = 128;
        //byte[] dxtBytes = new byte[ddsBytes.Length - DDS_HEADER_SIZE];
        //Buffer.BlockCopy(ddsBytes, DDS_HEADER_SIZE, dxtBytes, 0, ddsBytes.Length - DDS_HEADER_SIZE);

        Texture2D texture = new Texture2D(image.width, image.height, format, false);
        texture.name = name;
        texture.LoadRawTextureData(image.dxt);
        texture.Apply();
        if (format == TextureFormat.DXT5) texture.alphaIsTransparency = true;
        //Utils.VerticallyFlipRenderTexture(ref texture);

        return (texture);
    }

}
