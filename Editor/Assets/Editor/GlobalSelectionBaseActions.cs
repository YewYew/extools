using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.TerrainTools;
using UnityEngine;
 
/**
* This class is a "fake", global, Editor: it jumps into memory once, and sits
* in the background globally listening to selections, so that it can patch
* Unity's broken (and officially: they won't fix it) implementation of [SelectionBase]
*
* Based on @StunAustralia's code at: https://forum.unity.com/threads/in-editor-select-the-parent-instead-of-an-object-in-the-messy-hierarchy-it-creates.543479/#post-3586524
* Updated to fix issues with drill-down. This version: https://forum.unity.com/threads/in-editor-select-the-parent-instead-of-an-object-in-the-messy-hierarchy-it-creates.543479/#post-5691667
*/
[InitializeOnLoad]
public class GlobalSelectionBaseActions : Editor
{
    private static List<UnityEngine.Object> newSelection = null;
    private static UnityEngine.Object[] lastSelection = new UnityEngine.Object[] { };

    public static bool isBoxSelecting;
    public static Vector2 boxStartPos;
    public static Vector2 boxEndPos;
    public static Rect selectionRect;

    static GlobalSelectionBaseActions()
    {
        // Ensure we're told when the selection changes
        Selection.selectionChanged += OnSelectionChanged;
        // For some reason I can't be bothered investigating, you can't modify selections
        // while in OnSelectionChanged() so... hack to do it in Update() instead
        SceneView.duringSceneGui += OnSceneGUI;
    }
    public static void OnSelectionChanged()
    {
        if (SceneView.mouseOverWindow == null)
            return;

        // Only modify user selection if selected from the SceneView
        System.Type windowOver = SceneView.mouseOverWindow.GetType();
        System.Type sceneView = typeof(SceneView);
        //Debug.Log(windowOver);
        if (!windowOver.Equals(sceneView) && (selectionRect.size == Vector2.zero)) return;


        //  Look through them all, adjusting as needed
        var futureSelection = new List<UnityEngine.Object>();
        bool changed = false;
        foreach (GameObject go in Selection.GetFiltered<GameObject>(SelectionMode.Unfiltered))
        {
            changed = changed | AdjustIfNeeded(go, lastSelection, futureSelection);
        }
        // If nothing has changed, give the update nothing to reselect
        if (!changed)
        {
            futureSelection = null;
        }

        /** Only update newSelection atomically */
        newSelection = futureSelection;
        // Remember this selection so we can compare the next selection to it
        lastSelection = Selection.objects;
    }
    private static bool AdjustIfNeeded(GameObject go, object[] lastSelection, List<UnityEngine.Object> newSelection)
    {
        //Debug.Log("Selected: "+go );

        // If it was in the last selection set, leave it be
        //if (!lastSelection.Contains(go))
        //{
            //Debug.Log("...wasn't selected");

            GameObject parentWithGlobalSelectionBase = null;
            bool goHasGlobalSelectionBase = ObjectHasGlobalSelectionBase(go);
            parentWithGlobalSelectionBase = ParentWithGlobalSelectionBase(go);
            if
            (
                (parentWithGlobalSelectionBase != null && 
                //(!(lastSelection.Contains(parentWithGlobalSelectionBase)) ||
                selectionRect.size != Vector2.zero)
                //)
            )
            {
                //Debug.Log("....user NOT drilling down");
                // User NOT drilling down - replace selection with GlobalSelectionBase parent
                newSelection.Add(parentWithGlobalSelectionBase.gameObject);
                return true;
            }
        //}
        newSelection.Add(go);   // original go
        return false;
    }
    public static void OnSceneGUI(SceneView sceneView)
    {
        HandleMouse();
        if (newSelection != null)
        {
            Selection.objects = newSelection.ToArray();
            newSelection = null;
        }
    }
    public static bool ObjectHasGlobalSelectionBase(GameObject go)
    {
        foreach (Component component in go.GetComponents<MonoBehaviour>())
        {
            if (component.GetType().GetCustomAttributes(typeof(GlobalSelectionBaseAttribute), true).Length > 0)
            {
                return true;
            }
        }
        return false;
    }
    public static GameObject ParentWithGlobalSelectionBase(GameObject go)
    {
        if (go.transform.parent == null) return null;
        foreach (Component component in go.transform.parent.GetComponentsInParent<MonoBehaviour>(false))
        {
            if (component.GetType().GetCustomAttributes(typeof(GlobalSelectionBaseAttribute), true).Length > 0)
            {
                return component.gameObject;
            }
        }
        return null;
    }

    public static void HandleMouse()
    {
        //HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        //Debug.Log("test");
        Event e = Event.current;
        //var eventType = e.GetTypeForControl(controlID);
        //Debug.Log(e.type);

        // Check for the mouse down event to start the box selection
        if (e.type == EventType.MouseDown && e.button == 0)
        {
            isBoxSelecting = true;
            boxStartPos = e.mousePosition;
            selectionRect = new Rect(boxStartPos, Vector2.zero);
        }

        // Check for the mouse up event to end the box selection
        else if (e.type == EventType.MouseUp && e.button == 0)
        {
            isBoxSelecting = false;
            // Calculate the width and height of the selectionRect
        }
        // Update the selectionRect during box selection
        else if (isBoxSelecting && e.type == EventType.Used)
        {
            selectionRect.size = e.mousePosition - boxStartPos;
            //Debug.Log("Selection size: " + selectionRect.width + " x " + selectionRect.height);
        }
        else if (e.type == EventType.MouseMove)
        {
            isBoxSelecting = false;
            selectionRect.size = Vector2.zero;
        }
    }
}