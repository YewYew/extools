using Microsoft.VisualBasic.FileIO;
using Rayform;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms.VisualStyles;
using System.Xml.Linq;

namespace TreeView
{

    public partial class Form1 : Form // WIP debug tool that allows you see the class structure
    {
      //Struct target;
      public Form1()
      {
          InitializeComponent();
      }
      
      private void Form1_Load(object sender, EventArgs e)
      {
            //OpenFile openFile = OpenFile.Load("C:\\Users\\utente\\AppData\\Roaming\\Exanima\\Exanima005.rsg");
            //OpenFile openFile = OpenFile.Load("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\backup\\bridgecry06");
            //openFile.file = new Save();
            //openFile.file.ReadStruct(openFile.fs);

            //OpenFile openFile = OpenFile.Load("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\resource\\gews_uwa01.rfc");
            //openFile.file = new Content();
            //openFile.file.ReadStruct(openFile.fs, openFile.fs.Length);

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima Hellmode\\hm_files\\Resource\\exanima04.rfc";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Content file = new Content();
            //file.ReadStruct(fs);

            //string path = Config.savePath + "\\Exanima003.rsg";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Save file = new Save();
            //file.ReadStruct(fs);

            //PackageManager.Init();
            //string path = Config.savePath + "\\Exanima003.rsg";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Save file = new Save();
            //file.ReadStruct(fs);

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\resource\\exanima01.rfc";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Content file = new Content();
            //file.ReadStruct(fs);

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\morarm1.rfc";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Content file = new Content();
            //file.ReadStruct(fs);

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\resource\\humanbase.rfc";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Content file = new Content();
            //file.ReadFile(fs, fs.Length);

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\resource\\ancientdifm.rfi";
            //Stream fs = System.IO.File.Open(path, FileMode.Open);
            //Rayform.Image file = new Rayform.Image();
            //file.ReadStruct(fs);


            //Package file = (Package)Parser.ReadFile("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\Resource.rpk");

            //string path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\Resource.rpk";
            //OpenFile openFile = OpenFile.Load("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\backup\\bridge ub07 a02a.rfc");
            //OpenFile openFile = OpenFile.Load("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\backup\\bridge ub07 a02a.rfc");
            //var penis = PackageManager.LoadByPath("Resource.rpk\\common.rml\\logst01");

            //OpenFile openFile = OpenFile.Load("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\resource\\gews_uwa01.rfc");
            //openFile.data = new Content();
            //openFile.data.ReadStruct(openFile.fs);
            //Character cha = new Character();
            //Debug.WriteLine(cha.name);
            //fs.Position = 0;
            //file.WriteStruct(fs);

            OpenFile file = OpenFile.Load("C:\\Users\\utente\\AppData\\Roaming\\Exanima\\Exanima001.rsg");
            file.data = new Save();
            file.data.ReadStruct(file.fs);

            //var file = PackageManager.LoadByName("ub6trdrt");
            //var file = PackageManager.LoadByPath("Objlib.rpk\\chain_wall.rfc");
            //var file = PackageManager.LoadByPath("Objects.rpk\\torch a01");
            //openFile.file = new Save();
            //openFile.file.ReadStruct(openFile.fs);

            BuildTreeView(null, file, "");
            //BuildTreeView(null, PackageManager.LoadByName("humanbase.rfc").asset, "");
      }

      public void BuildTreeView(TreeNode node,object item, string path)
      {
            //FieldInfo[] fields = Utils.GetFields(item);
            FieldInfo[] fields = item.GetType()
            .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(f => f.GetCustomAttribute<CompilerGeneratedAttribute>() == null)
            .ToArray();

            if (node == null) 
                    node = treeView1.Nodes.Add(item.GetType().ToString());

            if (item.GetType().IsArray && item != null && item.GetType().GetArrayRank() == 1)
            {
                for (int i = 0; i < ((dynamic)item).Length; i++)
                {
                    if (i == 1000)
                    {
                        Debug.WriteLine(item.GetType());
                        break;
                    }
                        
                    if (((object)((dynamic)item)[i]) == null) return;
                    TreeNode listNode = node.Nodes.Add(((dynamic)item)[i].GetType().ToString());
                    object listItem = ((dynamic)item)[i];
                    BuildTreeView(listNode, listItem, path);
                }
            }

            foreach (FieldInfo field in fields) 
            {
                object fieldValue = field.GetValue(item);
                Type fieldType = null;

                if (fieldValue != null)
                    fieldType = fieldValue.GetType();
                else
                    fieldType = field.FieldType;

                //Debug.WriteLine("test");

                // if class
                if (fieldType.IsClass && !fieldType.IsGenericType)
                {
                    TreeNode root = node.Nodes.Add(fieldType + " " + field.Name);
                    if(fieldValue != null)
                        BuildTreeView(root, fieldValue, path);
                }
                // if struct
                //else if (fieldType.IsValueType && !fieldType.IsPrimitive)
                //{
                //    TreeNode root = node.Nodes.Add(fieldType + " " + field.Name);
                //    if (fieldValue != null)
                //        BuildTreeView(root, fieldValue, path);
                //}
                else if (fieldType == typeof(Id))
                {
                    TreeNode root = node.Nodes.Add(fieldType + " " + field.Name);
                    if (fieldValue != null)
                        BuildTreeView(root, fieldValue, path);
                }
                else if (fieldType == typeof(Matrix3x4))
                {
                    TreeNode root = node.Nodes.Add(fieldType + " " + field.Name);
                    if (fieldValue != null)
                        BuildTreeView(root, fieldValue, path);
                }
                // add a list
                else if (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    TreeNode listRoot = node.Nodes.Add(fieldType + " " + field.Name);
                    for (int i=0;i< ((dynamic)fieldValue).Count;i++)
                    {
                        TreeNode listNode = listRoot.Nodes.Add(((dynamic)fieldValue)[i].GetType().ToString());
                        object listItem = ((dynamic)fieldValue)[i];
                        BuildTreeView(listNode, listItem, path);
                    }
                }
                // add an array
                else if (fieldType.IsArray && fieldValue != null)
                {
                    TreeNode listRoot = node.Nodes.Add(fieldType + " " + field.Name);
                    for (int i = 0; i < ((dynamic)fieldValue).Length ; i++)
                    {
                        if(((object)((dynamic)fieldValue)[i])==null) return;
                        //if (((object)((dynamic)fieldValue)[i]) == null) Debug.WriteLine(listRoot);
                        TreeNode listNode = listRoot.Nodes.Add(((dynamic)fieldValue)[i].GetType().ToString());
                        object listItem = ((dynamic)fieldValue)[i];
                        BuildTreeView(listNode, listItem, path);
                    }
                }
                // add a field with its value
                else
                {
                    TreeNode root = node.Nodes.Add(fieldType + " " + field.Name);
                    //Debug.WriteLine(fieldType);
                    if(fieldValue != null)
                        root.Nodes.Add(fieldValue.ToString());
                }
            }
      }
        
      //private void button1_Click(object sender, EventArgs e)
      //{
      //    MessageBox.Show(treeView1.SelectedNode.FullPath.ToString());
      //}

      private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
      {

      }
    }
}