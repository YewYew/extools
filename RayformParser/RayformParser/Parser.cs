﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Rayform
{
    // Methods to read and write primitives and structs in the Stream
    public class Parser
    {
        #region read
        public static byte[] ReadBytes(Stream fs, int size)
        {
            byte[] buffer = new byte[Math.Clamp(size, 0, int.MaxValue)];
            fs.Read(buffer);
            return buffer;
        }

        public static int ReadInt32(Stream fs)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer);
            var value = BitConverter.ToInt32(buffer);
            return value;
        }

        public static uint ReadUInt32(Stream fs)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer);
            var value = BitConverter.ToUInt32(buffer);
            return value;
        }

        public static short ReadInt16(Stream fs)
        {
            byte[] buffer = new byte[2];
            fs.Read(buffer);
            var value = BitConverter.ToInt16(buffer);
            return value;
        }

        public static ushort ReadUInt16(Stream fs)
        {
            byte[] buffer = new byte[2];
            fs.Read(buffer);
            var value = BitConverter.ToUInt16(buffer);
            return value;
        }

        public static bool ReadBoolean(Stream fs)
        {
            byte[] buffer = new byte[1];
            fs.Read(buffer);
            var value = BitConverter.ToBoolean(buffer);
            return value;
        }

        public static byte ReadInt8(Stream fs)
        {
            var value = fs.ReadByte();
            return (byte)value;
        }

        // This method reads the amount of bytes specified,
        // then separates each digit of these bytes as a separate byte,
        // defined in a custom struct called int4, with a max size of 127.
        public static Int4[] ReadInt4(Stream fs, int length)
        {
            byte[] buffer = new byte[length];
            fs.Read(buffer);
            string hex = BitConverter.ToString(buffer).Replace("-", "");
            Int4[] value = hex.Select(x => (Int4)Convert.ToByte(x.ToString(), 16)).ToArray();
            return value;
        }

        public static Str128 ReadStr128(Stream fs)
        {
            byte[] buffer = new byte[16];
            fs.Read(buffer);
            Str128 value = Encoding.ASCII.GetString(buffer).TrimEnd('\0');
            return value;
        }

        public static float ReadFloat(Stream fs)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer);
            var value = BitConverter.ToSingle(buffer, 0);
            return value;
        }

        public static Vector3 ReadVector3(Stream fs)
        {
            Vector3 vector = new Vector3();
            vector.x = ReadFloat(fs);
            vector.z = ReadFloat(fs);
            vector.y = ReadFloat(fs);
            return vector;
        }

        public static Vector2 ReadVector2(Stream fs)
        {
            Vector2 vector = new Vector2();
            vector.x = ReadFloat(fs);
            vector.y = ReadFloat(fs);
            return vector;
        }

        public static Color ReadColor(Stream fs)
        {
            Color color = new Color(ReadFloat(fs), ReadFloat(fs) , ReadFloat(fs), ReadFloat(fs));
            return color;
        }

        public static Matrix3x4 ReadMatrix(Stream fs)
        {
            Matrix3x4 matrix = new Matrix3x4();
            matrix.m00 = ReadFloat(fs);
            matrix.m01 = ReadFloat(fs);
            matrix.m02 = ReadFloat(fs);
            matrix.m10 = ReadFloat(fs);
            matrix.m11 = ReadFloat(fs);
            matrix.m12 = ReadFloat(fs);
            matrix.m20 = ReadFloat(fs);
            matrix.m21 = ReadFloat(fs);
            matrix.m22 = ReadFloat(fs);
            matrix.m30 = ReadFloat(fs);
            matrix.m31 = ReadFloat(fs);
            matrix.m32 = ReadFloat(fs);
            return matrix;
        }

        public static string ReadString(Stream fs, int length)
        {
            byte[] buffer = new byte[length];
            fs.Read(buffer);
            string value = Encoding.ASCII.GetString(buffer).TrimEnd('\0');
            return value;
        }
        #endregion

        #region write
        public static void WriteBytes(Stream fs, byte[] value)
        {
            fs.Write(value);
        }

        public static void WriteInt32(Stream fs, int value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteUInt32(Stream fs, uint value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteInt16(Stream fs, short value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteUInt16(Stream fs, ushort value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteBoolean(Stream fs, bool value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteInt8(Stream fs, byte value)
        {
            fs.WriteByte(value);
        }

        public static void WriteInt4(Stream fs, Int4[] value)
        {
            byte[] bytes = Array.ConvertAll(value, item => (byte)item);
            string hex = BitConverter.ToString(bytes).Replace("-", "");
            //Debug.WriteLine(hex);

            for (int i = 1; i < hex.Length; i=i+4)
            {
                string byteHex = hex[i].ToString() + hex[i+2].ToString();
                Parser.WriteInt8(fs, Convert.ToByte(byteHex, 16));
            }
        }

        public static void WriteStr128(Stream fs, Str128 value)
        {
            int spaceToFill = 16;
            if (value != null)
            {
                fs.Write(Encoding.ASCII.GetBytes(value));
                spaceToFill = Math.Clamp(16 - value.Length, 0, 16);
            }

            if(spaceToFill > 0)
            {
                fs.Write(Enumerable.Repeat((byte)0, spaceToFill).ToArray());
            }

            //for(int i = 0;i < 16 - value.ToString().Length; i++)
            //{
            //    fs.WriteByte(0);
            //}
        }

        public static void WriteFloat(Stream fs, float value)
        {
            fs.Write(BitConverter.GetBytes(value));
        }

        public static void WriteVector3(Stream fs, Vector3 value)
        {
            WriteFloat(fs, value.x);
            WriteFloat(fs, value.z);
            WriteFloat(fs, value.y);
        }

        public static void WriteVector2(Stream fs, Vector2 value)
        {
            WriteFloat(fs, value.x);
            WriteFloat(fs, value.y);
        }

        public static void WriteColor(Stream fs, Color value)
        {
            WriteFloat(fs, value.r);
            WriteFloat(fs, value.g);
            WriteFloat(fs, value.b);
            WriteFloat(fs, value.a);
        }

        public static void WriteMatrix(Stream fs, Matrix3x4 value)
        {
            WriteFloat(fs, value.m00);
            WriteFloat(fs, value.m01);
            WriteFloat(fs, value.m02);
            WriteFloat(fs, value.m10);
            WriteFloat(fs, value.m11);
            WriteFloat(fs, value.m12);
            WriteFloat(fs, value.m20);
            WriteFloat(fs, value.m21);
            WriteFloat(fs, value.m22);
            WriteFloat(fs, value.m30);
            WriteFloat(fs, value.m31);
            WriteFloat(fs, value.m32);
        }

        public static void WriteString(Stream fs, string value)
        {
            if(value != null)
            {
                fs.Write(Encoding.ASCII.GetBytes(value));
            }
        }
        #endregion
    }
}
