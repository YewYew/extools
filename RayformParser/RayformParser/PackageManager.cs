﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Rayform
{
    // Weird package indexing system. I could change this at some point
    public class PackageManager
    {
        private static bool init;
        public static List<OpenFile> openPackages = new List<OpenFile>();
        //public static List<string> searchablePaths = new List<string> { "Resource.rpk", "Objlib.rpk", "Textures.rpk", "Resource.rpk\\Common.rml", "Resource.rpk\\Underworld0.rml", "Resource.rpk\\UnderworldB.rml" };
        //public static List<string> searchablePaths = new List<string>();
        public static OpenFile projectFile;

        public enum PackageCategory
        {
            Resources,
            LibNodes,
            Textures,
            Materials
        }

        public static void Init()
        {
            projectFile = OpenFile.Load(Config.installPath + "\\exanima.rfp");
            Project project = new Project();
            projectFile.data = project;
            project.ReadStruct(projectFile.fs);
            //searchablePaths.Clear();
            //searchablePaths.AddRange(project.resources);
            //searchablePaths.AddRange(project.libnodes);
            //searchablePaths.AddRange(project.textures);
            //searchablePaths.AddRange(project.materials);

            //foreach (string path in searchablePaths)
            //{
            //    LoadByPath(path);
            //}


            //objDb = (RdbObjs)LoadByPath("Resource.rpk\\objdb.rdb");
            init = true;
        }

        //public static Result LoadByName(string name)
        //{
        //    if (!init)
        //    {
        //        Init();
        //    }
        //
        //    //searches for this asset through the searchable paths
        //    foreach(string path in searchablePaths) 
        //    {
        //        object asset = LoadByPath(path + '\\' + name);
        //
        //        if (asset != null)
        //        {
        //            //Debug.WriteLine("unga?");
        //            return new Result { data = asset, path = path + '\\' + name };
        //        }
        //    }
        //    return null;
        //}

        public static Result LoadByName(string name, PackageCategory category)
        {
            if (!init)
            {
                Init();
            }

            Project project = (Project)projectFile.data;

            List<string> packages = project.resources;

            if (category == PackageCategory.LibNodes)
            {
                packages = project.libNodes;
            }
            else if (category == PackageCategory.Textures)
            {
                packages = project.textures;
            }
            else if (category == PackageCategory.Materials)
            {
                packages = project.materials;
            }
            
            //searches for this asset through the searchable paths
            foreach(string path in packages) 
            {
                object asset = LoadByPath(path + '\\' + name);
            
                if (asset != null)
                {
                    //Debug.WriteLine("unga?");
                    return new Result { data = asset, path = path + '\\' + name.ToLower() };
                }
            }
            return null;
        }

        public static object LoadByPath(string path)
        {
            if (!init)
            {
                Init();
            }

            //why does madoc truncate the extension there too?
            string packageFileName = Path.ChangeExtension(path.Split('\\')[0], ".rpk");
            string parentFolder = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            //check if package has been already opened and parsed
            OpenFile packageFile = openPackages.FirstOrDefault(x => x.fileName == packageFileName);

            //open the package
            if(packageFile == null)
            {
                packageFile = OpenFile.Load(Config.installPath + '\\' + packageFileName);
                packageFile.data = new Package();
                packageFile.data.ReadStruct(packageFile.fs);
                openPackages.Add(packageFile);
            }

            // If only the rpk is requested then return it
            if (Path.GetDirectoryName(path) == "")
            {
                return packageFile.data;
            }
            else
            {
                // Find the entry
                Package package = (Package)packageFile.data;
                Package.Entry entry = null;
                string[] paths = path.Split('\\').Skip(1).ToArray();
                for (int i = 0; i < paths.Length; i++)
                {
                    string entryName = Path.GetFileNameWithoutExtension(paths[i]).ToLower();
                    if (entry != null)
                    {
                        package = (Package)entry.data;
                    }
                    entry = package.entries.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x.name).ToLower() == entryName);
                    if (entry == null) return null; // Asset cant be found at this path
                }

                // Asset has never been loaded, so load it now
                if(entry.data==null)
                {
                    packageFile.fs.Position = entry.position;

                    if (entry.extension == ".rfc")
                    {
                        Content data = new Content();
                        data.ReadStruct(packageFile.fs, entry.size);
                        entry.data = data;
                    }
                    else if (entry.extension == ".rfi")
                    {
                        Image data = new Image();
                        data.ReadStruct(packageFile.fs);
                        entry.data = data;
                    }
                    else if (entry.extension == ".mat")
                    {
                        entry.data = new Material();
                        ((Material)entry.data).ReadStruct(packageFile.fs, entry.name, entry.size);
                    }
                    //else if (new string[] { ".rpk", ".rlb", ".fds", ".flb", ".rml" }.Contains(entry.extension))
                    //{
                    //    entry.data = new Package();
                    //    ((Package)entry.data).ReadStruct(packageFile.fs);
                    //}
                    else if (fileName == "objdb.rdb")
                    {
                        entry.data = new RdbObjs();
                        ((RdbObjs)entry.data).ReadStruct(packageFile.fs);
                    }
                    // What the fuck.mp4
                    else if (fileName == "locales.rdb")
                    {
                        entry.data = new RdbLocales();
                        ((RdbLocales)entry.data).ReadStruct(packageFile.fs);
                    }
                    else if (fileName == "objstrings.rdb")
                    {
                        entry.data = new RdbObjStrings();
                        ((RdbObjStrings)entry.data).ReadStruct(packageFile.fs);
                    }
                    // actors.rpk may function as the global chars rdb
                    // ...
                    else
                    {
                        Chunk data = Chunk.StartReadChunk(packageFile.fs);
                        data.ReadStruct(packageFile.fs);
                        data.EndReadChunk(packageFile.fs);
                        if (data.chunkSize == entry.size)
                        {
                            entry.data = data;
                        }
                        else
                        {
                            List<Chunk> chunks = new List<Chunk>();
                            chunks.Add(data);
                            while (packageFile.fs.Position < entry.position + entry.size)
                            {
                                Chunk chunk = Chunk.StartReadChunk(packageFile.fs);
                                chunk.ReadStruct(packageFile.fs);
                                data.EndReadChunk(packageFile.fs);
                                chunks.Add(chunk);
                            }
                            entry.data = chunks;
                        }
                    }
                }
                return entry.data;
            }
        }

        // A wrapper to make sure the actual asset path is returned
        public class Result
        {
            public string path;
            public object data;
            public string name { get { return Path.GetFileName(path); } }
        }

        public static void Unload()
        {
            foreach(OpenFile package in openPackages)
            {
                package.Close();
            }
            openPackages.Clear();
            projectFile.Close();
            //searchablePaths.Clear();
            init = false;
        }
    }
}
