﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Map : HeavyChunk
    {
        public int unknown0;
        public int unknown1;
        public int unknown2;
        public int unknown3;
        public Str128 mapName;

        public override void ReadStruct(Stream fs)
        {
            unknown0 = Parser.ReadInt32(fs);
            unknown1 = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);
            unknown3 = Parser.ReadInt32(fs);
            mapName = Parser.ReadStr128(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, unknown0);
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteInt32(fs, unknown2);
            Parser.WriteInt32(fs, unknown3);
            Parser.WriteStr128(fs, mapName);
        }
    }
}
