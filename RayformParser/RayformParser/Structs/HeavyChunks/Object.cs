﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	public class Object : HeavyChunk //00 00 10 00
	{
		public Str128 modelName; // Item model from Objects.rdb, used for being placed/dropped in the world.
		public byte unknown0;
        public byte iconView; // Determines how an item is displayed in the icon view
        public byte unknown1;
        public byte unknown2;
        public Id nameId = new Id(); // Entry index(not UID) into objstrings.rdb, string displayed in inventory, flagged with 0x8000_0000 if using local objstrings.
		public Id descId = new Id();
		public byte size; // If >= 50 object can no longer be picked up.
		public byte rarity;
		public ushort value;
		public byte unknown7;
		public byte unknown8;
		public byte unknown9; // Previously weaponGrip
        public byte unknown10;
		public byte unknown11; // Previously weight
        public byte unknown12;
		public byte unknown13;
		public byte unknown14;
		//For weapon_hold_type:
		// 0: 1h
		// 1: 2h
		// 2: polearm
		// other: fists

		public override void ReadStruct(Stream fs)
		{
			modelName = Parser.ReadStr128(fs);
			unknown0 = Parser.ReadInt8(fs);
            iconView = Parser.ReadInt8(fs);
            unknown1 = Parser.ReadInt8(fs);
            unknown2 = Parser.ReadInt8(fs);
			nameId.ReadStruct(fs);
            descId.ReadStruct(fs);
            size = Parser.ReadInt8(fs);
            rarity = Parser.ReadInt8(fs);
            value = Parser.ReadUInt16(fs);
			unknown7 = Parser.ReadInt8(fs);
			unknown8 = Parser.ReadInt8(fs);
			unknown9 = Parser.ReadInt8(fs);
			unknown10 = Parser.ReadInt8(fs);
			unknown11 = Parser.ReadInt8(fs);
			unknown12 = Parser.ReadInt8(fs);
			unknown13 = Parser.ReadInt8(fs);
			unknown14 = Parser.ReadInt8(fs);
		}

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteStr128(fs, modelName);
            Parser.WriteInt8(fs, unknown0);
            Parser.WriteInt8(fs, iconView);
            Parser.WriteInt8(fs, unknown1);
            Parser.WriteInt8(fs, unknown2);
            nameId.WriteStruct(fs);
            descId.WriteStruct(fs);
            Parser.WriteInt8(fs, size);
            Parser.WriteInt8(fs, rarity);
            Parser.WriteUInt16(fs, value);
            Parser.WriteInt8(fs, unknown7);
            Parser.WriteInt8(fs, unknown8);
            Parser.WriteInt8(fs, unknown9);
            Parser.WriteInt8(fs, unknown10);
            Parser.WriteInt8(fs, unknown11);
            Parser.WriteInt8(fs, unknown12);
            Parser.WriteInt8(fs, unknown13);
            Parser.WriteInt8(fs, unknown14);
        }
    }
}
