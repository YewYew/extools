﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	// 00 40 00 00, used for procedural weapons
	public class Weapon : HeavyChunk
	{   
		public GripType[] weaponGrips = new GripType[2];
        public GripFlags gripFlags;
		public byte unknown0;
        public Flags flags;
        public int seed;
        public int unknown2;
		public byte quality;
		public byte wear;
		public byte unknown3;
		public byte unknown4;

		public int unknown5;
		public int unknown6;
		public int unknown7;
		public int unknown8;
		public int unknown9;
        public int unknown10;
        public int unknown11;
        public int unknown12;

        public byte weight;
		public byte balance;
		public byte impactDamage;
		public byte thrustDamage;
        public byte unknown13;
        public byte unknown14;
        public byte unknown15;
        public byte slashDamage;
		public byte crushDamage;
		public byte pierceDamage;
		public bool flip;
		public byte flipImpactDamage;
		public byte flipThrustDamage;
		public byte flipSlashDamage;
		public byte flipCrushDamage;
		public byte flipPierceDamage;
		public byte unknown16;
        public Sound sound;
        public byte rank;
        public byte unknown17;
        public Ability ability;
        public byte unknown18;
        public byte unknown19;
        public byte unknown20;
        public Socket[] sockets = new Socket[4];
        public int unknown21;
        public int unknown22;
        public int unknown23;
        public int unknown24;

        public enum Flags
        {
            None = 0,
            HideStats = 1,
            Giant = 2
        }

        public Weapon()
		{
            for (int i = 0; i < 4; i++)
            {
                sockets[i] = new Socket();
            }
        }

		public enum GripType : byte
		{	
			Fists = 0,
			OneHanded = 1,
			TwoHanded = 2,
			Polearm = 3,

		}
        public enum GripFlags : byte
        {
            None = 0,
            Unknown = 1,
            Unknown2 = 2,
            Unknown3 = 3,
			Greatsword = 4
        }

        public enum Sound
        {
            Sharp1,
            Heavy1,
            Heavy2,
            Sharp2,
            Heavy3,
            OgreMace,
            Wood,
            Clutter,
            Clutter2
        }

        public enum Ability
        {
            None,
            Shock,
            Sharp,
            Fire,
            Sceptre
        }

        public override void ReadStruct(Stream fs)
		{
            Int4[] grips = Parser.ReadInt4(fs, 1);
            weaponGrips[0] = (GripType)(byte)grips[0];
            weaponGrips[1] = (GripType)(byte)grips[1];
			gripFlags = (GripFlags)Parser.ReadInt8(fs);
			unknown0 = Parser.ReadInt8(fs);
			flags = (Flags)Parser.ReadInt8(fs);
            seed = Parser.ReadInt32(fs);
			unknown2 = Parser.ReadInt32(fs);

            quality = Parser.ReadInt8(fs);
            wear = Parser.ReadInt8(fs);
            unknown3 = Parser.ReadInt8(fs);
            unknown4 = Parser.ReadInt8(fs);

            unknown5 = Parser.ReadInt32(fs);
            unknown6 = Parser.ReadInt32(fs);
            unknown7 = Parser.ReadInt32(fs);
            unknown8 = Parser.ReadInt32(fs);
            unknown9 = Parser.ReadInt32(fs);
            unknown10 = Parser.ReadInt32(fs);
            unknown11 = Parser.ReadInt32(fs);
            unknown12 = Parser.ReadInt32(fs);

            weight = Parser.ReadInt8(fs);
            balance = Parser.ReadInt8(fs);
            impactDamage = Parser.ReadInt8(fs);
            thrustDamage = Parser.ReadInt8(fs);
            unknown13 = Parser.ReadInt8(fs);
            unknown14 = Parser.ReadInt8(fs);
            unknown15 = Parser.ReadInt8(fs);
            slashDamage = Parser.ReadInt8(fs);
            crushDamage = Parser.ReadInt8(fs);
            pierceDamage = Parser.ReadInt8(fs);
            flip = Parser.ReadBoolean(fs);
            flipImpactDamage = Parser.ReadInt8(fs);
            flipThrustDamage = Parser.ReadInt8(fs);
            flipSlashDamage = Parser.ReadInt8(fs);
            flipCrushDamage = Parser.ReadInt8(fs);
            flipPierceDamage = Parser.ReadInt8(fs);
            unknown16 = Parser.ReadInt8(fs);
            sound = (Sound)Parser.ReadInt8(fs);
            rank = Parser.ReadInt8(fs);
            unknown17 = Parser.ReadInt8(fs);
            ability = (Ability)Parser.ReadInt8(fs);
            unknown18 = Parser.ReadInt8(fs);
            unknown19 = Parser.ReadInt8(fs);
			unknown20 = Parser.ReadInt8(fs);
			for (int i = 0; i < 4; i++)
			{
				//sockets[i] = new Socket();
                sockets[i].ReadStruct(fs);
			}
            unknown21 = Parser.ReadInt32(fs);
            unknown22 = Parser.ReadInt32(fs);
            unknown23 = Parser.ReadInt32(fs);
            unknown24 = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {	
            Parser.WriteInt4(fs, new Int4[] { (Int4)(byte)weaponGrips[0], (Int4)(byte)weaponGrips[1] });
			Parser.WriteInt8(fs, (byte)gripFlags);
			Parser.WriteInt8(fs, unknown0);
			Parser.WriteInt8(fs, (byte)flags);
            Parser.WriteInt32(fs, seed);

			Parser.WriteInt32(fs, unknown2);
            Parser.WriteInt8(fs, quality);
			Parser.WriteInt8(fs, wear);
			Parser.WriteInt8(fs, unknown3);
			Parser.WriteInt8(fs, unknown4);

			Parser.WriteInt32(fs, unknown5);
			Parser.WriteInt32(fs, unknown6);
			Parser.WriteInt32(fs, unknown7);
			Parser.WriteInt32(fs, unknown8);
			Parser.WriteInt32(fs, unknown9);
			Parser.WriteInt32(fs, unknown10);
			Parser.WriteInt32(fs, unknown11);
			Parser.WriteInt32(fs, unknown12);

			Parser.WriteInt8(fs, weight);
			Parser.WriteInt8(fs, balance);
			Parser.WriteInt8(fs, impactDamage);
			Parser.WriteInt8(fs, thrustDamage);
            Parser.WriteInt8(fs, unknown13);
            Parser.WriteInt8(fs, unknown14);
            Parser.WriteInt8(fs, unknown15);
            Parser.WriteInt8(fs, slashDamage);
			Parser.WriteInt8(fs, crushDamage);
			Parser.WriteInt8(fs, pierceDamage);
			Parser.WriteBoolean(fs, flip);
			Parser.WriteInt8(fs, flipImpactDamage);
			Parser.WriteInt8(fs, flipThrustDamage);
			Parser.WriteInt8(fs, flipSlashDamage);
			Parser.WriteInt8(fs, flipCrushDamage);
			Parser.WriteInt8(fs, flipPierceDamage);
            Parser.WriteInt8(fs, unknown16);
            Parser.WriteInt8(fs, (byte)sound);
            Parser.WriteInt8(fs, rank);
            Parser.WriteInt8(fs, unknown17);
            Parser.WriteInt8(fs, (byte)ability);
            Parser.WriteInt8(fs, unknown18);
            Parser.WriteInt8(fs, unknown19);
            Parser.WriteInt8(fs, unknown20);
            foreach (Socket socket in sockets)
            {
                socket.WriteStruct(fs);
            }
            Parser.WriteInt32(fs, unknown21);
            Parser.WriteInt32(fs, unknown22);
            Parser.WriteInt32(fs, unknown23);
            Parser.WriteInt32(fs, unknown24);
        }
    }
}