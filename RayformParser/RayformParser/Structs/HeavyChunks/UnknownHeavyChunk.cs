﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class UnknownHeavyChunk : HeavyChunk // If a chunk has not been defined yet, this fallback class is used to prevent data loss
    {
        public byte[] data;

        public override void ReadStruct(Stream fs)
        {
            chunkSize = Math.Clamp(chunkSize, 0, fs.Length - fs.Position+8);
            data = Parser.ReadBytes(fs, (int)chunkSize-8);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteBytes(fs, data);
        }
    }
}
