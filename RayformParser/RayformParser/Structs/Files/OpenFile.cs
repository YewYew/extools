﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    // All this does is group an object and a Stream. Do I really need this?
    public class OpenFile
    {
        public FileStream fs;
        public Struct data = new Struct();

        public string fileName { get { return Path.GetFileName(fs.Name); } }
        public string path { get { return fs.Name; } }

        public static OpenFile Load(string path)
        {
            OpenFile file = new OpenFile();
            file.fs = File.Open(path, FileMode.OpenOrCreate);
            return file;
        }

        public void Read(Type type)
        {
            fs.Position = 0;
            Chunk chunk = Chunk.StartReadChunk(fs);
            chunk.EndReadChunk(fs);
            if (type.BaseType != typeof(Chunk)) 
            {
                chunk.ReadStruct(fs);
            }
            else
            {
                data.ReadStruct(fs);
            }
        }

        public void Write()
        {
            fs.Position = 0;

            if (data.GetType().BaseType == typeof(Chunk))
            {
                ((Chunk)data).WriteChunk(fs);
            }
            else
            {
                data.WriteStruct(fs);
            }

            fs.SetLength(fs.Position);
            fs.Flush();
        }

        public void Close()
        {
            fs.Close();
            //fs = null;
            //data = null;
        }
    }
}
