﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static Rayform.CharacterPosition;
using System.Xml.Linq;
using static Rayform.DiaryEntries;
using System.Linq;
using static Rayform.Package;

namespace Rayform
{
    public class Package : Struct
    {
        public int signature;
        //public string format;
        //public string impliedExtension;
        public List<Entry> entries = new List<Entry>();

        public override void ReadStruct(Stream fs)
        {
            ReadPackage(fs, "");
        }
        private void ReadPackage(Stream fs, string path)
        {
            signature = Parser.ReadInt32(fs);
            uint size = Parser.ReadUInt32(fs);
            long start = fs.Position;
            while (fs.Position < start + size)
            {
                Entry entry = new Entry();
                entry.name = Parser.ReadStr128(fs);
                entry.offset = Parser.ReadUInt32(fs);
                entry.size = Parser.ReadUInt32(fs);
                entry.unknown0 = Parser.ReadInt32(fs);
                entry.unknown1 = Parser.ReadInt32(fs);
                entries.Add(entry);
                long next = fs.Position;
                Console.WriteLine("size is " + entry.size);
                entry.position = start + size + entry.offset;
                fs.Position = entry.position;
                //Console.WriteLine(fs.Position);
                Console.WriteLine("Now reading file: " + entry.name);

                entry.extension = GetExtension(path + '\\' + entry.name, fs, entry);

                // Only package entries are read
                if (new string[] { ".rpk", ".rlb", ".fds", ".flb", ".rml" }.Contains(entry.extension))
                {
                    Console.WriteLine("package found");
                    entry.data = new Package();
                    ((Package)entry.data).ReadPackage(fs, path + '\\' + entry.name);
                }

                Console.WriteLine("done");
                fs.Position = next;
            }
        }

        // Each package has multiple "entries".
        public class Entry
        {
            public Str128 name;
            public uint offset; // The offset of the entry from the beginning of the package.
            public uint size;
            public int unknown0;
            public int unknown1;
            public object data; // The actual data within the entry.
            public string extension;
            public long position;
        }

        public override void WriteStruct(Stream fs)
        {
            string tempFilePath = Path.GetTempPath() + Guid.NewGuid().ToString();
            Stream newStream = new FileStream(tempFilePath, FileMode.CreateNew);
            WritePackage(fs, newStream, ((FileStream)fs).Name);
            newStream.Flush();
            fs.Position = 0;
            newStream.Position = 0;
            newStream.CopyTo(fs);
            fs.SetLength(newStream.Length);
            newStream.Close();
            File.Delete(tempFilePath);
            fs.Flush();
        }

        public void WritePackage(Stream oldStream, Stream newStream, string path)
        {
            Parser.WriteInt32(newStream, signature);
            //long start = newStream.Position;
            int tableSize = entries.Count * 32;
            Parser.WriteInt32(newStream, tableSize);

            long rpkStart = newStream.Position + tableSize;

            for (int i = 0; i < entries.Count; i++)
            {
                long entryStart = newStream.Position;

                // Set the offset
                if (i == 0)
                {
                    entries[i].offset = 0;
                }
                else
                {
                    entries[i].offset = entries[i - 1].offset + entries[i - 1].size;
                }

                // Go to the entry offset and start writing

                oldStream.Position = entries[i].position;
                entries[i].position = rpkStart + entries[i].offset;
                newStream.Position = entries[i].position;
                if (entries[i].data == null)
                {
                    byte[] bytes = Parser.ReadBytes(oldStream, (int)entries[i].size);
                    Parser.WriteBytes(newStream, bytes);
                }
                else if (entries[i].data.GetType() == typeof(Package))
                {
                    ((Package)entries[i].data).WritePackage(oldStream, newStream, path + '\\' + entries[i].name);
                }
                else if (entries[i].data.GetType() == typeof(FileStream))
                {
                    FileStream entryStream = (FileStream)entries[i].data;
                    entryStream.Position = 0;
                    entryStream.CopyTo(newStream);
                    //entries[i].name = entryStream.Name;
                    entryStream.Close();
                    entries[i].data = null;
                    entries[i].extension = GetExtension(path + '\\' + entries[i].name, newStream, entries[i]);
                }

                // Set the size of this entry
                entries[i].size = (uint)(newStream.Position - rpkStart - entries[i].offset);

                // Go back and write the entry data
                newStream.Position = entryStart;
                Parser.WriteStr128(newStream, entries[i].name);
                Parser.WriteUInt32(newStream, entries[i].offset);
                Parser.WriteUInt32(newStream, entries[i].size);
                Parser.WriteInt32(newStream, entries[i].unknown0);
                Parser.WriteInt32(newStream, entries[i].unknown1);
                long next = newStream.Position;
                oldStream.Position = entries[i].position;
            }
            if (entries.Count > 0)
            {
                newStream.Position = rpkStart + entries[entries.Count - 1].offset + entries[entries.Count - 1].size;
            }
        }

        public static string GetExtension(string path, Stream fs, Package.Entry entry)
        {
            long start = fs.Position;
            string extension = Path.GetExtension(path);
            fs.Position = entry.position;
            int signatureCheck = Parser.ReadInt32(fs);
            string parentFolder = Path.GetDirectoryName(path);
            if (extension == "")
            { 
                if (Path.GetExtension(parentFolder) == ".rml") extension = ".mat";
                else if (signatureCheck == Utils.HexStringtoInt32("01 0C BF AF")) extension = ".rpk";
                else if (signatureCheck == Utils.HexStringtoInt32("CF AF 23 3D")) extension = ".rfc";
                else if (signatureCheck == Utils.HexStringtoInt32("C6 3D 2D 1D")) extension = ".rfi";
                else if (signatureCheck == Utils.HexStringtoInt32("52 49 46 46")) extension = ".wav";
                else if (signatureCheck == Utils.HexStringtoInt32("01 AD EF 3E")) extension = ".rft";
                //else
                //{
                //    fs.Position = entry.position;
                //    Chunk chunk = Chunk.StartReadChunk(fs);
                //    if (chunk.size == entry.entrySize) extension = ".chk";
                //    else extension = ".cks";
                //}
            }
            fs.Position = start;
            return extension;
        }
    }
}
