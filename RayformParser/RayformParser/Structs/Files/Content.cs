﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Rayform
{
	// Multipurpose file, contains various forms of game content, like locales or meshes.
	// Found as standalone files with the extension .rfc, or as a section inside save files,
	// as the full copy of the locale itself.
	public class Content : Struct
    {
        public int version;
        public List<Chunk> chunks = new List<Chunk>();

        public void ReadStruct(Stream fs, long size)
        {
            long start = fs.Position;
            version = Parser.ReadInt32(fs);

            while (fs.Position < start + size)
            {
                //Console.WriteLine(fs.Position-size);
                Chunk chunk = Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
            }
        }

        public override void ReadStruct(Stream fs)
        {
            ReadStruct(fs, fs.Length);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, version);

            foreach(Chunk chunk in chunks)
            {
                chunk.WriteChunk(fs);
            }
        }
    }
}
