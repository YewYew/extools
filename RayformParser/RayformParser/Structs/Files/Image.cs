﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Image : Struct
    {
        public int version;
        public int width;
        public int height;
        public int rfi_d;
        public Format format;
        public int rfi_f;
        public short rfi_short_a;
        public short rfi_short_b; //Occurs as 0x1000 or 0x5000
	    public uint size;
        public byte[] dxt;

        public enum Format : uint
        {
            BC1_1  = 0x813BC600,
            BC1_2  = 0x823BC600,
            BC1_3  = 0x0100C600,
            BC1_4  = 0x01004200,
            BC5U_1 = 0x927B8400,
            BC5U_2 = 0x817BE608,
            BC5U_3 = 0x827BA408,
            BC5U_4 = 0x1111C600,
            BC5U_5 = 0x827BE608,
            BC4U_1 = 0x813B4200,
            BC4U_2 = 0x01114200,
            BC4U_3 = 0x01002008,
            BC4U_4 = 0x0100E608,
            BC4U_5 = 0x0111C600,
            BC4U_6 = 0x11118400,
            BC4U_7 = 0x01006208
        }

        public override void ReadStruct(Stream fs)
        {
            version = Parser.ReadInt32(fs);
            width = Parser.ReadInt32(fs);
            height = Parser.ReadInt32(fs);
            rfi_d = Parser.ReadInt32(fs);
            format = (Format)Parser.ReadUInt32(fs);
            rfi_f = Parser.ReadInt32(fs);
            rfi_short_a = Parser.ReadInt16(fs);
            rfi_short_b = Parser.ReadInt16(fs);
            size = Parser.ReadUInt32(fs);
            dxt = Parser.ReadBytes(fs, (int)size);
        }
    }
}
