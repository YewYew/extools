﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rayform
{
    // Saved game, found in rsg files
    public class Save : Struct
    {
        public int version;
        public List<Section> sections = new List<Section>();

        public override void ReadStruct(Stream fs)
        {
            version = Parser.ReadInt32(fs);
            int sections_n = Parser.ReadInt32(fs)-1;
            //the first section is empty, so we skip it
            fs.Position += 24;

            for (int i=0; i < sections_n; i++)
            {
                Section section = new Section();
                section.offset = Parser.ReadUInt32(fs);
                section.size = Parser.ReadUInt32(fs);
                section.name = Parser.ReadStr128(fs);

                long next = fs.Position;
                fs.Position = section.offset;
                Console.WriteLine("Now reading section: " + section.name + " At offset: " + section.offset);

                if (section.name == "#worldchrdb")
                {
                    RdbChars rdb = (RdbChars)Chunk.StartReadChunk(fs);
                    rdb.ReadStruct(fs);
                    rdb.EndReadChunk(fs);
                    section.data = rdb;
                }
                else if (section.name == "#worldobjdb")
                {
                    List<Chunk> rdbs = new List<Chunk>();
                    RdbObjs rdb = (RdbObjs)Chunk.StartReadChunk(fs);
                    rdb.ReadStruct(fs);
                    rdb.EndReadChunk(fs);
                    rdbs.Add(rdb);

                    RdbObjStrings rdbObjStrings = (RdbObjStrings)Chunk.StartReadChunk(fs);
                    rdbObjStrings.ReadStruct(fs);
                    rdbObjStrings.EndReadChunk(fs);
                    rdbs.Add(rdbObjStrings);

                    section.data = rdbs;
                }
                else if (section.name == "#playerdata")
                {
                    Playerdata playerData = new Playerdata();
                    playerData.ReadStruct(fs, section.size);
                    section.data = playerData;
                }
                else if (section.offset > 0)
                {
                    Content content = new Content();
                    content.ReadStruct(fs, section.size);
                    section.data = content;
                }
                fs.Position = next;
                sections.Add(section);
            }
            // Move to the end
            if(sections.Count > 0)
            {
                fs.Position = sections.Last().offset + sections.Last().size;
            }
        }

        public override void WriteStruct(Stream fs)
        {
            uint baseSize = 0x2000;
            Parser.WriteInt32(fs, version);
            Parser.WriteInt32(fs, sections.Count+1);

            //the first section is empty, but these bytes are still required, or it will break the entire save
            Parser.WriteBytes(fs, Utils.HexStringtoBytes("000 00 00 00 0C 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"));

            for (int i=0; i<sections.Count; i++)
            {

                if(i == 0)
                {
                    sections[i].offset = baseSize;
                }
                else
                {
                    sections[i].offset = (sections[i-1].size + baseSize - 1) / baseSize * baseSize + sections[i-1].offset;
                }

                long start = fs.Position;
                fs.Position = sections[i].offset;

                if (sections[i].name == "#worldchrdb")
                {
                    ((Chunk)sections[i].data).WriteChunk(fs);
                }
                else if (sections[i].name == "#worldobjdb")
                {
                    ((List<Chunk>)sections[i].data)[0].WriteChunk(fs);
                    ((List<Chunk>)sections[i].data)[1].WriteChunk(fs);
                }
                else if (sections[i].name == "#playerdata")
                {
                    ((Struct)sections[i].data).WriteStruct(fs);
                }
                else
                {
                    ((Struct)sections[i].data).WriteStruct(fs);
                }

                //Debug.WriteLine(name + " " + start);
                
                sections[i].size = (uint)fs.Position - sections[i].offset;

                //write the section data
                fs.Position = start;
                Parser.WriteUInt32(fs, sections[i].offset);
                Parser.WriteUInt32(fs, sections[i].size);
                Parser.WriteStr128(fs, sections[i].name);
            }
            // Move to the end
            if (sections.Count > 0)
            {
                fs.Position = sections.Last().offset + sections.Last().size;
            }
        }

        // All sections are padded to the 0x2000 byte boundary.
        // The following sections can be found:
        // The first section is always empty, its purpose is unknown
        // #worldchrdb - rdb that contains character chunks
        // #worldobjdb - rdb that contains "objects", such as apparel or weapons
        // #playerdata - struct that contains player info and other miscellaneous chunks
        // If none of these are encountered, it can be assumed that the section is a locale
        public class Section
        {
            [JsonIgnore]
            public uint offset; // the offset from the start of the file
            [JsonIgnore]
            public uint size; // the total amount of bytes that the game will read inside the section
            public Str128 name;
            public object data;
        }
    }
}
