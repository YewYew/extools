﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Project : Struct
    {
        public int version;
        public int flags;
        public Str128 name;
        public List<string> resources = new List<string>();
        public List<string> libNodes = new List<string>();
        public List<string> textures = new List<string>();
        public List<string> materials = new List<string>();

        public override void ReadStruct(Stream fs)
        {
            version = Parser.ReadInt32(fs);
            flags = Parser.ReadInt32(fs);
            name = Parser.ReadStr128(fs);
            int unknown = Parser.ReadInt32(fs);
            resources.Add(Parser.ReadString(fs, Parser.ReadInt32(fs)));

            int libNodes_n = Parser.ReadInt32(fs);
            for (int i = 0; i < libNodes_n; i++)
            {
                libNodes.Add(Parser.ReadString(fs, Parser.ReadInt32(fs)));
            }
            int textures_n = Parser.ReadInt32(fs);
            for (int i = 0; i < textures_n; i++)
            {
                textures.Add(Parser.ReadString(fs, Parser.ReadInt32(fs)));
            }
            int materials_n = Parser.ReadInt32(fs);
            for (int i = 0; i < materials_n; i++)
            {
                materials.Add(Parser.ReadString(fs, Parser.ReadInt32(fs)));
            }
        }
    }
}
