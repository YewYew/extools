﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class Tilemap : Chunk
    {
        public List<Str128> wallSets = new List<Str128>(); // Wallsets; Tile collection names "GEWS" (filenames into Resource.rpk)
        public List<Str128> floorSets = new List<Str128>(); // "GEFS"
        //public int GridExt; // No point in storing these
        //public int GridExt2;
        public int unknown; // Stores 0, reverts back if changed.
        public int gridSize; // Stores 600, which happens to be each tile's width in world units, but this is reverted back to 600 if changed.
        public int gridWidth;
        public int gridHeight;
        public Cell[,] cells;
        /*
        Tiles are stored northwest to northeast in lines advancing south.
	    1 tile width is 600 units.Standing in the middle of the map(if 32x32 tiles, stand between the 4 middle tiles with 16x16 tiles in each quadrant around you) means you have a location of 0, then walking 1 tile east means you have a location of x=600, going back and walking a tile west makes your location x=-600.
        i32 [GridExt*GridExt2] WGrid ; Tile components. Based on cardinal directions, stores wall and corner components separately.
        Each hex digit is a component.
	    WGrid format:
	    >ne<>se< >sw<>nw< eeeessss wwwwnnnn ; Integer view. To isolate ssss, do i&0x0F00.
	    
        Legend:

        6 2 7        1     2    3     4       5          6          7          8
        1   3       west north east south south-west north-west north-east south-east
        5 4 8
        */

        public override void ReadStruct(Stream fs)
        {
            int wallsets_n = Parser.ReadInt32(fs);
            for (int i = 0; i < wallsets_n; i++)
            {
                wallSets.Add(Parser.ReadStr128(fs));
            }
            int floorsets_n = Parser.ReadInt32(fs);
            for (int i = 0; i < floorsets_n; i++)
            {
                floorSets.Add(Parser.ReadStr128(fs));
            }
            gridWidth = Parser.ReadInt32(fs);
            gridHeight = Parser.ReadInt32(fs);
            unknown = Parser.ReadInt32(fs);
            gridSize = Parser.ReadInt32(fs);

            cells = new Cell[gridWidth, gridHeight];
            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    cells[x, y].component = Parser.ReadInt4(fs, 4);
                }
            }

            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    cells[x, y].variant = Parser.ReadInt4(fs, 4);
                }
            }

            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    cells[x, y].set = Parser.ReadInt4(fs, 4);
                }
            }
        }
        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, wallSets.Count);
            foreach (Str128 wallset in wallSets)
            {
                Parser.WriteStr128(fs, wallset);
            }

            Parser.WriteInt32(fs, floorSets.Count);
            foreach (Str128 floorset in floorSets)
            {
                Parser.WriteStr128(fs, floorset);
            }

            Parser.WriteInt32(fs, gridWidth);
            Parser.WriteInt32(fs, gridHeight);
            Parser.WriteInt32(fs, unknown);
            Parser.WriteInt32(fs, gridSize);

            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    Parser.WriteInt4(fs, cells[x, y].component);
                }
            }

            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    Parser.WriteInt4(fs, cells[x, y].variant);
                }
            }

            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    Parser.WriteInt4(fs, cells[x, y].set);
                }
            }
        }

        public struct Cell
        {
            //public static string[] wallGroupsLabels = new string[] { "Unknown", "Wall Cap Left", "Wall Cap Right", "Wall Gap", "Unknown", "Unknown", "Unknown", "Wall", "Unknown", "Unknown", "Unknown", "Doorway", "Unknown", "Unknown", "Unknown", "Unknown" };
            //public static string[] wallGroups = new string[] { "+FBA", "+WLF", "+WRF", "+WLF+WRF", "null", "null", "null", "+WCA", "null", "null", "null", "+WDF", "null", "null", "null", "null" };
            //public static string[] cornersGroups = new string[] { "+FCA", "+CLA", "+CRA", "+CIA", "+COA", "null", "unk", "unk" };
            //public static string[] floorsGroups = new string[] { "+FMA", "unk", "+TNS", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "null" };

        public static GroupSet[] wallGroups = new GroupSet[]
        {
            new GroupSet
            {
                floors = new string[][] { new string[] { "+FBA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+WLF" } },
                floors = new string[][] { new string[] { "+FRA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+WRF" } },
                floors = new string[][] { new string[] { "+FLA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+WLF" }, new string[] { "+WRF" } },
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+WCA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+WDF", "+WDA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            }
        };

        public static GroupSet[] cornersGroups = new GroupSet[]
        {
            new GroupSet
            {
                floors = new string[][] { new string[] { "+FCA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CLA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CRA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CIA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+COA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                floors = new string[][] { new string[] { "+FCA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CLA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CRA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+CIA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "+COA" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[] { "unk" } }
            },
        };

        public static GroupSet[] floorsGroups = new GroupSet[]
        {
            new GroupSet
            {
                floors = new string[][] { new string[]{ "+FMA" } }
            },
            new GroupSet
            {
            },
            new GroupSet
            {
                floors = new string[][] { new string[]{ "+TNS" } }
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
        };

        public static GroupSet[] specialsGroups = new GroupSet[]
        {
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{"+TCD", "+TCC", "+TCR"} },
                maskedTiles = new TileType[] { TileType.West, TileType.North, TileType.East, TileType.South, TileType.NorthWest, TileType.NorthEast, TileType.SouthWest, TileType.Floor }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TPP" } },
                maskedTiles = new TileType[] { TileType.North, TileType.South, TileType.Floor }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TNS" } },
                maskedTiles = new TileType[] { TileType.West, TileType.North, TileType.East, TileType.South, TileType.SouthWest, TileType.NorthWest, TileType.NorthEast, TileType.SouthEast, TileType.Floor }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TAW" } },
                maskedTiles = new TileType[] { TileType.North, TileType.NorthWest, TileType.NorthEast }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TEL" } },
                maskedTiles = new TileType[] { TileType.North, TileType.NorthWest, TileType.NorthEast }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TAL" } },
                maskedTiles = new TileType[] { TileType.North, TileType.NorthWest, TileType.NorthEast }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+TAR" } },
                maskedTiles = new TileType[] { TileType.North, TileType.NorthWest, TileType.NorthEast }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "+unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "unk" } }
            },
            new GroupSet
            {
                walls = new string[][] { new string[]{ "unk" } }
            }
        };

        public Int4[] component; // Tile components. Based on cardinal directions, stores wall and corner components separately.
        public Int4[] variant; // Loads variants
        public Int4[] set; // Tile environment (dungeon, house, etc.)

        public class GroupSet
        {
	        public string name = "Unknown";
            public string[][] walls = new string[][] { new string[] { "unk" } };
            public string[][] floors = new string[][] { new string[] { "unk" } };
            public TileType[] maskedTiles = new TileType[0];
        }

        public enum TileType
            {
                West,
                North,
                East,
                South,
                SouthWest,
                NorthWest,
                NorthEast,
                SouthEast,
                Floor,
                Special
            }

            public Cell(bool value)
            {
                component = new Int4[8];
                variant = new Int4[8];
                set = new Int4[8];
            }

            //public enum SpecialOrientation
            //{
            //    North = 0,
            //    East = 1,
            //    South = 2,
            //    West = 3,
            //    NorthWest,
            //    SouthEast,
            //
            //}

            public int floorSet
            {
                get
                {
                    return set[0];
                }
                set
                {
                    set[0] = value;
                }
            }

            public int wallSet
            {
                get
                {
                    return set[1];
                }
                set
                {
                    set[1] = value;
                }
            }

            public bool isCorner
            {
                get
                {
                    return set[4] >= 8;
                }
                set
                {
                    if (value && set[4] < 8)
                    {
                        set[4] += 8;
                    }
                    else if (!value && set[4] >= 8)
                    {
                        set[4] -= 8;
                    }
                }
            }

            public bool hasSpecial { get { return set[5] > 0; } }

            public bool terrainEnabled
            {
                get
                {
                    return component[4] >= 8;
                }
                set
                {
                    if (value && component[4] < 8)
                    {
                        component[4] += 8;
                    }
                    else if (!value && component[4] >= 8)
                    {
                        component[4] -= 8;
                    }
                }
            }

            public bool tilesetsEnabled
            {
                get
                {
                    return component[5] >= 8;
                }
                set
                {
                    if (value && component[5] < 8)
                    {
                        component[5] += 8;
                    }
                    else if (!value && component[5] >= 8)
                    {
                        component[5] -= 8;
                    }
                }
            }

            public int getRotation(TileType type)
            {
                int rotation = 0;
                if (type == TileType.West || type == TileType.SouthWest)
                {
                    rotation = 270;
                }
                if (type == TileType.North || type == TileType.NorthWest)
                {
                    rotation = 0;
                }
                if (type == TileType.East || type == TileType.NorthEast)
                {
                    rotation = 90;
                }
                if (type == TileType.South || type == TileType.SouthEast)
                {
                    rotation = 180;
                }
                if (type == TileType.Special)
                {
                    int value = set[4];
                    if (value >= 8)
                        value -= 8;

                    if (value == 0)
                        rotation = 0;
                    if (value == 2)
                        rotation = 270;
                    else if (value == 4)
                        rotation = 180;
                    else if (value == 6)
                        rotation = 90;
                }
                return rotation;
                //return (Orientation)((value - 8) / 2);
            }
            
            public bool IsTileActive(TileType type)
            {
                if ((!hasSpecial && type == TileType.Special) || (!tilesetsEnabled)) return false;

                if (hasSpecial)
                {
                    foreach (TileType maskedTile in specialsGroups[set[5]].maskedTiles)
                    {
                        TileType rotatedMaskedTile = RotateTile(maskedTile, getRotation(TileType.Special));
                        if (type == rotatedMaskedTile) return false;
                    }
                }
                return true;
            }

            //public string GetTile(TileType type)
            //{
            //    return GetGroup(type) + GetVariant(type).ToString().PadLeft(2, '0');
            //}

            public TileType[] GetActiveTiles()
            {
                List<TileType> types = new List<TileType>();
                foreach (TileType type in Enum.GetValues(typeof(TileType)))
                {
                    if(IsTileActive(type))
                    types.Add(type);
                }
                return types.ToArray();
            }

            public static TileType RotateTile(TileType type, int rotation)
            {
                TileType rotatedTile = type;

                for (int i = 0; i < rotation / 90; i++)
                {
                    if (rotatedTile == TileType.West) rotatedTile = TileType.North;
                    else if (rotatedTile == TileType.North) rotatedTile = TileType.East;
                    else if (rotatedTile == TileType.East) rotatedTile = TileType.South;
                    else if (rotatedTile == TileType.South) rotatedTile = TileType.West;
                    else if (rotatedTile == TileType.SouthWest) rotatedTile = TileType.NorthWest;
                    else if (rotatedTile == TileType.NorthWest) rotatedTile = TileType.NorthEast;
                    else if (rotatedTile == TileType.NorthEast) rotatedTile = TileType.SouthEast;
                    else if (rotatedTile == TileType.SouthEast) rotatedTile = TileType.SouthWest;
                }
                return rotatedTile;
            }

            public GroupSet GetGroups(TileType type)
            {
                GroupSet groups = new GroupSet();
                if (type == TileType.West)
                {
                    groups = wallGroups[component[0]];
                }
                else if (type == TileType.North)
                {
                    groups = wallGroups[component[1]];
                }
                else if (type == TileType.East)
                {
                    groups = wallGroups[component[2]];
                }
                else if (type == TileType.South)
                {
                    groups = wallGroups[component[3]];
                }
                else if (type == TileType.SouthWest)
                {
                    groups = cornersGroups[component[4]];
                }
                else if (type == TileType.NorthWest)
                {
                    groups = cornersGroups[component[5]];
                }
                else if (type == TileType.NorthEast)
                {
                    groups = cornersGroups[component[6]];
                }
                else if (type == TileType.SouthEast)
                {
                    groups = cornersGroups[component[7]];
                }
                else if (type == TileType.Floor)
                {
                    groups = floorsGroups[set[2]];
                }
                else if (type == TileType.Special)
                {
                    groups = specialsGroups[set[5]];
                }
                return groups;
            }

            public int GetVariant(TileType type)
            {
                int value = 0;
                if (type == TileType.West)
                {
                    value = variant[0];
                }
                else if (type == TileType.North)
                {
                    value = variant[1];
                }
                else if (type == TileType.East)
                {
                    value = variant[2];
                }
                else if (type == TileType.South)
                {
                    value = variant[3];
                }
                else if (type == TileType.SouthWest)
                {
                    value = variant[4];
                }
                else if (type == TileType.NorthWest)
                {
                    value = variant[5];
                }
                else if (type == TileType.NorthEast)
                {
                    value = variant[6];
                }
                else if (type == TileType.SouthEast)
                {
                    value = variant[7];
                }
                else if (type == TileType.Floor)
                {
                    value = set[3];
                }
                else if (type == TileType.Special) //WHAT???
                {
                    value = set[7]*2 + set[4]/8;
                }
                return value;
            }

            //public SetTile(TileType type, string tileName)
            //}
            //	
            //}
        }
    }
}
