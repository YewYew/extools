﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class InventoryItem : Struct
    {
        public HeldItem item = new HeldItem();
        public ushort x; // Position of the item within the inventory, X coordinate.
        public ushort y; // Ditto, Y coordinate.

        public override void ReadStruct(Stream fs)
        {
            item.ReadStruct(fs);
            x = Parser.ReadUInt16(fs);
            y = Parser.ReadUInt16(fs);
        }

		public override void WriteStruct(Stream fs)
		{
            item.WriteStruct(fs);
            Parser.WriteUInt16(fs, x);
            Parser.WriteUInt16(fs, y);
        }
    }
}
