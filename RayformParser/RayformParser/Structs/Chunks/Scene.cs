﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Scene : Chunk // Contains both 3D assets and animations
    {
        public List<Node> nodes = new List<Node>();
        public override void ReadStruct(Stream fs)
        {
            int chunks_n = Parser.ReadInt32(fs);
            for(int i=0; i < chunks_n; i++)
            {
                Debug.WriteLine(fs.Position);
                Node chunk = (Node)Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                nodes.Add(chunk);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, nodes.Count);
            foreach (Node node in nodes)
            {
                node.WriteChunk(fs);
            }
        }

        public class Node : Chunk
        {
            public Str128 nodeName;
            public int nodeFlags;
            public Flags objFlags;
            public int suppress; // Count for something unknown.Also suppresses a large amount of fields if not 0.
            public int parent; // 0 if no parent, objchunks are indexed starting from 1.
            public Matrix3x4 objTransform; // Relative to the parent's origin.
            public Vector3 obj_vec_a;
            public Vector3 obj_vec_b; // Has something to do with clickability on the character screen, and inventory icon space fill
            public float obj_float_a;
            public int obj_int_b;

            public enum Flags
            {
                PointLight = 0x00140100,
                SpotLight = 0x00140200,
                Uknown = 0x10000000,
                Mesh = 0x00231000
            }

            public override void ReadStruct(Stream fs)
            {
                nodeName = Parser.ReadStr128(fs);
                nodeFlags = Parser.ReadInt32(fs);
                objFlags = (Flags)Parser.ReadInt32(fs);
                suppress = Parser.ReadInt32(fs);
                parent = Parser.ReadInt32(fs);
                objTransform = Parser.ReadMatrix(fs);
                obj_vec_a = Parser.ReadVector3(fs);
                obj_vec_b = Parser.ReadVector3(fs);
                obj_float_a = Parser.ReadFloat(fs);
                obj_int_b = Parser.ReadInt32(fs);
                if (suppress == 0)
                {
                    ReadNode(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteStr128(fs, nodeName);
                Parser.WriteInt32(fs, nodeFlags);
                Parser.WriteInt32(fs, (int)objFlags);
                Parser.WriteInt32(fs, suppress);
                Parser.WriteInt32(fs, parent);
                Parser.WriteMatrix(fs, objTransform);
                Parser.WriteVector3(fs, obj_vec_a);
                Parser.WriteVector3(fs, obj_vec_b);
                Parser.WriteFloat(fs, obj_float_a);
                Parser.WriteInt32(fs, obj_int_b);
                if(suppress == 0)
                {
                    WriteNode(fs);
                }
            }

            public virtual void ReadNode(Stream fs)
            {

            }

            public virtual void WriteNode(Stream fs)
            {

            }
        }
    }
}
