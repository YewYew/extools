﻿using System;
using System.IO;

namespace Rayform
{
    public class UnknownChunk : Chunk // If a chunk has not been defined yet, this fallback class is used to prevent data loss
    {
        //public int chunkId;
        public byte[] data;

        public override void ReadStruct(Stream fs)
        {
            chunkSize = Math.Clamp(chunkSize, 0, fs.Length - fs.Position);
            data = Parser.ReadBytes(fs, (int)chunkSize);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteBytes(fs, data);
        }
    }
}
