﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Node : Scene.Node
    {
        public int unknown1;
        public int unknown2;
        public int unknown3;
        public override void ReadNode(Stream fs)
        {
            unknown1 = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);
            unknown3 = Parser.ReadInt32(fs);
        }
    }
}
