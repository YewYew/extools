﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class LightSource : Scene.Node
    {
        public LightFlags lightFlags;
        public Color color;
        public float ambient;
        public float brightness;
        public float distance;
        public float distanceParam;
        public float shadowSoften;
        // if objFlags & 0x300 == 0x200
        public float spotCut;
        public float spotSoft;
        // if LgtFlags & 0x30000 != 0
        public int unknown;
        public float animStrength;
        public float animSpeed;
        public float animEmission;

        public enum LightFlags
        {
            Animation = 0x30000
        }

        public override void ReadNode(Stream fs)
        {
            lightFlags = (LightFlags)Parser.ReadInt32(fs);
            color = Parser.ReadColor(fs);
            ambient = Parser.ReadFloat(fs);
            brightness = Parser.ReadFloat(fs);
            distance = Parser.ReadFloat(fs);
            distanceParam = Parser.ReadFloat(fs);
            shadowSoften = Parser.ReadFloat(fs);

            if (objFlags.HasFlag(Flags.SpotLight)) // if objFlags contains this flag
            {
                spotCut = Parser.ReadFloat(fs);
                spotSoft = Parser.ReadFloat(fs);
            }
            if (lightFlags.HasFlag(LightFlags.Animation)) // if LgtFlags contains this flag
            {
                unknown = Parser.ReadInt32(fs);
                animStrength = Parser.ReadFloat(fs);
                animSpeed = Parser.ReadFloat(fs);
                animEmission = Parser.ReadFloat(fs);
            }

            fs.Position = end;
        }

        public override void WriteNode(Stream fs)
        {
            Parser.WriteInt32(fs, (int)lightFlags);
            Parser.WriteColor(fs, color);
            Parser.WriteFloat(fs, ambient);
            Parser.WriteFloat(fs, brightness);
            Parser.WriteFloat(fs, distance);
            Parser.WriteFloat(fs, distanceParam);
            Parser.WriteFloat(fs, shadowSoften);

            if ((objFlags & Flags.SpotLight) != 0)
            {
                Parser.WriteFloat(fs, spotCut);
                 Parser.WriteFloat(fs, spotSoft);
            }
            if ((lightFlags & LightFlags.Animation) != 0)
            {
                Parser.WriteInt32(fs, unknown);
                 Parser.WriteFloat(fs, animStrength);
                 Parser.WriteFloat(fs, animSpeed);
                 Parser.WriteFloat(fs, animEmission);
            }
        }
    }
}
