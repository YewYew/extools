﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using static Rayform.CharacterPosition;

namespace Rayform
{
    public class Enviroment : Chunk
    {
        public int unknown1;
        public int unknown2;
        public float unknown3;
        public float unknown4;
        public float unknown5;
        public float unknown6;
        public float unknown7;
        public float unknown8;
        public Color fog;
        public Color sunlightColor; // Fairly sure of this one?
        public Color ambient;
        public float unknown9;
        public float unknown10;
        public float unknown11;
        public int unknown12;
        public float unknown13;
        public float unknown14;
        public float unknown15;
        public float unknown16;
        public float unknown17;
        public float unknown18;
        public float unknown19;
        public float unknown20;
        public float unknown21;
        public float unknown22;
        public float unknown23;
        public float unknown24;
        public float unknown25;
        public float unknown26;
        public int unknown27;
        public float unknown28;
        public int unknown29;
        public int unknown30;
        public int unknown31;
        public Str128 envMap;
        public int unknown32;
        public int unknown33;
        public int unknown34;
        public int unknown35;
        public int unknown36;
        public int unknown37;
        public int unknown38;
        public int unknown39;
        public int unknown40;
        public int unknown41;
        public int unknown42;
        public int unknown43;
        public int unknown44;
        public int unknown45;
        public int unknown46;

        public override void ReadStruct(Stream fs)
		{
			unknown1 = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);
            unknown3 = Parser.ReadFloat(fs);
            unknown4 = Parser.ReadFloat(fs);
            unknown5 = Parser.ReadFloat(fs);
            unknown6 = Parser.ReadFloat(fs);
            unknown7 = Parser.ReadFloat(fs);
            unknown8 = Parser.ReadFloat(fs);
            fog = Parser.ReadColor(fs);
            sunlightColor = Parser.ReadColor(fs);
            ambient = Parser.ReadColor(fs);
            unknown9 = Parser.ReadFloat(fs);
            unknown10 = Parser.ReadFloat(fs);
            unknown11 = Parser.ReadFloat(fs);
            unknown12 = Parser.ReadInt32(fs);
            unknown13 = Parser.ReadFloat(fs);
            unknown14 = Parser.ReadFloat(fs);
            unknown15 = Parser.ReadFloat(fs);
            unknown16 = Parser.ReadFloat(fs);
            unknown17 = Parser.ReadFloat(fs);
            unknown18 = Parser.ReadFloat(fs);
            unknown19 = Parser.ReadFloat(fs);
            unknown20 = Parser.ReadFloat(fs);
            unknown21 = Parser.ReadFloat(fs);
            unknown22 = Parser.ReadFloat(fs);
            unknown23 = Parser.ReadFloat(fs);
            unknown24 = Parser.ReadFloat(fs);
            unknown25 = Parser.ReadFloat(fs);
            unknown26 = Parser.ReadFloat(fs);
            unknown27 = Parser.ReadInt32(fs);
            unknown28 = Parser.ReadFloat(fs);
            unknown29 = Parser.ReadInt32(fs);
            unknown30 = Parser.ReadInt32(fs);
            unknown31 = Parser.ReadInt32(fs);
            envMap = Parser.ReadStr128(fs);
            unknown32 = Parser.ReadInt32(fs);
            unknown33 = Parser.ReadInt32(fs);
            unknown34 = Parser.ReadInt32(fs);
            unknown35 = Parser.ReadInt32(fs);
            unknown36 = Parser.ReadInt32(fs);
            unknown37 = Parser.ReadInt32(fs);
            unknown38 = Parser.ReadInt32(fs);
            unknown39 = Parser.ReadInt32(fs);
            unknown40 = Parser.ReadInt32(fs);
            unknown41 = Parser.ReadInt32(fs);
            unknown42 = Parser.ReadInt32(fs);
            unknown43 = Parser.ReadInt32(fs);
            unknown44 = Parser.ReadInt32(fs);
            unknown45 = Parser.ReadInt32(fs);
            unknown46 = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteInt32(fs, unknown2);
            Parser.WriteFloat(fs, unknown3);
            Parser.WriteFloat(fs, unknown4);
            Parser.WriteFloat(fs, unknown5);
            Parser.WriteFloat(fs, unknown6);
            Parser.WriteFloat(fs, unknown7);
            Parser.WriteFloat(fs, unknown8);
            Parser.WriteColor(fs, fog);
            Parser.WriteColor(fs, sunlightColor);
            Parser.WriteColor(fs, ambient);
            Parser.WriteFloat(fs, unknown9);
            Parser.WriteFloat(fs, unknown10);
            Parser.WriteFloat(fs, unknown11);
            Parser.WriteInt32(fs, unknown12);
            Parser.WriteFloat(fs, unknown13);
            Parser.WriteFloat(fs, unknown14);
            Parser.WriteFloat(fs, unknown15);
            Parser.WriteFloat(fs, unknown16);
            Parser.WriteFloat(fs, unknown17);
            Parser.WriteFloat(fs, unknown18);
            Parser.WriteFloat(fs, unknown19);
            Parser.WriteFloat(fs, unknown20);
            Parser.WriteFloat(fs, unknown21);
            Parser.WriteFloat(fs, unknown22);
            Parser.WriteFloat(fs, unknown23);
            Parser.WriteFloat(fs, unknown24);
            Parser.WriteFloat(fs, unknown25);
            Parser.WriteFloat(fs, unknown26);
            Parser.WriteInt32(fs, unknown27);
            Parser.WriteFloat(fs, unknown28);
            Parser.WriteInt32(fs, unknown29);
            Parser.WriteInt32(fs, unknown30);
            Parser.WriteInt32(fs, unknown31);
            Parser.WriteStr128(fs, envMap);
            Parser.WriteInt32(fs, unknown32);
            Parser.WriteInt32(fs, unknown33);
            Parser.WriteInt32(fs, unknown34);
            Parser.WriteInt32(fs, unknown35);
            Parser.WriteInt32(fs, unknown36);
            Parser.WriteInt32(fs, unknown37);
            Parser.WriteInt32(fs, unknown38);
            Parser.WriteInt32(fs, unknown39);
            Parser.WriteInt32(fs, unknown40);
            Parser.WriteInt32(fs, unknown41);
            Parser.WriteInt32(fs, unknown42);
            Parser.WriteInt32(fs, unknown43);
            Parser.WriteInt32(fs, unknown44);
            Parser.WriteInt32(fs, unknown45);
            Parser.WriteInt32(fs, unknown46);
        }
	}
}