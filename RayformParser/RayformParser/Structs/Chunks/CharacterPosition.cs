﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class CharacterPosition : Chunk
	{
		public Id characterId = new Id(); // The ID of the character to look up in the appropriate character DB.
		public Vector3 locOrigin; // Vector: XYZ
		public float locOrientation; // In the range [-π, π].
		public Flags flags; // Unknown. 0xA00 when only chunk 0xCAC0E100 is present, or 0x1 when chunk 0xCAC0EA00 is present.

		// TODO: The documentation claims that there are three possible chunk types in here.
		// One is the BonePositions chunk below,
		// the second is unknown and only rarely appears in save files,
		// and the third has yet to be seen in a real savefile, but appears in the code.
		// However, here we only have one type of unknown. Does it mean we need to add another?
		public List<Chunk> chunks = new List<Chunk>();

		public enum Flags : uint
		{
			Alive = 0,
			Attacking = 2,
			Dead = 0xA00
		}
		public override void ReadStruct(Stream fs)
		{
			characterId.ReadStruct(fs);
			locOrigin = Parser.ReadVector3(fs);
			locOrientation = Parser.ReadFloat(fs);
			flags = (Flags)Parser.ReadUInt32(fs);
			while (fs.Position < end)
			{
				Chunk chunk = StartReadChunk(fs);
				chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
			}
		}

		public override void WriteStruct(Stream fs)
		{
			characterId.WriteStruct(fs);
			Parser.WriteVector3(fs, locOrigin);
			Parser.WriteFloat(fs, locOrientation);
			Parser.WriteUInt32(fs, (uint)flags);
			foreach(Chunk chunk in chunks)
			{
				chunk.WriteChunk(fs);
			}
		}

		public class BonePositions : Chunk
        {
            public List<Matrix3x4> transforms = new List<Matrix3x4>();

			public override void ReadStruct(Stream fs)
			{
				while (fs.Position < end)
				{
					Matrix3x4 transform = Parser.ReadMatrix(fs);
					transforms.Add(transform);
				}
			}

			public override void WriteStruct(Stream fs)
			{
				foreach(Matrix3x4 transform in transforms)
				{
					Parser.WriteMatrix(fs, transform);
				}
			}
		}

		public class Unknown : Chunk
		{
			public int unknown0;
			public int unknown1;
			public int unknown2;

            public override void ReadStruct(Stream fs)
            {
                unknown0 = Parser.ReadInt32(fs);
				unknown1 = Parser.ReadInt32(fs);
				unknown2 = Parser.ReadInt32(fs);
			}

            public override void WriteStruct(Stream fs)
            {
				Parser.WriteInt32(fs, unknown0);
				Parser.WriteInt32(fs, unknown1);
				Parser.WriteInt32(fs, unknown2);
            }
        }
    }
}