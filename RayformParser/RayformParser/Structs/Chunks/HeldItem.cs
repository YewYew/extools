﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class HeldItem : Struct
    {
		public Id id = new Id();
        public ObjClass.Flags flags;

        public override void ReadStruct(Stream fs)
        {
            id.ReadStruct(fs);
            flags = (ObjClass.Flags)Parser.ReadInt32(fs);
        }

		public override void WriteStruct(Stream fs)
		{
            id.WriteStruct(fs);
			Parser.WriteInt32(fs, (int)flags);
		}
	}
}
