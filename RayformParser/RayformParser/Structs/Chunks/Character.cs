﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class Character : Chunk
    {
        public Str128 name;
        public Str128 surname;
        public float weight = 0.5f; // Fat
        public float physique = 0.5f; // Muscle
        public float height;
        public float age;
        public ushort skinX;
        public ushort skinY;
        public int hairStyle;
        public byte hairHue; // X
        public byte hairBrightness; // Y
        public byte hairGreyness;
        public byte unknown1;
        public int hairNull;
        public Str128 voice;
        public float voicePitch;
        public CharacterFlags characterFlags;
        public Race race;
        public byte hairFlags; // Beard and hair texture.
        public bool altLoadout;
        public int lastLocale;
        public HeldItem[] handEquips = new HeldItem[2]; // Main loadout: Right hand, Left hand
        public HeldItem[] handEquips2 = new HeldItem[2]; // Secondary Loadout: Right hand, Left hand
        public List<HeldItem> equippedItems = new List<HeldItem>(); // Equipped apparel, with a capacity of int.
        public float decomposition; // Zombification, used in the range[0.0, 1.0], high values being more sloppy. If Decomposition is not zero, characters have zombie voices and will not wake up from a KO.
        public ushort health = ushort.MaxValue; //from 0 to 0xFFFF
        public ushort stamina = ushort.MaxValue; //from 0 to 0xFFFF
        public ushort focusStamina = ushort.MaxValue; //from 0 to 0xFFFF
        public ushort focusHealth = ushort.MaxValue; //from 0 to 0xFFFF
        public float combatSkill; // Combat skill in the range[0, 100]. Is calculated from the amount of learned skills.
        public float undeadVoice;
        public Personality personality = new Personality();
        public int RNGSeed;
        public int experience;
        public int unknown2;
        public List<Skill> skills = new List<Skill>(); // Int capacity.
        public List<InventoryItem> inventory = new List<InventoryItem>(); // Items in the inventory.
        public List<Role> roles = new List<Role>();
        public Relations relations = new Relations();
        public List<WorldRelations> worldRelations = new List<WorldRelations>();
        public List<Condition> conditions = new List<Condition>();
        //public int unknown5;
        //public int unknown6;
        public List<Chunk> chunks = new List<Chunk>(); // Additional information, like arena loadouts appended at the end of the character as chunks.

        public Character()
        {
            handEquips[0] = new HeldItem();
            handEquips[1] = new HeldItem();
            handEquips2[0] = new HeldItem();
            handEquips2[1] = new HeldItem();
        }

        public enum CharacterFlags : byte
        {
            //Male = 0,
            Female = 1,
            Unraisable = 64
        }

        public enum Race : byte
        {
            Human,
            Skeleton,
            Ancient,
            Ogre,
            Sir,
            Fael,
            Ghoul,
            AlphaGhoul,
            Gobbler,
            Controller,
            Unused1,
            Unused2,
            Unused3,
            Unused4,
            Unused5,
            Wraith,
            Golem,
            GolemBoss,
            GolemSC
        }

        // Does nothing for the player character, but still present.
        public class Personality
        {
            public byte trustfulness;
            public byte bravery;
            public byte benAch;
            public byte neuroticism;
        }

        public class WorldRelations : Struct
        {
            public int unknown1;
            public int unknown2;
            public Relations relations = new Relations();

            public override void ReadStruct(Stream fs)
            {
                unknown1 = Parser.ReadInt32(fs);
                unknown2 = Parser.ReadInt32(fs);
                relations = new Relations();
                relations.ReadStruct(fs);
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteInt32(fs, unknown1);
                Parser.WriteInt32(fs, unknown2);
                relations.WriteStruct(fs);
            }
        }

        public class Relations : Struct
        {
            public List<Entry> entries = new List<Entry>();
            public uint dataSize;

            public override void ReadStruct(Stream fs)
            {
                //long start = fs.Position;
                int relations_n = Parser.ReadInt32(fs);

                if (relations_n > 0)
                {
                    long tableSize = relations_n * 8;
                    long dataStart = fs.Position + tableSize + 4;

                    //int size = Parser.ReadInt32(fs);

                    for (int i = 0; i < relations_n; i++)
                    {
                        Entry entry = new Entry();
                        entry.id = Parser.ReadInt32(fs);
                        entry.entryOffset = Parser.ReadUInt32(fs);
                        long next = fs.Position;
                        fs.Position = dataStart + entry.entryOffset;
                        entry.entrySize = Parser.ReadUInt32(fs);
                        entry.data = Parser.ReadBytes(fs, (int)(entry.entrySize - 4));
                        entries.Add(entry);
                        fs.Position = next;
                        //if (i < relations_n-1)
                        //{
                        //    fs.Position = next;
                        //}
                    }
                    dataSize = Parser.ReadUInt32(fs);
                    fs.Position += dataSize;
                    //long dataSize = Parser.ReadInt32(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteInt32(fs, entries.Count);
                if (entries.Count > 0)
                {
                    long tableSize = entries.Count * 8;
                    long dataStart = fs.Position + tableSize + 4;

                    for (int i = 0; i < entries.Count; i++)
                    {
                        // Set the offset
                        if (i == 0)
                        {
                            entries[i].entryOffset = 0;
                        }
                        else
                        {
                            entries[i].entryOffset = entries[i - 1].entryOffset + entries[i - 1].entrySize;
                        }

                        Parser.WriteInt32(fs, entries[i].id);
                        Parser.WriteUInt32(fs, entries[i].entryOffset);
                        long next = fs.Position;

                        // go to the entry offset and start writing
                        // Set the size of this entry
                        fs.Position = dataStart + entries[i].entryOffset;
                        entries[i].entrySize = (uint)(entries[i].data.Length + 4);
                        Parser.WriteUInt32(fs, entries[i].entrySize);
                        Parser.WriteBytes(fs, entries[i].data);

                        fs.Position = next;

                    }

                    dataSize = entries.Last().entryOffset + entries.Last().entrySize;
                    Parser.WriteUInt32(fs, dataSize);
                    fs.Position = dataStart + dataSize;
                }
            }

            public class Entry
            {
                public int id;
                public uint entryOffset;
                public uint entrySize;
                public byte[] data = new byte[0];
            }
        }

        public class Condition : Struct
        {
            public int type;
            public float conditionRemaining;

            public override void ReadStruct(Stream fs)
            {
                type = Parser.ReadInt32(fs);
                if (type == Utils.HexStringtoInt32("02 00 CC CE"))
                {
                    conditionRemaining = (float)Parser.ReadInt32(fs);
                }
                if (type == Utils.HexStringtoInt32("02 00 CC CE"))
                {
                    conditionRemaining = Parser.ReadFloat(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteInt32(fs, type);
                if (type == Utils.HexStringtoInt32("02 00 CC CE"))
                {
                    Parser.WriteInt32(fs, (int)conditionRemaining);
                }
                if (type == Utils.HexStringtoInt32("02 00 CC CE"))
                {
                    Parser.WriteFloat(fs, conditionRemaining);
                }
                //if (type == Utils.HexStringtoInt32("02 00 CC CE"))
                //{
                //    Parser.WriteFloat(fs, remainingTime);
                //}
            }
        }

        public class Role : Struct
        {
            public Id id = new Id();
            public int flags;

            public override void ReadStruct(Stream fs)
            {
                id.ReadStruct(fs);
                flags = Parser.ReadInt32(fs);
            }

            public override void WriteStruct(Stream fs)
            {
                id.WriteStruct(fs);
                Parser.WriteInt32(fs, flags);
            }
        }

        // A possible chunk appended below the character. Contains equipment loadouts for the arena.
        public class ArenaLoadouts : Chunk
        {
            public List<Loadout> loadouts = new List<Loadout>();

            public class Loadout : Struct
            {
                public int loadoutV; // Loadout version? Need info.
                public HeldItem[] handEq = new HeldItem[2]; // Main loadout: Right hand, Left hand
                public HeldItem[] handEq2 = new HeldItem[2]; // Secondary Loadout: Right hand, Left hand
                public List<HeldItem> equips = new List<HeldItem>(); // Equipped apparel

                public override void ReadStruct(Stream fs)
                {
                    loadoutV= fs.ReadByte();
                    handEq[0].ReadStruct(fs);
                    handEq[1].ReadStruct(fs);
                    handEq2[0].ReadStruct(fs);
                    handEq2[1].ReadStruct(fs);

                    int equips_n = Parser.ReadInt32(fs);
                    for (int i = 0; i < equips_n; i++)
                    {
                        HeldItem item = new HeldItem();
                        item.ReadStruct(fs);
                        equips.Add(item);
                    }
                }

				public override void WriteStruct(Stream fs)
                {
                    Parser.WriteInt32(fs, loadoutV);
                    handEq[0].WriteStruct(fs);
                    handEq[1].WriteStruct(fs);
                    handEq2[0].WriteStruct(fs);
                    handEq2[1].WriteStruct(fs);

                    foreach(HeldItem item in equips)
                    {
                        item.WriteStruct(fs);
                    }
                }
            }
        }

        public override void ReadStruct(Stream fs)
        {
            name = Parser.ReadStr128(fs);
            surname = Parser.ReadStr128(fs);
            weight = Parser.ReadFloat(fs);
            physique = Parser.ReadFloat(fs);
            height = Parser.ReadFloat(fs);
            age = Parser.ReadFloat(fs);
            skinX = Parser.ReadUInt16(fs);
            skinY = Parser.ReadUInt16(fs);
            hairStyle = Parser.ReadInt32(fs);
            hairHue = Parser.ReadInt8(fs);
            hairBrightness = Parser.ReadInt8(fs);
            hairGreyness = Parser.ReadInt8(fs);
            unknown1 = Parser.ReadInt8(fs);
            hairNull = Parser.ReadInt32(fs);
            voice = Parser.ReadStr128(fs);
            voicePitch = Parser.ReadFloat(fs);
            characterFlags = (CharacterFlags)Parser.ReadInt8(fs);
            race = (Race)Parser.ReadInt8(fs);
            hairFlags = Parser.ReadInt8(fs);
            altLoadout = Parser.ReadBoolean(fs);
            lastLocale = Parser.ReadInt32(fs);
            handEquips[0] = new HeldItem();
            handEquips[0].ReadStruct(fs);
            handEquips[1] = new HeldItem();
            handEquips[1].ReadStruct(fs);
            handEquips2[0] = new HeldItem();
            handEquips2[0].ReadStruct(fs);
            handEquips2[1] = new HeldItem();
            handEquips2[1].ReadStruct(fs);

            int equips_n = Parser.ReadInt32(fs);
            for (int i = 0; i < equips_n; i++)
            {
                HeldItem item = new HeldItem();
                item.ReadStruct(fs);
                equippedItems.Add(item);
            }

            decomposition = Parser.ReadFloat(fs);
            stamina = Parser.ReadUInt16(fs);
            health = Parser.ReadUInt16(fs);
            focusStamina = Parser.ReadUInt16(fs);
            focusHealth = Parser.ReadUInt16(fs);
            combatSkill = Parser.ReadFloat(fs);
            undeadVoice = Parser.ReadFloat(fs);

            //personality = new Personality();
            personality.trustfulness = Parser.ReadInt8(fs);
            personality.bravery = Parser.ReadInt8(fs);
            personality.benAch = Parser.ReadInt8(fs);
            personality.neuroticism = Parser.ReadInt8(fs);

            RNGSeed = Parser.ReadInt32(fs);
            experience = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);

            int skills_n = Parser.ReadInt32(fs);
            for (int i = 0; i < skills_n; i++)
            {
                Skill skill = new Skill();
                skill.ReadStruct(fs);
                skills.Add(skill);
            }
            
            int items_n = Parser.ReadInt32(fs);
            for (int i = 0; i < items_n; i++)
            {
                InventoryItem item = new InventoryItem();
                item.ReadStruct(fs);
                inventory.Add(item);
            }
            
            int roles_n = Parser.ReadInt32(fs);
            for (int i = 0; i < roles_n; i++)
            {
                Role role = new Role();
                role.ReadStruct(fs);
                roles.Add(role);
            }

            relations = new Relations();
            relations.ReadStruct(fs);

            int locales_n = Parser.ReadInt32(fs);
            for (int i = 0; i < locales_n; i++)
            {
                WorldRelations worldRelation = new WorldRelations();
                worldRelation.ReadStruct(fs);
                worldRelations.Add(worldRelation);
            }

            //fs.Position = end;
            //return;

            int conditions_n = Parser.ReadInt32(fs);
            for (int i = 0; i < conditions_n; i++)
            {
                Condition condition = new Condition();
                condition.ReadStruct(fs);
                conditions.Add(condition);
            }

            //unknown5 = Parser.ReadInt32(fs);

            //if(worldRelations.Count==0)
            //{
            //unknown6 = Parser.ReadInt32(fs);
            //}
            //unknown6 = (int)(end - fs.Position);

            while (fs.Position < end)
            { 
                Chunk chunk = Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
            }
        }

		public override void WriteStruct(Stream fs)
		{
			Parser.WriteStr128(fs, name);
			Parser.WriteStr128(fs, surname);
			Parser.WriteFloat(fs, weight);
			Parser.WriteFloat(fs, physique);
			Parser.WriteFloat(fs, height);
			Parser.WriteFloat(fs, age);
			Parser.WriteUInt16(fs, skinX);
			Parser.WriteUInt16(fs, skinY);
			Parser.WriteInt32(fs, hairStyle);
			Parser.WriteInt8(fs, hairHue);
			Parser.WriteInt8(fs, hairBrightness);
			Parser.WriteInt8(fs, hairGreyness);
            Parser.WriteInt8(fs, unknown1);
            Parser.WriteInt32(fs, hairNull);
			Parser.WriteStr128(fs, voice);
			Parser.WriteFloat(fs, voicePitch);
			Parser.WriteInt8(fs, (byte)characterFlags);
			Parser.WriteInt8(fs, (byte)race);
			Parser.WriteInt8(fs, hairFlags);
			Parser.WriteBoolean(fs, altLoadout);
			Parser.WriteInt32(fs, lastLocale);
            handEquips[0].WriteStruct(fs);
            handEquips[1].WriteStruct(fs);
			handEquips2[0].WriteStruct(fs);
			handEquips2[1].WriteStruct(fs);

            Parser.WriteInt32(fs, equippedItems.Count);
            foreach (HeldItem item in equippedItems)
            {
                item.WriteStruct(fs);
            }

			Parser.WriteFloat(fs, decomposition);
			Parser.WriteUInt16(fs, stamina);
			Parser.WriteUInt16(fs, health);
			Parser.WriteUInt16(fs, focusStamina);
			Parser.WriteUInt16(fs, focusHealth);
			Parser.WriteFloat(fs, combatSkill);
			Parser.WriteFloat(fs, undeadVoice);

			Parser.WriteInt8(fs, personality.trustfulness);
			Parser.WriteInt8(fs, personality.bravery);
			Parser.WriteInt8(fs, personality.benAch);
			Parser.WriteInt8(fs, personality.neuroticism);

            Parser.WriteInt32(fs, RNGSeed);
            Parser.WriteInt32(fs, experience);
            Parser.WriteInt32(fs, unknown2);

            Parser.WriteInt32(fs, skills.Count);
            foreach (Skill skill in skills)
            {
                skill.WriteStruct(fs);
            }

            Parser.WriteInt32(fs, inventory.Count);
            foreach (InventoryItem inventoryItem in inventory)
			{
				inventoryItem.WriteStruct(fs);
			}

            Parser.WriteInt32(fs, roles.Count);
            foreach (Role role in roles)
			{
				role.WriteStruct(fs);
			}

            relations.WriteStruct(fs);

            Parser.WriteInt32(fs, worldRelations.Count);
            foreach (WorldRelations worldRelation in worldRelations)
            {
                worldRelation.WriteStruct(fs);
            }

            Parser.WriteInt32(fs, conditions.Count);
            foreach (Condition condition in conditions)
            {
                condition.WriteStruct(fs);
            }

			//Parser.WriteInt32(fs, unknown5);
			//Parser.WriteInt32(fs, unknown6);

            foreach (Chunk chunk in chunks)
            {
                chunk.WriteChunk(fs);
            }
        }
	}
}