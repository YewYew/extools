﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public enum MeleeAbilities
    {
        Empty,
	    Remise,
	    Feint, // Cease a feined attack to quickly renew it.
	    Fend, // Abandon your attack to quickly parry.
	    Riposte, // After a parry, rapidly follow with an attack.
	    Draw, // Unimplemented. Cut or hook with your weapon after landing a blow.
	    Impel, // Unimplemented. Same description as Fend.
	    Brace, // Unimplemented. Use stronger bracing parries to stop more powerful blows.
	    DualWield // Use two weapons to attack and defend simultaneously.
    }
    public enum ArmorAbilities
    {
        none = 0
    }
    public enum ShieldAbilities
    {
        none = 0
    }
    public enum InsightAbilities
    {
        none = 0
    }
    public enum ConcentrationAbilities
    {
        none = 0
    }

    public class Skill : Struct
    {
        public short skillId;
        public ushort skillExp;
        //[FieldSize(typeof(byte))]
        public List<byte> abilities = new List<byte>();

        public override void ReadStruct(Stream fs)
        {
            skillId = Parser.ReadInt16(fs);
            skillExp = Parser.ReadUInt16(fs);
            byte abilities_n = Parser.ReadInt8(fs);
            for(int i=0; i < abilities_n; i++)
            {
                abilities.Add(Parser.ReadInt8(fs));
            }
        }

		public override void WriteStruct(Stream fs)
		{
			Parser.WriteInt16(fs, skillId);
            Parser.WriteUInt16(fs, skillExp);
            Parser.WriteInt8(fs, (byte)abilities.Count);
            foreach(byte ability in abilities)
            {
                Parser.WriteInt8(fs, ability);
            }
        }
    }

}
