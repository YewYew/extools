﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Rayform.CharacterPosition;
using static Rayform.Exploration;

namespace Rayform
{
    public class GlobalXP : Chunk // Why? It's already stored in the set. MADOC MOMENT
    {
        public int xp;

        public override void ReadStruct(Stream fs)
        {
            xp = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, xp);
        }
    }
}
