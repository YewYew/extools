﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Rayform;

namespace Rayform
{
    public class Rdb : Chunk
    {
        public int version;
        [JsonIgnore]
        public int tableSize;
        public List<Entry> entries = new List<Entry>();

        public override void ReadStruct(Stream fs)
        {
            version = Parser.ReadInt32(fs);
            tableSize = Parser.ReadInt32(fs);
            long rdbStart = fs.Position + tableSize;
            while (fs.Position < rdbStart - 16)
            {
                Entry entry = new Entry();
                entry.baseId = Parser.ReadInt32(fs);
                entry.entryType = (Entry.EntryType)Parser.ReadUInt32(fs);
                entry.entryOffset = Parser.ReadUInt32(fs);
                entry.entrySize = Parser.ReadUInt32(fs);
                Console.WriteLine("Now reading Rdb entry at offset: " + entry.entryOffset + " Size: " + entry.entrySize);
                long next = fs.Position;
                fs.Position = rdbStart + entry.entryOffset;
                entry.data = ReadRdbData(fs, entry.entrySize);
                entries.Add(entry);
                fs.Position = next;
            }
        }

        public override void WriteStruct(Stream fs)
        {
            //long start = fs.Position;

            Parser.WriteInt32(fs, version);

            tableSize = (entries.Count + 1) * 16;
            Parser.WriteInt32(fs, tableSize);

            long rdbStart = fs.Position + tableSize;

            for (int i = 0; i < entries.Count; i++)
            {
                long entryStart = fs.Position;

                // Set the offset
                if (i == 0)
                {
                    entries[i].entryOffset = 0;
                }
                else
                {
                    entries[i].entryOffset = entries[i - 1].entryOffset + entries[i - 1].entrySize;
                }

                // go to the entry offset and start writing
                fs.Position = rdbStart + entries[i].entryOffset;
                WriteRdbData(fs, entries[i].data);

                // Set the size of this entry
                entries[i].entrySize = (uint)(fs.Position - rdbStart - entries[i].entryOffset);

                // Go back and write the entry data
                fs.Position = entryStart;
                Parser.WriteInt32(fs, entries[i].baseId);
                Parser.WriteUInt32(fs, (uint)entries[i].entryType);
                Parser.WriteUInt32(fs, entries[i].entryOffset);
                Parser.WriteUInt32(fs, entries[i].entrySize);
            }
            // Done, so write the fake entry and move back to the end of the chunk
            fs.Position = rdbStart - 16;
            Parser.WriteBytes(fs, Utils.HexStringtoBytes("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"));
            if (entries.Count > 0)
            {
                fs.Position = rdbStart + entries[entries.Count - 1].entryOffset + entries[entries.Count - 1].entrySize;
            }
        }

        public virtual object ReadRdbData(Stream fs, uint size)
        {
            Console.WriteLine("No reading support for this rdb type");
            return null;
        }

        public virtual void WriteRdbData(Stream fs, object data)
        {
            Console.WriteLine("No writing support for this rdb type");
        }

        // The struct used for rdb indexing
        public class Entry
        {
            public int baseId;
            public EntryType entryType;
            public uint entryOffset;
            public uint entrySize;
            public object data;
			
			public enum EntryType : uint
			{
				None = 0,
				Template = 0x10000000,
				Hazard = 0x10010000, // Fire, Mechanism, Electricity (Operative1)
				Misc = 0x10100000, // Scroll, Generic
				DropTable = 0x10100200,
				Apparel = 0x10101000,
				Weapon = 0x10104000,
				Key = 0x10110000, // Key, Salve, Seal, Lever, Fire (Operative2)
				Container = 0x10120000,
				Door = 0x10140000, // Some Doors can be Operatives
				Zone = 0x9000AC07, // Triggers
                Character = 0x225F5CA0,
                Shield = 0x10108000,
                Underwear = 0x10001000
            }
        }

        public static object FindRdbEntry(Content locale, Save save, Id id, Type type)
        {
            Rdb rdb = FindRdb(locale, save, id.flags, type);
            if (rdb != null)
            {
                Rdb.Entry entry = rdb.entries.FirstOrDefault(x => x.baseId == id.baseId);
                if (entry != null)
                {
                    return entry.data;
                }
            }
            return null;
        }

        public static Rdb FindRdb(Content locale, Save save, Id.Flags flags, Type type)
        {
            if (type == typeof(RdbObjs))
            {
                if (flags == Id.Flags.Global)
                {
                    return (RdbObjs)PackageManager.LoadByPath("Resource.rpk\\objdb.rdb");
                }
                else if (flags == Id.Flags.World)
                {
                    if (save != null)
                    {
                        Save.Section section = save.sections.FirstOrDefault(x => x.name == "#worldobjdb");

                        if (section != null)
                        {
                            return ((List<Chunk>)section.data).OfType<RdbObjs>().FirstOrDefault();
                        }
                    }
                }
                else if (flags == Id.Flags.Local)
                {
                    if (locale != null)
                    {
                        LocaleObjs localeObjs = locale.chunks.OfType<LocaleObjs>().FirstOrDefault();
                        if (localeObjs != null)
                        {
                            return localeObjs.chunks.OfType<RdbObjs>().FirstOrDefault();
                        }
                    }
                }
            }
            if (type == typeof(RdbObjStrings))
            {
                if (flags == Id.Flags.Global)
                {
                    return (RdbObjStrings)PackageManager.LoadByPath("Resource.rpk\\objstrings.rdb");
                }
                else if (flags == Id.Flags.World)
                {
                    if (save != null)
                    {
                        Save.Section section = save.sections.FirstOrDefault(x => x.name == "#worldobjdb");

                        if (section != null)
                        {
                            return ((List<Chunk>)section.data).OfType<RdbObjStrings>().FirstOrDefault();
                        }
                    }
                }
                else if (flags == Id.Flags.Local)
                {
                    if (locale != null)
                    {
                        LocaleObjs localeObjs = locale.chunks.OfType<LocaleObjs>().FirstOrDefault();
                        if (localeObjs != null)
                        {
                            return localeObjs.chunks.OfType<RdbObjStrings>().FirstOrDefault();
                        }
                    }
                }
            }
            else if (type == typeof(RdbChars))
            {
                if (flags == Id.Flags.Global)
                {
                    // TODO
                }
                else if (flags == Id.Flags.World)
                {
                    if (save != null)
                    {
                        return (RdbChars)save.sections.FirstOrDefault(x => x.name == "#worldchrdb").data;
                    }
                }
                else if (flags == Id.Flags.Local)
                {
                    if (locale != null)
                    {
                        LocaleChars localeChars = locale.chunks.OfType<LocaleChars>().FirstOrDefault();
                        if (localeChars != null)
                        {
                            return localeChars.chunks.OfType<RdbChars>().FirstOrDefault();
                        }
                    }
                }
            }
            return null;
        }
    }
}