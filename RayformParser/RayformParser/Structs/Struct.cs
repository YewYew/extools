﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Text;

namespace Rayform
{
    public class Struct
    {
        // Reads the entire struct.
        public virtual void ReadStruct(Stream fs)
        {
            Console.WriteLine("read not supported yet " + GetType());
        }

        //public virtual void ReadStruct(Stream fs, long size)
        //{
        //    Console.WriteLine("read not supported yet " + GetType());
        //}

        public virtual void WriteStruct(Stream fs)
        {
            Console.WriteLine("write not supported yet " + GetType());
        }
    }
}